\page quick guide Quick Guide

this is a short and quick introduction into NastjaPy providing some basic usage instructions. For more detailed questions we refer to the full documentation.

# Prerequisits
- Python 3.8+
- pandas
- scipy
- h5py

if using multiprocessing:
- mpi4py

# Installation
Currently:
```
git clone https://gitlab.com/nastja/nastjapy  
cd nastjapy  
python3 -m pip install -e .
```
Soon:
`python3 -m pip install nastjapy`

# Usage Example
```
from nastja.DataHandler import DataHandler
from nastja.feature_comparison import compare_feature_dicts
import pandas as pd

data_path1, data_path2 = "/PATH/TO/FIRST/SPHEROID", "/PATH/TO/SECOND/SPHEROID"
# specify feature_metadata to select which features to extract witch which settings, falls back
# on DataHandler.DEFAULT_FEATURE_METADATA
feature_metadata = {
        "volumes": {},
        "central_local_density":        {"min_r": 0, "max_r": 400, "dr": 2},
        "average_local_density":        {"min_r": 0, "max_r": 400, "dr": 2},
        "tumor_area": {"overwrite": True},
        "tumor_surface_orientation": {"reassign": True, "overwrite": True},
        "gaslike_dists": {}
    }
dh1 = DataHandler(data_path1, feature_metadata=feature_meta_data)
dh2 = DataHandler(data_path2, feature_metadata=feature_meta_data)

dh1.extract_all_features()
dh2.extract_all_features()

distances = compare_feature_dicts(dh1.features, dh2.features)
print(pd.DataFrame(distances)
```


# Basic Usage

## Loading Data
The fundamental class of the nastjapy package is the DataHandler:
```
from nastja.DataHandler import DataHandler
```
Instances of this class represent the interface used to interact with data from various sources.
To instantiate a DataHandler, you need to specify the path to your data and nastjapy will detect the filetype. 
Since multiple formatting options for `.mat` files are supported, you need to specify the formatting with the `mat_format` parameter.
```
dh = DataHandler(data_path = "PATH/TO/YOUR/DATA", mat_format="macrospheroids")
```
Additional parameters are:
- `timestep_range`: only loads timesteps specified by (start, end, step)
- `shiftToOrigin`: shifts the positions of all cells to center them on the origin
- `scale_factor`: multiply each dimension by this factor

Instead of defining the datapath, the `data` and `features` dictionaries can also be passed directly, for example if we 
are reusing results from a different DataHandler object.

Important attributes of the DataHandler object:

- `DataHandler.data`: dictionary containing positions (and if applicable velocities) for each timestep as a pd.DataFrame
- `DataHandler.features`: dictionary containing dictionaries for each features
- `DataHandler.feature_metadata`: Parameter settings used to extract the features stored in `DataHandler.features`

While it is possible to access these attributes directly,
```
df_at_tp2 = dh.data[2]
print(df)
pos_at_tp2 = np.array(df_at_tp2[["CenterX", "CenterY", "CenterZ"]])
cld_at_tp3 = dh.features["central_local_density"][3]
```
it is often more practicable to use the getter methods:
```
pos_at_tp2 = dh.get_positions_at_time(2)
cld_at_tp3 =  dh.get_central_local_density_at_time(3)
```
In the case of requesting features this has the added benefit of avoiding `KeyError`s if the feature has not yet been 
computed for the given timestep, but computing it instead (see Section Extracting Features).
From an existing DataHandler we can extract new DataHandlers containing only specified timesteps:
```
timestep_range = (2,12,3)
dh2 = dh.extract_datahandler_by_range(timestep_range)  # new datahandler containing timesteps 2,5,8,11
dh3 = dh.extract_datahandler_bytimesteps([3,10,2])     # new datahandler containing timesteps 3,10,2 (in that order)
```
Other useful operations are combining and copying of DataHandlers:
```
copied_dh = copy_datahandler(dh)
doubled_dh = combine_datahandlers(dh, copied_dh)
```

## Extracting Features
We can now use this DataHandler to extract various features. Currently supported are:

- velocities
- volumes
- average_local_density
- individual_local_density
- individual_velocity_correlation
- average_velocity_correlation
- central_local_density
- central_velocity_correlation
- radially_averaged_local_density
- radially_averaged_velocity_correlation
- non_gaslike_fraction
- radial_displacement
- tumor_area
- tumor_surface_orientation
- gaslike_dists

Let's extract the feature "central_local_density". If the source data contains multiple timesteps, we can either extract
the feature for a given point in time via `DataHandler.get_FEATURE_NAME_at_time(time_point)`:
```
central_local_density = dh.get_central_local_density_at_time(2) # extracts cld at timestep 2
```
or we can extract the feature for every time point via `DataHandler.FEATURE_NAME`
```
central_local_densities = dh.central_local_density()
```
Most features require parameters, which are stored in the dh.feature_metadata dictionary. The dh.feature_metadata dictionary
can be specified during instantiation of the DataHandler. If unspecified, default values
as specified in DEFAULT_FEATURE_METADATA are selected, e.g. for the central_local_density:
```
DEFAULT_FEATURE_METADATA = {
        ...
        "central_local_density":        {"min_r": 0, "max_r": 800, "dr": 2},
        ...
    }
```
In this case, these parameters specify the range and the bin width used to compute this distribution. If you want to use
different parameters you can either change the specific parameters during initialization, change the feature_metadata 
dictionary of an existing DataHandler object or simply call the feature method with different parameters. In the last case the 
feature_metadata dictionary gets updated automatically.
```
cld = dh.central_local_density(mar_r=400, dr=10)
# print updated feature metadata:
print(dh.feature_metadata["central_local_density"])
```
The DataHandler computes all required features on demand. The requested feature for each time step is only computed, if 
it has not been previously computed, otherwise the stored feature is returned. (To circumvent this, the `recompute` 
flag can be set.) Features that require other features (like velocity_correlations requires the cell velocities to be computed), 
act similarly.

Instead of extracting each feature individually, you can also use `dh.extract_all_features`. This extracts all features
that are specified in `dh.feature_metadata`.

Extracting features with high resolution for many timesteps can become quite compute intensive. If multiple cores are available,
the command line tool `extract_features_parallel.py` can be used to accelerate the feature extraction using MPI. See 
`python extract_features_parallel --help` for more details.

## Comparing DataHandler

Once the features are extracted, they can be used to compute a distance measure between spheroids:
```
from nastja.feature_comparison import compare_feature_dicts
distances = compare_feature_dicts(dh1.features, dh2.features)      # compares all features contained in both DataHandlers
distances = compare_feature_dicts(dh1.features, dh2.features, 
                                  feature_names="volumes",              #  only compares 'volumes' feature
                                  timestep_mapping=[(1,10),(2,100)])    #  compares timestep 1 and 2 of dh1 with timestep 10 and 100 of dh2
print(pd.DataFrame(distances)                       
```
This returns a list containing a dictionary per compared feature and timstep pairing, to be used as a pd.DataFrame or directly. Each entry contains
the compared timesteps, the feature name and the computed distance.

The overall deviation score can be computed using `get_overall_deviation_score(dh1,dh2)`.

## Visualization

TODO

## Generating Simulation config files

Nastjapy can be used to generate new config files from existing data to be used with CellsInSilico. Using the SimulationGenerator,
a timestep and a simulation box can be extracted from a DataHandler, specifying the starting positions of a new simulation.

## Storing DataHandlers

Feature and feature_metadata dictionaries can be saved independently as pickled objects using 
`dh.save_feature_dict()` and `dh.save_feature_metadata`. Specifying the path to these pickled objects when creating a new
DataHandler from the same data is an easy way to continue working. It is also possible to store the entire DataHandler
using dh.save_datahandler().
Alternatively, the datahandler can also be stored as `.hdf5` file via `dh.store_as_hdf5`.
```
dh.save_as_hdf5(filepath="dh.h5")
loaded_dh = DataHandler(data_path="dh.h5")
```
Choosing the storage backend depends on the use case.
Pickling the objects results in a smaller file size and is marginally faster, but requires to always load the entire object, while the .h5 backend
only loads timesteps once they are requested by the user. An additional disadvantage of pickling in general is, that it 
is not guaranteed to show the same behaviour with later software versions and that it is only accessible using python.
We recommend using pickle when it is clear that always the entire DataHandler is needed. For cases where repeated loading
of individual timesteps from different DataHandlers is required (e.g. for Machine Learning applications) or when processing
the extracted features with different software or programming languages, the h5. backend is recommended.