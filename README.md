# Python interface for CellsInSilico and other data

[NAStJA](https://gitlab.com/nastja/nastja/) is an HPC simulation framework for various domains. This python package is intended to provide easy access to use python scripts for pre- and post-processing of the CellsInSilico application of NAStJA.

## The io module
NAStJA produces different output files all collected in one output directory. Create a `SimDir(output directory)` to access the output files:

-  Parallel output vti files can be read via `readVTI(frame)` as a pyVista object.
- Cell info csv data can be read via `readCSV(frame)` as a pandas dataframe.
- Squeezed cell info data in a SQLite database can be read via `readSQL(frame)` as a pandas dataframe.
- `query(string)` is used to make arbitrary queries.


## The datahandler module

The datahandler module enables loading data from multiple sources (NAStJA, experimental, etc.). These data can then be analyzed and compared. Creating a datahandler object `dh` can be done in multiple ways:

-  `nastja.DataHandler(output directory)`
-   `nastja.DataHandler(simDir)`


#### Development notes
You can install this local package editable for the development via `python3 -m pip install -e .`

Use `python3 -m build` to build the package and `python3 -m twine upload dist/*` to upload it to PyPI.
