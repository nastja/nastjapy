import numpy as np

from DataHandler import DataHandler, copy_datahandler


def rotation_transformation(
    datahandler: DataHandler, angle: float
) -> tuple[DataHandler, DataHandler]:
    rotated = copy_datahandler(datahandler)
    # TODO
    for tp in rotated.data.keys():
        rotated.positions = rotated


def noise_transformation(
    datahandler: DataHandler, transformation_parameter: float
) -> tuple[DataHandler, DataHandler]:
    transformed = copy_datahandler(datahandler)
    for tp in transformed.data.keys():
        transformed.positions[tp] += (
            np.random.rand(*transformed.positions[tp].shape) * transformation_parameter
        )
    transformed.extract_all_features()
    return datahandler, transformed


def scaling_transformation(
    datahandler: DataHandler, transformation_parameter: float
) -> tuple[DataHandler, DataHandler]:
    transformed = copy_datahandler(datahandler)
    for tp in rotated.data.keys():
        transformed.positions[tp] *= transformation_parameter
    transformed.extract_all_features()
    return datahandler, transformed


class TransformationIterator:
    def __init__(
        self, base_datahandler, parameter_range, num_transformations, transformation
    ):
        self.base_datahandler = base_datahandler
        self.parameter_range = parameter_range
        self.num_transformations = num_transformations
        self.transformation = transformation

    def __iter__(self):
        self._current_idx = 0
        return self

    def __next__(self):
        self._current_idx += 1
        if self._current_idx > self.num_transformations:
            raise StopIteration

        transformation_parameter = (
            self._current_idx
            * (self.parameter_range[0] - self.parameter_range[1])
            / float(self.num_transformations)
        )
        next_pair = self.transformation(self.base_datahandler, transformation_parameter)
        return next_pair
