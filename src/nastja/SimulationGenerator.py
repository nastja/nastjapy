import numpy as np
import scipy.io as sio
import h5py
import pandas as pd
from scipy.stats import wasserstein_distance
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.spatial import Voronoi, voronoi_plot_2d
from scipy.spatial import ConvexHull
from scipy.spatial import cKDTree
import os
import json
from copy import copy

from nastja.DataHandler import DataHandler
import pickle
import jsbeautifier

from time import time

# TODO Integrate SimulationGenerator into NastjaPy framework


def savejson(config, outfilename):
    options = jsbeautifier.default_options()
    options.indent_size = 4
    config_pretty = jsbeautifier.beautify(json.dumps(config), options)
    with open(outfilename, "w") as f:
        f.write(config_pretty)

class SimulationGenerator:
    def __init__(self, data_path, dataHandler=None, hyperparams=None):
        assert isinstance(dataHandler, DataHandler), "Datahandler must be provided"
        # if dataHandler is None:
        # self.data_path = data_path
        #        else:
        self.dh = dataHandler  # DataHandler(data_path)
        standard_hyperparams = {
            "box_size": 200.0,
            "box_padding": 50,
            "block_count": [2, 2, 2],
            "space_scaling_factor": 1.0,
            "time_scaling_factor": 1,  # chosen to allow for max speed as seen in data; 1 timestep in data = 5 timesteps in sim
            "initial_cell_size": 5,
            "min_num_points": 50,
            "simulation_length": 250000,
            "sim_box_border_size": 0,
            "template_path": "templates/template1.json",
            "particle_id": 0,
            "outdir": "test",
            "liquid": 1,
            "celltypes": [1],
        }
        if hyperparams is not None:
            standard_hyperparams.update(hyperparams)
        self.hyperparams = standard_hyperparams
        self.out_dir = self.hyperparams["outdir"]
        self.config = {}
        self.run_details = {}
        self.filling_config = None
        os.makedirs(self.out_dir, exist_ok=True)

    def gauge_box_size(self, bounding_scale=1.0, ret=True):
        """
        bounding_scale: box size increase factor, for allowing bounding box
        """

        max_size_per_dim = int(self.hyperparams["box_size"])
        print(max_size_per_dim)
        for tp in self.dh.data.keys():
            pos = self.hyperparams[
                "space_scaling_factor"
            ] * self.dh.get_positions_at_time(tp)
            # print(pos.shape)
            try:
                max_at_tp = np.max(
                    [int(np.max(pos[:, i].round())) for i in range(pos.shape[1])]
                )
            except ValueError:
                continue
            # print(max_at_tp)
            # print(max_size_per_dim)
            max_size_per_dim = max(max_size_per_dim, max_at_tp)
        max_size_per_dim *= bounding_scale
        self.hyperparams["box_size"] = int(max_size_per_dim)
        if ret == True:
            return max_size_per_dim
        else:
            return None

    def generate_conf_at_time(
        self,
        tp: int,
        ID: str,
        params: dict,
        ret: bool = False,
        del_filling: bool = True,
        move_to_center_of_box: bool = True,
        separate_filling_config: bool = False,
        dumpCfg: bool = False,
    ):
        starting_positions = (
            self.hyperparams["space_scaling_factor"] * self.dh.get_positions_at_time(tp)
        ).astype(
            int
        )  # simulation_box["starting_positions"]

        # blocksize_per_dim = int(self.hyperparams['space_scaling_factor'] * self.hyperparams['block_size'])
        box_size = self.hyperparams["box_size"] + 2 * self.hyperparams["box_padding"]
        block_count = self.hyperparams["block_count"]
        block_size = [int(box_size / block_count[i]) for i in range(3)]

        # bbox = [vec.astype(int) for vec in bbox]
        # bbox = [vec.tolist() for vec in bbox]
        run_details = {
            "time_point": tp,
            "ID": ID,
            "space_scaling_factor": self.hyperparams["space_scaling_factor"],
            "time_scaling_factor": self.hyperparams["time_scaling_factor"],
            "simulation_length": self.hyperparams["simulation_length"],
            "hyperparams": self.hyperparams,
            "params": params,
        }

        path_to_template = self.hyperparams["template_path"]
        with open(os.path.join(path_to_template)) as template:
            config = json.load(template)

        config["Geometry"] = {"blocksize": block_size, "blockcount": block_count}

        if "Filling" not in config.keys() and separate_filling_config == False:
            config["Filling"] = {"cells": []}
        if del_filling == True and "Include" in config.keys():
            del config["Include"]

        fillingCfg = None
        if separate_filling_config == True:
            fillingCfg = {"Filling": {"cells": []}}
            config["Include"] = "%s_filling.json" % ID

        box_shift = int(move_to_center_of_box) * 0.5 * box_size

        for pos in starting_positions:
            lower_bound = [
                int(pos[i] - 0.5 * self.hyperparams["initial_cell_size"] + box_shift)
                for i in range(3)
            ]
            upper_bound = [
                int(pos[i] + 0.5 * self.hyperparams["initial_cell_size"] + box_shift)
                for i in range(3)
            ]
            if isinstance(fillingCfg, dict):
                fillingCfg["Filling"]["cells"].append(
                    {
                        "shape": "cube",
                        "box": [lower_bound, upper_bound],
                        "celltype": self.hyperparams["celltypes"][0],
                    }
                )

            else:
                config["Filling"]["cells"].append(
                    {
                        "shape": "cube",
                        "box": [lower_bound, upper_bound],
                        "celltype": self.hyperparams["celltypes"][0],
                    }
                )
        # config["Filling"]["cells"].append({"shape": "cube",
        #                                    "pattern": "voronoi",
        #                                    "pointlist": starting_positions,
        #                                    "size": self.hyperparams["initial_cell_size"],
        #                                    "box": [[0, 0, 0], self.hyperparams['bounding_box']],
        #                                    "celltype": self.hyperparams['celltypes'][0]})

        liquid = self.hyperparams["liquid"]
        config["CellsInSilico"]["liquid"] = liquid

        config["CellsInSilico"]["temperature"] = params["temperature"]
        config["CellsInSilico"]["volume"]["default"]["value"] = params["volume"]
        config["CellsInSilico"]["volume"]["lambda"] = params["volume_lambda"]
        config["CellsInSilico"]["surface"]["default"]["value"] = params["surface"]
        config["CellsInSilico"]["surface"]["lambda"] = params["surface_lambda"]

        config["Settings"]["timesteps"] = self.hyperparams["simulation_length"]
        # adhesion_matrix_dim = self.hyperparams['liquid'] + 1 + self.hyperparams['N_celltypes']  #Liquid val + cell types + solid val of 0
        # adhesion_matrix = np.zeros((adhesion_matrix_dim, adhesion_matrix_dim))
        adhesion_matrix = copy(params["adhesion"])
        config["CellsInSilico"]["adhesion"]["matrix"] = adhesion_matrix
        #     for param_key, param_val in params.items():
        #         config["CellsInSilico"][param_key] = param_val

        self.config = config
        self.filling_config = fillingCfg
        self.run_details = run_details

        if dumpCfg == True:
            self.dump_config(ID)

        if ret == True:
            return config, run_details

    def dump_config(self, ID, indent=4):
        cwd = os.getcwd()
        config_filepath = os.path.join(cwd, self.out_dir, str(ID) + ".json")
        savejson(self.config,config_filepath)
        # with open(config_filepath, "w+") as f:
        #     json.dump(self.config, f, indent=indent)
        if isinstance(self.filling_config, dict):
            config_filepath = os.path.join(cwd, self.out_dir, str(ID) + "_filling.json")
            savejson(self.config,config_filepath)
            # with open(config_filepath, "w+") as f:
            #     json.dump(self.filling_config, f, indent=indent)

        run_details_filepath = os.path.join(
            cwd, self.out_dir, str(ID) + "_run_details.json"
        )
        savejson(self.run_details,run_details_filepath)
        # with open(run_details_filepath, "w+") as f:
        #     json.dump(self.run_details, f)
        print(json.dumps(self.config, indent=indent))

        return config_filepath

    # def select_simulation_box(self, tp=None, entire_bounding_box=False):
    #     if tp is None:
    #         tp = np.random.randint(self.data.total_time - 50)
    #     positions = self.data.get_positions_at_time(tp)
    #     starting_positions, bbox = self.data.extract_starting_positions(tp, translate_to_origin=True,
    #                                                                     min_num_points=self.hyperparams["min_num_points"],
    #                                                                     positions=positions,
    #                                                                     size=self.hyperparams["box_size"],
    #                                                                     entire_bounding_box=entire_bounding_box)
    #     simulation_box = {"tp": tp,
    #                       "starting_positions": starting_positions,
    #                       "bbox": bbox}
    #     return simulation_box

    # def collect_features_for_simulation_box(self, simulation_box, ID=0):
    #     features = {"volumes": [],
    #                      "velocities": [], "local_density": [], "velocity_correlation": []}
    #     for tp in range(simulation_box["tp"], simulation_box["tp"]+self.hyperparams["simulation_length"]):
    #         t = time()
    #         positions = self.data.get_positions_at_time(tp)
    #         print("positions:", time()-t)
    #         t = time()
    #         velocities = self.data.get_velocities_at_time(tp)
    #         print("velocities:", time() - t)
    #         t = time()
    #         volumes = self.data.get_volumes_at_time(tp)
    #         print("volumes:", time() - t)
    #         t = time()
    #         tree = cKDTree(positions)
    #         print("tree building:", time() - t)
    #         t = time()
    #         velocity_correlation = self.data.get_individual_velocity_correlation_at_time(tp)
    #         plt.plot(velocity_correlation)
    #         plt.savefig("velocity_correlation_2.png")
    #         plt.show()
    #         print("velocity correlation:", time() - t)
    #         t = time()
    #         local_density = self.data.get_individual_local_density_at_time(tp)
    #         print("local density:", time() - t)
    #         t = time()
    #         features["volumes"].append(volumes)
    #         features["velocities"].append(velocities)
    #         features["local_density"].append(local_density)
    #         features["velocity_correlation"].append(velocity_correlation)
    #         print("saving:", time() - t)
    #     with open(os.path.join(self.outdir, ID, "features")) as pickled_features:
    #         pickle.dump(features, pickled_features)
    #     return os.path.join(self.outdir, ID, "features")

    # def generate_conf(self, ID, params, simulation_box):
    #     starting_positions = self.hyperparams["space_scaling_factor"] * simulation_box["starting_positions"]
    #     bbox = [self.hyperparams["space_scaling_factor"]*c for c in simulation_box["bbox"]]
    #     starting_positions += np.array(self.hyperparams["sim_box_border_size"])
    #     starting_positions = (np.round(starting_positions)).astype(int)
    #     starting_positions = starting_positions.tolist()

    #     yolk_axes = [int(axis) for axis in self.data.semiaxes_over_time_sw10[simulation_box["tp"]]]
    #     yolk_center = [-int(dim) + self.hyperparams["sim_box_border_size"] for dim in bbox[0]]

    #     determine_block_size = False
    #     if determine_block_size:
    #         blocksize = int(np.ceil((bbox[1] + 2*self.hyperparams["sim_box_border_size"]) / self.hyperparams["blockcount"]))

    #         blockcount = np.ceil(bbox[1] / np.array(blocksize))
    #         blockcount = blockcount.astype(int)
    #         blockcount = blockcount.tolist()
    #     else:
    #         blocksize = [72, 108, 216]
    #         blockcount = [6, 4,2]

    #     bbox[1] += bbox[0]
    #     bbox = [vec.astype(int) for vec in bbox]
    #     bbox = [vec.tolist() for vec in bbox]
    #     run_details = {"bounding_box": bbox,
    #                    "time_point": simulation_box["tp"],
    #                    "ID": ID,
    #                    "space_scaling_factor": self.hyperparams["space_scaling_factor"],
    #                    "time_scaling_factor": self.hyperparams["time_scaling_factor"],
    #                    "simulation_length": self.hyperparams["simulation_length"],
    #                    "hyperparams": self.hyperparams,
    #                    "params": params}

    #     cwd = os.getcwd()
    #     path_to_template = self.hyperparams["template_path"]
    #     with open(os.path.join(cwd, path_to_template)) as template:
    #         config = json.load(template)

    #     config["Geometry"] = {"blocksize": blocksize,
    #                           "blockcount": blockcount}
    #     config["Filling"]["cells"].append({"shape": "cube",
    #                                        "pattern": "voronoi",
    #                                        "pointlist": starting_positions,
    #                                        "size": self.hyperparams["initial_cell_size"],
    #                                        "box": [[0, 0, 0], ["Nx", "Ny", "Nz"]]})
    #     # config["Filling"]["cells"].append({"shape": "ellipsoid",
    #     #                                    "value": 0,
    #     #                                    "center": yolk_center,
    #     #                                    "xsemiaxis": yolk_axes[0],
    #     #                                    "ysemiaxis": yolk_axes[1],
    #     #                                    "zsemiaxis": yolk_axes[2]})
    #     config["CellsInSilico"]["liquid"] = self.hyperparams['liquid']

    #     config["CellsInSilico"]["temperature"] = params["temperature"]
    #     config["CellsInSilico"]["volume"]["default"]["value"] = params["volume"]
    #     config["CellsInSilico"]["volume"]["lambda"]["value"] = params["volume_lambda"]
    #     config["CellsInSilico"]["surface"]["default"]["value"] = params["surface"]
    #     config["CellsInSilico"]["surface"]["lambda"]["value"] = params["surface_lambda"]

    #     adhesion_matrix = np.zeros((3, 3))
    #     adhesion_matrix[2:, 2:] = params["adhesion"]
    #     adhesion_matrix[0, 2] = params["adhesion"]
    #     adhesion_matrix[2, 0] = params["adhesion"]

    #     adhesion_matrix = adhesion_matrix.tolist()
    #     config["CellsInSilico"]["adhesion"]["matrix"] = adhesion_matrix
    #     #     for param_key, param_val in params.items():
    #     #         config["CellsInSilico"][param_key] = param_val

    #     config_filepath = os.path.join(cwd, self.out_dir, str(ID) + ".json")
    #     with open(config_filepath, "w+") as f:
    #         json.dump(config, f, indent=2)
    #     run_details_filepath = os.path.join(cwd, self.out_dir, str(ID) + "_run_details.json")
    #     with open(run_details_filepath, "w+") as f:
    #         json.dump(run_details, f)
    #     print(json.dumps(config, indent=2))
    #     return config_filepath

    # def compute_distributions(self, positions):
    #     tree = cKDTree(positions)

    #     # volume_distribution
    #     volume_distribution = get_volume_distribution(positions)
    #     # velocity_distribution
    #     velocity_distribution = get_velocity_distribution(positions)
    #     # local_density_function
    #     local_density = get_local_density(_, tree)
    #     # velocity_correlation
    #     velocity_correlation = get_velocity_correlation(tree)
    #     return volume_distribution, velocity_distribution, local_density, velocity_correlation

    # def compute_objective_function(self, ):
    #     pass
