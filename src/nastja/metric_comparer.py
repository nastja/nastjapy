from __future__ import annotations
from copy import deepcopy
from collections.abc import Callable
from functools import partial
from itertools import permutations
from joblib import Parallel, delayed
import numpy as np
import os
import pickle
import json
import sys
import time
from typing import Tuple, List
from .utils import get_cartesians, get_sphericals

import h5py as h5

from nastja.DataHandler import DataHandler, FEATURE_FUNCTIONS, copy_datahandler
from nastja import feature_comparison, metrics
from nastja.utils import cartesian_to_spherical, rot


def load_pair_from_new_dhs(dh1_number, dh2_number, filepath, **kwargs):
    dh1 = DataHandler("%s/%05d/out0" % (filepath, dh1_number), **kwargs)
    dh2 = DataHandler("%s/%05d/out0" % (filepath, dh2_number), **kwargs)
    return dh1_number, dh1, dh2_number, dh2


def load_pair_from_pickle(dh1_number, dh2_number, filepath):
    dh1 = None
    dh2 = None
    try:
        with open("%s/%05d/out0/datahandler.p" % (filepath, dh1_number), "rb") as f:
            dh1 = pickle.load(f)
        with open("%s/%05d/out0/datahandler.p" % (filepath, dh2_number), "rb") as f:
            dh2 = pickle.load(f)
        return dh1_number, dh1, dh2_number, dh2
    except (KeyError, ValueError, FileNotFoundError, OSError) as error:
        print("error")
        return dh1_number, None, dh2_number, None


def load_replicate(outNbr1, outNbr2, filepath):
    dh1 = None
    dh2 = None
    try:
        with open("%s/out%d/datahandler.p" % (filepath, outNbr1), "rb") as f:
            dh1 = pickle.load(f)
        with open("%s/out%d/datahandler.p" % (filepath, outNbr2), "rb") as f:
            dh2 = pickle.load(f)
        return outNbr1, dh1, outNbr2, dh2
    except (KeyError, ValueError, FileNotFoundError, OSError) as error:
        return outNbr1, None, outNbr2, None


def load_pair_from_hdf5(dh1_number, dh2_number, filepath):
    try:
        dh1 = DataHandler("%s/%05d/out0/datahandler.h5" % (filepath, dh1_number))
        dh2 = DataHandler("%s/%05d/out0/datahandler.h5" % (filepath, dh2_number))
        return (dh1_number, dh1, dh2_number, dh2)
    except (KeyError, ValueError, FileNotFoundError, OSError) as error:
        return (dh1_number, None, dh2_number, None)


def rotation_transformation_function(
    datahandler: DataHandler, transformation_parameter: float
) -> DataHandler:
    for tp in datahandler.data.keys():
        positions = datahandler.get_positions_at_time(tp)
        new_positions = rot(positions, theta=transformation_parameter)
        datahandler.update_positions_at_time(tp=tp, new_positions=new_positions)
    return datahandler


def noise_transformation_function(
    datahandler: DataHandler, transformation_parameter: float
) -> DataHandler:
    for tp in datahandler.data.keys():
        positions = datahandler.get_positions_at_time(tp)
        new_positions = (
            2
            * (np.random.rand(*positions.shape) - np.full(positions.shape, 0.5))
            * transformation_parameter
            + positions
        )
        datahandler.update_positions_at_time(tp=tp, new_positions=new_positions)
    return datahandler


def deformation_transformation_function(
    datahandler: DataHandler,
    transformation_param: float,
    transformation_frequency: float = 10,
) -> DataHandler:
    for tp in datahandler.data.keys():
        positions = datahandler.get_positions_at_time(tp)
        spherical = get_sphericals(positions)
        spherical[:, 2] = spherical[:, 2] + transformation_param * (
            np.cos(transformation_frequency * spherical[:, 0])
            + np.sin(transformation_frequency * spherical[:, 1])
        )
        new_positions = get_cartesians(spherical)
        datahandler.update_positions_at_time(tp=tp, new_positions=new_positions)
    return datahandler


def scaling_transformation_function(
    datahandler: DataHandler, transformation_parameter: float
) -> DataHandler:
    for tp in datahandler.data.keys():
        positions = datahandler.get_positions_at_time(tp)
        new_positions = positions * transformation_parameter
        datahandler.update_positions_at_time(tp=tp, new_positions=new_positions)
    return datahandler


def apply_transformation_function(
    datahandler: DataHandler,
    transformation_parameter: float,
    transformation_function: Callable[
        [DataHandler, float], DataHandler
    ] = scaling_transformation_function,
    extract_features: bool = True,
    save_transformed_to: str = None,
) -> Tuple[DataHandler, DataHandler]:
    transformed = copy_datahandler(datahandler)
    transformed = transformation_function(transformed, transformation_parameter)
    if extract_features:
        transformed.extract_all_features()
    if save_transformed_to:
        transformed.save_datahandler(save_transformed_to)
    return datahandler, transformed


def optimize_weighted_distance_combination(
    feature_distances: List[np.array], id_to_subgroup_mapping=None
) -> List[float]:
    pass


def get_combined_distance_weights(
    subgroups: List[DataHandler], feature_names: List[str]
) -> List[float]:
    intra_distances = [
        [
            distance(spheroid1, spheroid2)
            for spheroid1, spheroid2 in permutations(subgroup, 2)
            for subgroup in subgroups
        ]
    ]


class TransformationIterator:
    def __init__(
        self,
        base_datahandler,
        parameter_range,
        num_transformations,
        transformation_function,
        extract_features=True,
    ):
        self.base_datahandler = base_datahandler
        self.parameter_range = parameter_range
        self.num_transformations = num_transformations
        self.transformation_function = transformation_function
        self.transformation = partial(
            apply_transformation_function,
            transformation_function=self.transformation_function,
            extract_features=extract_features,
        )

    def __iter__(self):
        self._current_idx = 0
        return self

    def __next__(self):
        self._current_idx += 1
        if self._current_idx > self.num_transformations:
            raise StopIteration

        transformation_parameter = (
            self._current_idx
            * (self.parameter_range[1] - self.parameter_range[0])
            / float(self.num_transformations)
        )
        #        print(transformation_parameter)
        next_pair = self.transformation(self.base_datahandler, transformation_parameter)
        return (0, next_pair[0], self._current_idx, next_pair[1])


LOADFUNCS = {
    "dh": load_pair_from_new_dhs,
    "pickle": load_pair_from_pickle,
    "pickle_replicate": load_replicate,
    "hdf5": load_pair_from_hdf5,
    "custom": None,
}


def dhPair(dh1, dh2, filepath, loadFunc, **kwargs):
    print(dh1)
    print(dh2)
    print(filepath)
    print(loadFunc)
    return loadFunc(dh1, dh2, filepath, **kwargs)


def create_2d_projection_from_distances(distances, initial_positions=None):
    """Turns a symmetric distance matrix into a list of 2d points

    Parameters:
        distances: Distance Matrix
        initial_positions: starting positions for SMACOF algorithm. Specify this with output of previous run to have
                           comparable results
    Returns:
         2-dimensional embedding
    """
    embedding = MDS(n_components=2)
    embedding.set_params(dissimilarity="precomputed")
    pos_2d = embedding.fit_transform(distances, init=initial_positions)
    # x, y = [p[0] for p in pos_2d], [p[1] for p in pos_2d]
    # plt.scatter(x, y)
    return pos_2d


class ParamSpaceIterator:
    xParam: int = 1
    paramFile: np.ndarray = None
    simPath: str = None

    def __init__(
        self,
        sim_path,
        loadMode,
        xParam=1,
        yParam=None,
        folders1=None,
        folders2=None,
        val=0,
        loadFunc=None,
    ):
        assert loadMode in LOADFUNCS.keys(), "Unknown loading function"
        # TODO: remove redundant loadMode
        self.loadFunc = LOADFUNCS[loadMode]
        if loadMode == "custom":
            assert type(loadFunc) != type(
                None
            ), "If custom loadMode is set, you need to provide a function handle for loadFunc"
            self.loadFunc = loadFunc
        self.xParam = xParam
        self.yParam = yParam
        self.simPath = sim_path
        self.folders1 = folders1
        self.folders2 = folders2
        self.size = 0
        if isinstance(self.folders1, np.ndarray):
            self.size = self.folders1.size
        elif isinstance(self.folders1, list):
            self.size = len(self.folders1)

        recalc = False
        if type(folders1) == type(None) or type(folders2) == type(None):
            recalc = True
        else:
            if isinstance(folders1, np.ndarray) and isinstance(folders2, np.ndarray):
                if (
                    folders1.size != folders2.size
                ):  # If this is the case, we need to manually set folders1 and folders2
                    recalc = True
            if isinstance(folders1, list) and isinstance(folders2, list):
                if len(folders1) != len(
                    folders2
                ):  # If this is the case, we need to manually set folders1 and folders2
                    recalc = True

        if recalc == True:
            self.paramFile = np.loadtxt(sim_path + "folder_config_key.txt")

            if (
                yParam == None
            ):  # If this is the case, we look at the phase space gradient, i.e. all changes along axis
                self.paramVals = np.unique(self.paramFile[:, xParam])
                self.dh1Indices = np.where(
                    self.paramFile[:, self.xParam] == self.paramVals[val]
                )[0]

                self.folders1 = deepcopy(self.paramFile[self.dh1Indices, 0])  #
                params = deepcopy(self.paramFile[self.dh1Indices, :])
                assert (
                    val < self.paramVals.size - 1
                ), "Final param val. No comparison available"

                params[:, xParam] = self.paramVals[val + 1]

                self.folders2 = np.zeros(self.folders1.size)
                for i, row in enumerate(params):
                    index = np.where((self.paramFile[:, 1:] == row[1:]).all(1) == True)[
                        0
                    ]
                    if index.size > 0:
                        self.folders2[i] = self.paramFile[index, 0]

            else:  # If this is the case, we compare folders along the parameter axes
                xParamVals = np.unique(self.paramFile[:, xParam])
                yParamVals = np.unique(self.paramFile[:, yParam])
                index = np.where(
                    (self.paramFile[:, xParam] == xParamVals[0])
                    & (self.paramFile[:, yParam] == yParamVals[0])
                )[0]
                self.folders1 = np.full(
                    xParamVals.size * yParamVals.size,
                    self.paramFile[index[0], 0],
                    dtype=int,
                )
                self.folders2 = np.zeros(self.folders1.size, dtype=int)
                count = 0
                for valX in xParamVals:
                    for valY in yParamVals:
                        index = np.where(
                            (self.paramFile[:, xParam] == valX)
                            & (self.paramFile[:, yParam] == valY)
                        )[0]
                        self.folders2[count] = self.paramFile[index[0], 0]
                        count += 1
        self.it = 0
        # self.dhPair = dhPair(self.folders1[self.it], self.folders2[self.it], self.simPath, self.loadFunc)

    def __iter__(self):
        self.dhPair = dhPair(
            self.folders1[self.it], self.folders2[self.it], self.simPath, self.loadFunc
        )
        return self

    def __next__(self):
        if self.it >= self.size:
            raise StopIteration
        self.dhPair = dhPair(
            self.folders1[self.it], self.folders2[self.it], self.simPath, self.loadFunc
        )
        x = self.dhPair
        self.it += 1
        return x


#################################### METRIC COMPARER ############################################################


class MetricComparer:
    def __init__(self, dhIt, featureList, normalize=False, metrics_for_features=None):
        self.it = dhIt
        self.featureList = featureList
        self.cmpVals = None
        self.normalize = normalize
        if metrics_for_features == None:
            self.metrics_for_features = feature_comparison.METRICS_FOR_FEATURES
        else:
            self.metrics_for_features = metrics_for_features

    def saveCmp(
        self, outFilePath, mode: str = "pickle", split_features: bool = False
    ):  # TODO: add more modes
        assert (
            "." not in outFilePath
        ), "Please provide outFilepath without the file suffix"
        assert self.cmpVals != None, "No comparison vals present"

        if mode == "pickle":
            with open(outFilePath + "_all_features.p", "wb") as f:
                pickle.dump(self.cmpVals, f)
        # elif mode == 'json':
        #     with open(outFilePath + '_all_features.json', 'w') as f:
        #         json.dump(self.cmpVals, f, indent=4)

    def runCmp(self, ret=False, timestep_mapping: list = []):
        self.cmpVals = {}
        for dhPair in self.it:
            id1, dh1, id2, dh2 = dhPair
            cmpVal = feature_comparison.compare_datahandlers(
                dh1,
                dh2,
                timestep_mapping=timestep_mapping,
                feature_names=self.featureList,
            )
            self.cmpVals[(id1, id2)] = deepcopy(cmpVal)

        if ret == True:
            return self.cmpVals
