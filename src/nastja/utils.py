# from __future__ import annotations
import numpy as np
import pandas as pd
from tqdm import tqdm
from sklearn.neighbors import KDTree
import time
from skimage import measure
from scipy.ndimage.filters import gaussian_filter
from scipy.optimize import minimize
from scipy.stats import gaussian_kde
import matplotlib.pyplot as plt
from itertools import combinations
from copy import deepcopy
import json
import h5py


class HDFGroup:
    def __init__(self, filepath, group_name):
        self.filepath = filepath
        self.group_name = group_name

    def __getitem__(self, dataset_name):
        with h5py.File(self.filepath, "r") as f:
            if isinstance(dataset_name, int):
                dataset_name = str(dataset_name)

            dataset = np.array(f[self.group_name][dataset_name])

        return dataset

    def keys(self):
        with h5py.File(self.filepath) as f:
            keys = list(f[self.group_name].keys())
        return keys

    def items(self):
        with h5py.File(self.filepath) as f:
            items = {key: self.__getitem__(key) for key in self.keys()}
        return items.items()


class HDFGroupWithSubgroups:
    def __init__(self, filepath, group_name):
        self.filepath = filepath
        self.group_name = group_name

    def __getitem__(self, subgroupname):
        return HDFGroup(self.filepath, f"{self.group_name}/{subgroupname}")

    def keys(self):
        with h5py.File(self.filepath) as f:
            keys = list(f[self.group_name].keys())
        return keys

    def items(self):
        with h5py.File(self.filepath) as f:
            items = {key: self.__getitem__(key) for key in self.keys()}
        return items.items()


class HDFToDataframeHandler:
    def __init__(self, filepath, group_name):
        self.filepath = filepath
        self.group_name = group_name

    def __getitem__(self, dataset_name):
        # with pd.HDFStore(self.filepath) as store:
        #    dataframe = store[self.group_name]  # [dataset_name]
        dataframe = pd.read_hdf(self.filepath, key=f"{self.group_name}/{dataset_name}")
        return dataframe

    # def __setitem__(self, dataset_name, new_value):
    #    with h5py.File(self.filepath) as f:
    #        f[dataset_name] =

    def keys(self):
        with h5py.File(self.filepath) as f:
            keys = list(f[self.group_name].keys())
        return keys

    def items(self):
        with h5py.File(self.filepath) as f:
            items = {key: self.__getitem__(key) for key in self.keys()}
        return items.items()


class HDFFile:
    def __init__(self, filepath):
        self.filepath = filepath

    def __getitem__(self, item):
        return HDFGroup(self.filepath, item)


def test_cart2spher():
    cart = np.random.rand(10000, 3) - np.ones((10000, 3)) * 0.5
    spherical = cartesian_to_spherical(cart)

    fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
    ax1.plot(spherical[:, 1])
    ax2.plot(spherical[:, 2])
    plt.show()


def cartesian_to_spherical(cart_pointcloud: np.array) -> np.array:
    spherical_pointcloud = np.empty(cart_pointcloud.shape)
    xy = cart_pointcloud[:, 0] ** 2 + cart_pointcloud[:, 1] ** 2
    spherical_pointcloud[:, 0] = np.sqrt(xy + cart_pointcloud[:, 2] ** 2)
    spherical_pointcloud[:, 1] = np.arctan2(
        np.sqrt(xy), cart_pointcloud[:, 2]
    )  # for elevation angle defined from Z-axis down
    # ptsnew[:,4] = np.arctan2(xyz[:,2], np.sqrt(xy)) # for elevation angle defined from XY-plane up
    spherical_pointcloud[:, 2] = np.arctan2(
        cart_pointcloud[:, 1], cart_pointcloud[:, 0]
    )
    return spherical_pointcloud


def dist(p1, p2):
    return np.linalg.norm(p1 - p2)


def pointwise_norm(vectors: np.array) -> float:
    return np.sum(np.abs(vectors) ** 2, axis=-1) ** (1.0 / 2)


def create_distance_cache(vectors):
    comb_array = np.array(list(combinations(vectors, 2)))
    return pointwise_norm(comb_array[:, 0] - comb_array[:, 1])


def create_dot_cache(vectors):
    comb_array = np.array(list(combinations(vectors, 2)))
    return np.apply_along_axis(np.dot, 2, comb_array)


def all_equal(iterator):
    iterator = iter(iterator)
    try:
        first = next(iterator)
    except StopIteration:
        return True
    return all(first == x for x in iterator)


def is_in_box(p, a, b, c, d):
    i, j, k, v = [x - a for x in [b, c, d, p]]
    inside = 0 < np.dot(v, i) < np.dot(i, i)
    inside &= 0 < np.dot(v, j) < np.dot(j, j)
    inside &= 0 < np.dot(v, k) < np.dot(k, k)
    return inside


def extract_box(points, corners):
    return np.array(list(filter(lambda x: is_in_box(x, *corners), points)))


def extract_cuboid(points, corner, x, y, z):
    corner = np.array(corner)
    x, y, z = (
        corner + np.array([x, 0, 0]),
        corner + np.array([0, y, 0]),
        corner + np.array([0, 0, z]),
    )
    return extract_box(points, [corner, x, y, z])


def bbox_to_corners(bbox):
    corners = [
        bbox[0],
        bbox[0] + [bbox[1][0], 0, 0],
        bbox[0] + [0, bbox[1][0], 0],
        bbox[0] + [0, 0, bbox[1][0]],
    ]
    return corners


def extract_positions_with_ids_in_box(points, list_of_ids, corners):
    ids_in_box = []
    for ID, point in zip(list_of_ids, points):
        if is_in_box(point, *corners):
            ids_in_box.append(ID)
    return np.array(ids_in_box)


def get_args_dict(fn, args, kwargs):
    args_names = fn.__code__.co_varnames[: fn.__code__.co_argcount]
    return {**dict(zip(args_names, args)), **kwargs}


def conditional_decorator(dec, condition):
    def decorator(func):
        if not condition:
            # Return the function unchanged, not decorated.
            return func
        return dec(func)

    return decorator


def conditional_tqdm(iterable, show_progress):
    if show_progress:
        return tqdm(iterable)
    else:
        return iterable


def find_highest_density(numbers):
    """Uses kernel density estimation to determine the point with highest density for a list of numbers."""
    # Create a KDE object with the given numbers
    kde = gaussian_kde(numbers)

    # Generate a range of x values to evaluate the density function
    x_values = np.linspace(min(numbers), max(numbers), 1000)

    # Evaluate the density function at each x value
    y_values = kde(x_values)

    # Find the x value with the highest density
    highest_density_x = x_values[np.argmax(y_values)]

    return highest_density_x


class Node:
    """A node class for A* Pathfinding"""

    def __init__(self, parent=None, position=None, kd_tree_idx=None):
        self.parent = parent
        self.position = position
        self.kd_tree_idx = kd_tree_idx

        self.g = 0
        self.h = 0
        self.f = 0

    def __eq__(self, other):
        try:
            return all(self.position == other.position)
        except ValueError:
            return np.all(self.position == other.position)


# def calc_vels(pos : pd.DataFrame(),boxsize : int=400):
#     vel = (pos.subtract(positions[tp - self.timestep_range[-1]], axis="index")).rename(
#                     columns={"CenterX": "VelX", "CenterY": "VelY", "CenterZ": "VelZ"})

# for row in vel:
# x_plus =
# x_minus =
# y_plus =
# y_minus =
# z_plus =
# z_minus =
# return vel


def compute_neighbours(
    positions: np.array, start: np.array, radius: float = 0.2, verbose: bool = False
) -> tuple:
    """
    computes neighbourhoods for all points in positions with given radius
    """
    new_positions = np.zeros((len(positions) + 1, 3))
    new_positions[0] = start
    new_positions[1:] = positions
    tree = KDTree(new_positions)
    neighbours = tree.query_radius(new_positions, r=radius)
    if verbose == True:
        print(neighbours)
    neighbours = [indexes[1:] for indexes in neighbours]
    return new_positions, neighbours


def astar(
    positions, neighbours, start_index, end_index, verbose=False, timelimit: int = 100
):
    """Returns a list of tuples as a path from the given start to the given end through the given point cloud

    positions: np.array of coordinates
    neighbours: list containing the indices of neighbours for each point
    end: end point to be reached
    verbose: if true, path is printed
    timelimit: maximum time in seconds, for which the while loop is allowed to run.
    Returns path: list of coordinates
    """

    # Create start and end node
    start_node = Node(None, positions[start_index], start_index)
    start_node.g = start_node.h = start_node.f = 0
    end_node = Node(None, positions[end_index], end_index)
    end_node.g = end_node.h = end_node.f = 0

    end = positions[end_index]
    # Initialize both open and closed list
    open_list = []
    closed_list = []

    # Add the start node
    open_list.append(start_node)

    starttime = time.time()

    # Loop until you find the end
    while len(open_list) > 0:
        if time.time() - starttime >= timelimit:
            if verbose == True:
                print("Time limit exceeded")
            break
        # print([node.position for node in open_list])
        if verbose == True:
            print(
                "len(open_list) = %d, len(closed_list = %d"
                % (len(open_list), len(closed_list))
            )
        # Get the current node
        current_node = open_list[0]
        current_index = 0
        for index, item in enumerate(open_list):
            if item.f < current_node.f:
                current_node = item
                current_index = index

        # Pop current off open list, add to closed list
        open_list.pop(current_index)
        closed_list.append(current_node)

        # Found the goal
        if current_node == end_node:
            if verbose == True:
                print("Found the goal")
            path = []
            current = current_node
            while current is not None:
                path.append(current.position)
                current = current.parent
            return path[::-1]  # Return reversed path

        # Generate children
        children = []
        # try:
        if verbose == True:
            print(
                "current_node.kd_tree_idx = %d. Its type: %s"
                % (current_node.kd_tree_idx, type(current_node.kd_tree_idx))
            )
            print("len(neighbors) = %d" % len(neighbours[current_node.kd_tree_idx]))
        children = [
            Node(current_node, positions[neighbour], neighbour)
            for neighbour in neighbours[current_node.kd_tree_idx]
        ]
        # except TypeError:
        #     if verbose == True:
        #         print('TypeError')
        #     children = []
        if verbose == True:
            print("len(children) = %d" % len(children))
        # Loop through children
        for child in children:
            # Child is on the closed list
            in_closed_list = False
            for closed_child in closed_list:
                if child == closed_child:
                    in_closed_list = True
            if in_closed_list:
                continue

            # Create the f, g, and h values
            child.g = current_node.g + dist(child.position, current_node.position)
            child.h = dist(child.position, end)
            child.f = child.g + child.h

            # Child is already in the open list
            in_open_list = False
            for open_node in open_list:
                if child == open_node and child.g > open_node.g:
                    in_open_list = True
            if in_open_list:
                continue

            # Add the child to the open list
            open_list.append(child)


def extract_wall_points(
    img,
):  # Takes a 2d-numpy array and returns locations of all points (i,j) where img[i,j] == 1
    wall_points_x = []
    wall_points_y = []
    for i in range(img[:, 0].size):
        indices = list(np.where(img[i, :] != 0.0)[0])
        wall_points_x += indices
        wall_points_y += len(indices) * [i]
    #        for j in range(img[0,:].size):
    #            if img[i,j] != 0.0:
    #                wall_points_x.append(i)
    #                wall_points_y.append(j)
    return (wall_points_x, wall_points_y)


def get_circumference(sorted_positions):
    point_to_point_dists = np.array(
        [
            dist(sorted_positions[i - 1, :], sorted_positions[i, :])
            for i in range(1, sorted_positions[:, 0].size)
        ]
    )
    return np.sum(point_to_point_dists)


def rot(positions, theta, axis=1):  # Rotates a point cloud around axis x,y or z
    rot_x = np.array(
        [
            [1, 0, 0],
            [0, np.cos(theta), -np.sin(theta)],
            [0, np.sin(theta), np.cos(theta)],
        ]
    )
    rot_y = np.array(
        [
            [np.cos(theta), 0, np.sin(theta)],
            [0, 1, 0],
            [-np.sin(theta), 0, np.cos(theta)],
        ]
    )
    rot_z = np.array(
        [
            [np.cos(theta), -np.sin(theta), 0],
            [np.sin(theta), np.cos(theta), 0],
            [0, 0, 1],
        ]
    )

    rot_mat = 0
    if axis == 1:
        rot_mat = rot_x
    elif axis == 2:
        rot_mat = rot_y
    else:
        rot_mat = rot_z

    positions = rot_mat @ positions.transpose()
    positions = positions.transpose()
    return positions


def rot3Angles(positions, alpha, beta, gamma):
    rot_x = np.array(
        [
            [1, 0, 0],
            [0, np.cos(gamma), -np.sin(gamma)],
            [0, np.sin(gamma), np.cos(gamma)],
        ]
    )
    rot_y = np.array(
        [[np.cos(beta), 0, np.sin(beta)], [0, 1, 0], [-np.sin(beta), 0, np.cos(beta)]]
    )
    rot_z = np.array(
        [
            [np.cos(alpha), -np.sin(alpha), 0],
            [np.sin(alpha), np.cos(alpha), 0],
            [0, 0, 1],
        ]
    )

    positions = rot_z @ rot_y @ rot_x @ positions.transpose()
    return positions.transpose()


def get_marching_cubes_tumor_surface(
    pos,
    cellSize=30,
    NPerDim=100,
    sigma=1.5,
    threshold=0.2,
    eps=0.01,
    offset=250,
    returnMCInfo=False,
):
    img = np.zeros((NPerDim, NPerDim, NPerDim), dtype=float)
    i = 0
    x = pos[:, i] + np.abs(np.min(pos[:, i])) + 0.5 * offset
    i += 1
    y = pos[:, i] + np.abs(np.min(pos[:, i])) + 0.5 * offset
    i += 1
    z = pos[:, i] + np.abs(np.min(pos[:, i])) + 0.5 * offset

    # above is equivalent to x,y,z = (pos[:,i]+np.abs(np.min(pos[:,i])) + 0.5*offset for i in range(3))

    maxPerDim = [np.max(x), np.max(y), np.max(z)]
    maxDist = np.max(maxPerDim) + 0.5 * offset
    print(maxDist)
    bins = np.linspace(0, maxDist, NPerDim)
    dxyz = bins[1] - bins[0]
    NStepsCellSize = int(0.5 * (cellSize / (dxyz) + 1))

    count = 0
    for i in range(x.size):
        try:
            xIndex = int(np.where((bins[:-1] <= x[i]) & (bins[1:] > x[i]))[0][0])
            yIndex = int(np.where((bins[:-1] <= y[i]) & (bins[1:] > y[i]))[0][0])
            zIndex = int(np.where((bins[:-1] <= z[i]) & (bins[1:] > z[i]))[0][0])
            # img[xIndex,yIndex,zIndex] = 1
            img[
                max(0, xIndex - NStepsCellSize) : min(
                    NPerDim - 1, xIndex + NStepsCellSize
                ),
                max(0, yIndex - NStepsCellSize) : min(
                    NPerDim - 1, yIndex + NStepsCellSize
                ),
                max(0, zIndex - NStepsCellSize) : min(
                    NPerDim - 1, zIndex + NStepsCellSize
                ),
            ] = 1
            count += 1
        except IndexError:
            continue
    print(count)
    print(pos.shape)

    originalImg = deepcopy(img)
    success = False
    while success == False:
        img = deepcopy(originalImg)
        try:
            img = gaussian_filter(img, sigma=sigma)
            img = img > threshold
            #    return img
            # plt.imshow(img[:,:,int(0.5)*NPerDim])
            # plt.show()

            verts, faces, normals, values = measure.marching_cubes(img, 0)
            marchingCubeInfo = (verts, faces, normals, values)
            success = True

        except RuntimeError:
            threshold -= 0.1 * threshold
            if threshold <= 0:
                threshold = 0.5
                sigma -= 0.1 * sigma
            success = False

    points = np.zeros((verts.shape), dtype=float)
    for i in range(3):
        points[:, i] = bins[verts.astype(int)[:, i]] - 0.5 * offset
        points[:, i] -= np.mean(points[:, i])
    if returnMCInfo == True:
        return [points, marchingCubeInfo]
    else:
        return points


def get_sphericals(points, retArray=True):
    phis = np.zeros(points.shape[0], dtype=float)
    # thetas = np.zeros(points.shape[0],dtype=float)
    # Rs = np.zeros(points.shape[0],dtype=float)

    Rs = np.sqrt(points[:, 0] ** 2 + points[:, 1] ** 2 + points[:, 2] ** 2)
    thetas = np.arccos(points[:, 2] / Rs)

    indices = np.where(points[:, 0] > 0)[0]
    phis[indices] = np.arctan(points[indices, 1] / points[indices, 0])

    indices = np.where((points[:, 0] < 0) & (points[:, 1] >= 0))[0]
    phis[indices] = np.arctan(points[indices, 1] / points[indices, 0]) + np.pi

    indices = np.where((points[:, 0] < 0) & (points[:, 1] < 0))[0]
    phis[indices] = np.arctan(points[indices, 1] / points[indices, 0]) - np.pi

    indices = np.where((points[:, 0] == 0) & (points[:, 1] > 0))[0]
    phis[indices] = 0.5 * np.pi

    indices = np.where((points[:, 0] == 0) & (points[:, 1] < 0))[0]
    phis[indices] = -0.5 * np.pi

    indices = np.where((points[:, 0] == 0) & (points[:, 1] == 0))[0]
    print(indices.size)

    if retArray == True:
        spoints = np.zeros((thetas.size, 3), dtype=float)
        spoints[:, 0] = thetas
        spoints[:, 1] = phis
        spoints[:, 2] = Rs
        return spoints
    else:
        return (thetas, phis, Rs)


def get_cartesians(sphericals):
    cartesians = np.zeros(sphericals.shape, dtype=float)
    cartesians[:, 0] = (
        sphericals[:, 2] * np.sin(sphericals[:, 0]) * np.cos(sphericals[:, 1])
    )
    cartesians[:, 1] = (
        sphericals[:, 2] * np.sin(sphericals[:, 0]) * np.sin(sphericals[:, 1])
    )
    cartesians[:, 2] = sphericals[:, 2] * np.cos(sphericals[:, 0])
    return cartesians


def standardize_data(cmp_dict: dict, featureList, timestep, filename: str = ""):
    """
    Calculate the standardization factors from cmp_dict

    Parameters

    ----------

    cmp_dict : Dictionary
        Key: datahandler_ids (tuple[int,int])
        Value: feature_dict (Dictionary)
            Key: 'feature' (str)
            Value: timestep_dict (Dictionary)
                Key: 'Timestep' (str)
                Value: Feature distance (float)
    Dict containing comparisons between datahandlers for all features.


    """

    standardization_factors = {feature: [] for feature in featureList}
    key_0 = list(cmp_dict.keys())[0]
    for j, feature in enumerate(featureList):
        if cmp_dict[key_0][j]["feature"] == feature:
            dists = np.array([cmp_dict[key][j]["distance"] for key in cmp_dict.keys()])
        else:
            features = [cmp_dict[key_0][entry]["feature"] for entry in range(len(featureList))]
            index = np.where(features == feature)[0]
            dists = np.array([cmp_dict[key][index]["distance"] for key in cmp_dict.keys()])
        meanDist = np.mean(dists)
        stdDist = np.std(dists)
        dists[:] -= meanDist
        dists[:] /= stdDist
        distShift = np.abs(np.min(dists))
        standardization_factors[feature] = (meanDist, stdDist, distShift)

    if len(filename) > 0 and isinstance(filename, str):
        with open(filename, "w") as f:
            json.dump(standardization_factors, f)

    return standardization_factors


def cost(lambdas: np.ndarray, D_intras, D_inters):
    """
    Calculate the coupling factors for the calculation of the overall deviation score from the datahandler comparison data in cmp_dict

    Parameters

    ----------

    cmp_dict : Dictionary
        Key: datahandler_ids (tuple[int,int])
        Value: feature_dict (Dictionary)
            Key: 'feature' (str)
            Value: timestep_dict (Dictionary)
                Key: 'Timestep' (str)
                Value: Feature distance (float)
    Dict containing comparisons between datahandlers for all features.

    id_grouping : Dictionary
        Key : group_name (str)
        Value : id_list (list)
    Dict containing the assignment of datahandler ids to groups, e.g. replicates, simulated phenotypes etc.
    """

    D_intra = 0.0
    D_inter = 0.0
    sol = 0.0
    for j, feature in enumerate(D_intras.keys()):
        D_intra += lambdas[j] * D_intras[feature]
        D_inter += lambdas[j] * D_inters[feature]
    #    sol += lambdas[j]*D_intras[feature] / D_inters[feature]#FORMULATE IT AS MINIMIZATION PROBLEM

    # return sol
    return D_intra - D_inter  # FORMULATE IT AS MINIMIZATION PROBLEM


def fit_lambdas(
    cmp_dict,
    featureList,
    standardization_factors,
    timestep,
    filename: str = "",
    activate_shift_dist: bool = True,
):
    """
    Calculate the coupling factors for the calculation of the overall deviation score from the datahandler comparison data in cmp_dict

    Parameters

    ----------


    cmp_dict : Dictionary
        Key: datahandler_ids and replicate numbers (tuple[int,int,int,int])
        Value: feature_dict (Dictionary)
            Key: feature_name (str)
            Value: timestep_dict (Dictionary)
                Key: 'Timestep' (str)
                Value: Feature distance (float)
    Dict containing comparisons between datahandlers for all features.

    id_grouping : Dictionary
        Key : group_name (str)
        Value : id_list (list)
    Dict containing the assignment of datahandler ids to groups, e.g. replicates, simulated phenotypes etc.
    """

    D_intras = {}
    D_inters = {}

    for j, feature in enumerate(featureList):
        keys = np.array([key for key in cmp_dict.keys()])
        f1 = keys[:, 0]
        r1 = keys[:, 1]
        f2 = keys[:, 2]
        r2 = keys[:, 3]
        key_0 = list(cmp_dict.keys())[0]
        if cmp_dict[key_0][j]["feature"] == feature:
            dists = np.array([cmp_dict[key][j]["distance"] for key in cmp_dict.keys()])
        else:
            features = [cmp_dict[key_0][entry]["feature"] for entry in range(len(featureList))]
            index = np.where(features == feature)[0]
            dists = np.array([cmp_dict[key][index]["distance"] for key in cmp_dict.keys()])

        meanDist, stdDist, distShift = standardization_factors[feature]
        dists -= meanDist
        dists /= stdDist
        if activate_shift_dist:
            dists += distShift
        D_intra = 0.0
        D_inter = 0.0

        for f in np.unique(f1):
            indices = np.where((f1 == f) & (f2 == f))[0]
            if indices.size > 0:
                D_intra += np.sum(dists[indices])
            indices = np.where((f1 == f) & (f2 != f))[0]
            if indices.size > 0:
                D_inter += np.sum(dists[indices])

        #        for replicates_1 in id_grouping.values():
        #                indices = np.where((np.isin(f1[:], replicates_1) == True)  & (np.isin(f2[:], replicates_1)) == True)[0] #Same phenotype
        #                if indices.size > 0:
        #                    D_intra += np.sum(dists[indices])
        #                indices = np.where((np.isin(f1[:], replicates_1) == True)  & (np.isin(f2[:], replicates_1)) == False)[0] #Different phenotype
        #                if indices.size > 0:
        #                    D_inter += np.sum(dists[indices])

        D_intras[feature] = deepcopy(D_intra)
        D_inters[feature] = deepcopy(D_inter)

    initLambdas = np.sqrt(1 / len(featureList)) * np.ones(
        len(featureList), dtype=float
    )  # Set initial condition for lambdas

    # Constraint that one minus the sum of all variables**2 must be zero
    cons = {
        "type": "eq",
        "fun": lambda initLambdas: 1 - np.sum(initLambdas * initLambdas),
    }

    # Minimized function cost
    res = minimize(
        cost,
        initLambdas,
        args=(D_intras, D_inters),
        bounds=[(0, 1) for i in range(len(featureList))],
        constraints=cons,
    )

    if res.success == False:
        raise ValueError("Optimization unsuccessful!")

    lambdaDict = {}
    for i, key in enumerate(featureList):
        lambdaDict[key] = res["x"][i]

    if len(filename) > 0 and isinstance(filename, str):
        with open(filename, "w") as f:
            json.dump(lambdaDict, f)

    return lambdaDict
