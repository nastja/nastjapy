from copy import deepcopy
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from nastja.DataHandler import DataHandler
from nastja import feature_comparison
from nastja import metrics
from scipy.stats import wasserstein_distance
import pickle


class PhaseSpaceVisualizer:
    simDir: str = ""
    outDir: str = ""
    dhDict: dict = {}
    paramFile: np.array = np.array([])
    tMax: int = 249
    timestepRange: tuple = (249, 250, 1)
    scale_factor: float = 1.0
    loadFromH5: bool = True
    loadFromPickle: bool = False

    xParam: int = 1  # Parameter to be shown on the horizontal
    yParam: int = 2  # Parameter to be shown on the vertical

    paramNames = [
        "Foldernumber",
        "ECM-density",
        "CE-adhesion",
        "CC-adhesion",
        "Cell stiffness",
        "RW Persistence",
        "Motility",
        "ECM degradation rate",
        "Division",
    ]
    paramFixVals: np.array = np.zeros(len(paramNames), dtype=int)

    def __init__(
        self,
        simDir: str = "",
        outDir: str = None,
        scale_factor=1.0,
        timestepRange=(249, 250, 1),
        loadFromH5=True,
        loadFromPickle=False,
    ):
        assert os.path.isdir(simDir), 'Specified path "%s" is not a directory' % simDir

        self.simDir = simDir
        if outDir == None:
            self.outDir = simDir
        else:
            self.outDir = outDir
        self.scale_factor = scale_factor
        assert os.path.isfile(
            "%sfolder_config_key.txt" % self.simDir
        ), "No folder config key file found in simDir"

        self.paramFile = param_file = np.loadtxt(
            "%sfolder_config_key.txt" % self.simDir
        )
        self.loadFromH5 = loadFromH5
        self.loadFromPickle = loadFromPickle
        self.timestepRange = timestepRange

    def addDH(self, folderNumber, timestepRange=(248, 249, 1)):
        if self.loadFromPickle == True:
            if os.path.isfile(
                "%s/%05d/out0/datahandler.p" % (self.simDir, folderNumber)
            ):
                with open(
                    "%s/%05d/out0/datahandler.p" % (self.simDir, folderNumber), "rb"
                ) as f:
                    dh = pickle.load(f)
                self.dhDict.update({folderNumber: deepcopy(dh)})
            else:
                self.dhDict[folderNumber] = None
        elif self.loadFromH5 == True:
            try:
                self.dhDict.update(
                    {
                        folderNumber: DataHandler(
                            "%s/%05d/out0/datahandler.h5" % (self.simDir, folderNumber),
                            timestep_range=self.timestepRange,
                            scale_factor=self.scale_factor,
                        )
                    }
                )
            except FileNotFoundError:
                self.dhDict[folderNumber] = None
            except KeyError:
                self.dhDict[folderNumber] = None
        else:
            try:
                self.dhDict.update(
                    {
                        folderNumber: DataHandler(
                            "%s/%05d/out0" % (self.simDir, folderNumber),
                            timestep_range=self.timestepRange,
                            scale_factor=self.scale_factor,
                        )
                    }
                )
            except FileNotFoundError:
                self.dhDict[folderNumber] = None

    def removeDH(self, folderNumber):
        del self.dhDict[folderNumber]

    def clearFrames(self):
        self.dhDict = {}

    def collectFrames(self, folderNumbers, clearDict=False):
        if clearDict == True:
            self.clearFrames()

        for folderNumber in folderNumbers:
            try:
                self.addDH(folderNumber)
            except IndexError:
                pass
            except FileNotFoundError:
                pass

    def printFixVals(self):
        paramFixVals = self.makeParamCombination()
        for i, val in enumerate(paramFixVals):
            if i != 0 and i != self.xParam and i != self.yParam:
                print("%s = %1.2f" % (self.paramNames[i], val))

    def makeGridFig(
        self,
        filename,
        plotShape: tuple = None,
        paramsInTitle: bool = False,
        folderNrInTitle: bool = True,
        show: bool = True,
        save: bool = False,
        supAxes: bool = False,
        proj_axis: int = 3,
        glMode: int = 0,
        hvSpace=-0.1,
        fontsize: int = 20,
    ):
        assert type(filename) == str, "filename must be a string"
        assert filename[-4:] == ".png", "currently supports only png format"

        self.printFixVals()

        plt.rcParams.update({"font.size": 10})
        NFrames = len(list(self.dhDict.keys()))
        NFigsPerDim = int(np.sqrt(NFrames))
        if NFrames % NFigsPerDim > 0:
            NFigsPerDim += 1

        xParamVals = np.unique(self.paramFile[:, self.xParam])
        yParamVals = np.unique(self.paramFile[:, self.yParam])
        NRows = NFigsPerDim
        NCols = NFigsPerDim

        if plotShape != None:
            print(
                "Unequal axes detected. Plot shape will be (%d,%d)"
                % (plotShape[0], plotShape[1])
            )
            NRows, NCols = plotShape
        fig, axs = plt.subplots(
            NRows,
            NCols,
            figsize=(self.scale_factor * NCols * 2.5, self.scale_factor * NRows * 2.5),
        )

        supTitleStr = ""
        if paramsInTitle == True:
            supTitleStr += "%s (horizontal) vs %s (vertical)" % (
                self.paramNames[self.xParam],
                self.paramNames[self.yParam],
            )
        fig.suptitle(supTitleStr, fontsize=fontsize)
        if supAxes == True:
            fig.supxlabel(self.paramNames[self.xParam], fontsize=fontsize)
            fig.supylabel(self.paramNames[self.yParam], fontsize=fontsize)
        xIndex = 0
        yIndex = 1
        if proj_axis == 1:
            xIndex = 1
            yIndex = 2
        elif proj_axis == 2:
            yIndex = 2

        i, row, col = (0, 0, 0)

        for key, val in self.dhDict.items():
            gl = np.full((1, 3), -999)
            ngl = np.full((1, 3), -999)
            if val != None:
                val.assign_gaslikes_at_time(self.tMax, mode=glMode)
                gl = val.get_positions_at_time(self.tMax, extraction_mode=3)
                ngl = val.get_positions_at_time(self.tMax, extraction_mode=2)
            params = self.getParams(key)

            if plotShape == None:
                row = min(i // NFigsPerDim, NFigsPerDim - 1)
                col = i % NFigsPerDim
            else:
                row = np.where(params[self.yParam] == yParamVals)[0][0]
                col = np.where(params[self.xParam] == xParamVals)[0][0]

            #            if row != 0 or col != 0:
            axs[row, col].axis("off")
            axs[row, col].scatter(ngl[:, xIndex], ngl[:, yIndex], s=7.5, c="blue")
            if gl.shape != ngl.shape:
                axs[row, col].scatter(gl[:, xIndex], gl[:, yIndex], s=7.5, c="red")
            elif np.any(ngl[:, xIndex] != gl[:, xIndex]) or np.any(
                ngl[:, yIndex] != gl[:, yIndex]
            ):
                axs[row, col].scatter(gl[:, xIndex], gl[:, yIndex], s=7.5, c="red")
            axs[row, col].set_xlim((0, self.scale_factor * 400))
            axs[row, col].set_ylim((0, self.scale_factor * 400))
            axs[row, col].set_xticks([])
            titleStr = ""

            if folderNrInTitle == True:
                titleStr += "Simu %05d," % (key)
            if paramsInTitle == True:
                titleStr += "%1.2f, %1.2f" % (params[self.xParam], params[self.yParam])
            axs[row, col].set_title(titleStr)
            i += 1
        fig.subplots_adjust(wspace=hvSpace, hspace=hvSpace)
        if save == True:
            plt.savefig(self.outDir + filename)
        if show == True:
            plt.show()
        else:
            plt.close()

    def updateParamFixVals(self, paramList):
        for i, val in enumerate(paramList):
            possibleVals = np.unique(self.paramFile[:, i])
            assert (
                val >= 0 and val < possibleVals.size
            ), "Param %d (%s) can only have a value >= 0 and < %d" % (
                i,
                self.paramNames[i],
                possibleVals.size,
            )
            self.paramFixVals[i] = val

    def makeParamCombination(self):
        paramArray = np.zeros(self.paramFixVals.size, dtype=float)
        for i, val in enumerate(self.paramFixVals):
            possibleVals = np.unique(self.paramFile[:, i])
            paramArray[i] = possibleVals[val]
        return paramArray

    def loadPhaseSpace(self, xParam, yParam):
        self.clearFrames()

        assert xParam >= 0 and xParam < self.paramFile.shape[1], (
            "xParam needs to be between 0 and %d" % self.paramFile.shape[1]
        )
        assert yParam >= 0 and yParam < self.paramFile.shape[1], (
            "yParam needs to be between 0 and %d" % self.paramFile.shape[1]
        )

        self.xParam = xParam
        self.yParam = yParam
        xParamVals = np.unique(self.paramFile[:, xParam])
        yParamVals = np.unique(self.paramFile[:, yParam])
        paramCombination = self.makeParamCombination()

        foldersToAdd = np.zeros(xParamVals.size * yParamVals.size, dtype=int)
        for y in yParamVals:
            for x in xParamVals:
                paramCombination[self.xParam] = x
                paramCombination[self.yParam] = y
                cmpArr = self.paramFile[:, 1] == paramCombination[1]
                for i in range(2, self.paramFile.shape[1]):
                    cmpArr = cmpArr & (self.paramFile[:, i] == paramCombination[i])
                folderIndex = np.where(cmpArr == True)[0]

                assert (
                    folderIndex.size == 1
                ), "In function showPhaseSpace: Two fitting positions were found for the given param combination!"
                self.addDH(folderIndex[0])

    def showPhaseSpace(self, xParam, yParam, *args, **kwargs):
        self.loadPhaseSpace(xParam=xParam, yParam=yParam)
        xParamVals = np.unique(self.paramFile[:, xParam])
        yParamVals = np.unique(self.paramFile[:, yParam])
        self.makeGridFig(*args, plotShape=(yParamVals.size, xParamVals.size), **kwargs)

    #        print(xParamVals)
    #        print(yParamVals)

    def showSingleFolder2DAt(
        self,
        t,
        folderNumber,
        outFolder,
        save=False,
        show=True,
        glMode=0,
        nglColor="blue",
        glColor="red",
        minAx=0,
        maxAx=400,
    ):
        self.addDH(folderNumber, timestepRange=(t, t + 1, 1))
        self.dhDict[folderNumber].assign_gaslikes_at_time(t, mode=glMode)
        fig, axs = plt.subplots(1, 2, figsize=(22, 10))
        self.dhDict[folderNumber].assign_gaslikes_at_time(t, mode=0)
        pos = self.dhDict[folderNumber].get_positions_at_time(t, extraction_mode=2)
        axs[0].scatter(pos[:, 0], pos[:, 1], label="%d" % folderNumber, c=nglColor)
        axs[1].scatter(pos[:, 0], pos[:, 2], label="%d" % folderNumber, c=nglColor)
        ngl_shape = pos.shape
        pos = self.dhDict[folderNumber].get_positions_at_time(t, extraction_mode=3)
        if pos.shape != ngl_shape:
            axs[0].scatter(pos[:, 0], pos[:, 1], label="%d" % folderNumber, c=glColor)
            axs[1].scatter(pos[:, 0], pos[:, 2], label="%d" % folderNumber, c=glColor)
        for i in range(2):
            axs[i].set_xlim((minAx, maxAx))
            axs[i].set_ylim((minAx, maxAx))
        if save == True:
            if os.path.isdir(outFolder + "/%05d" % folderNumber) == False:
                os.mkdir(outFolder + "/%05d" % folderNumber)
            plt.savefig(
                outFolder + "/%05d/projection_frame_%05d.png" % (folderNumber, t)
            )
        if show == True:
            plt.show()
        else:
            plt.close()

    def showSingleFolder3DAt(
        self, t, folderNumber, outFolder, save=False, glMode=0, **kwargs
    ):
        self.addDH(folderNumber, timestepRange=(t, t + 1, 1))
        self.dhDict[folderNumber].assign_gaslikes_at_time(t, mode=glMode)
        fileName = outFolder + "/%05d/frame3D_%05d.png" % (folderNumber, t)
        if save == False:
            fileName = None
        self.dhDict[folderNumber].show_3d_at_time(t, filename=fileName, **kwargs)

    def makeMovieFrames2D(self, folderNumber, outFolder, **kwargs):
        for t in range(self.tMax + 1):
            self.showSingleFolder2DAt(
                t, folderNumber, outFolder, save=True, show=False, **kwargs
            )

    def makeMovieFrames3D(self, folderNumber, **kwargs):
        for t in range(self.tMax + 1):
            self.showSingleFolder3DAt(t, folderNumber, save=True, show=False, **kwargs)

    def getParams(self, folderNumber):
        return self.paramFile[folderNumber, :]

    def load_feature(self, foldernumber, featurename):
        simdir = args.data_path + "/%05d/out0" % foldernumber
        with open(simdir + "/feature_dict.p", "rb") as f:
            feature_dict = pickle.load(f)
        return feature_dict[featurename]

    def compareDhs(self, dh1, dh2, key):
        if type(dh1) == int:
            self.addDH(dh1)
            dh1 = self.dhDict[dh1]
        if type(dh2) == int:
            self.addDH(dh2)
            dh2 = self.dhDict[dh2]
        metricVals = np.zeros(len(list(dh1.data.keys())), dtype=float)
        for tp in dh1.data.keys():
            t1 = int(tp)
            #            for t2 in dh2.data.keys():
            try:
                metricVals[t1] = feature_comparison.METRICS_FOR_FEATURES[key](
                    dh1.features[key][t1], dh2.features[key][t1]
                )
            except KeyError:
                if key == "tumor_area":
                    metricVals[t1] = metrics.mse(
                        dh1.features[key][t1], dh2.features[key][t1]
                    )
                else:
                    plotFeature = False
                    continue
        return metricVals

    def showDhComparison(self, refDh, dhList, refSimuName="0", fontsize=20):
        assert len(dhList) > 0, "dhList must contain at least 1 folderNumber"
        plt.rcParams.update({"font.size": fontsize})
        NFeaturesMax = len(list(refDh.features.keys()))
        NPerDim = int(np.sqrt(NFeaturesMax)) + 1
        fig, axs = plt.subplots(NPerDim, NPerDim, figsize=(11 * NPerDim, 10 * NPerDim))
        count = 0
        fig.suptitle(("Comparing simu %s with a list of simus" % refSimuName))
        for folderNumber in dhList:
            print("Adding folder %05d" % folderNumber)
            self.addDH(folderNumber)
        for key in refDh.features.keys():
            print(key)
            row = count // 3
            column = count % 3
            axs[row, column].set_xlabel(("Timestep"))
            axs[row, column].set_ylabel(("Metric distance"))
            axs[row, column].set_title(("Comparing feature %s" % key))
            try:
                for folderNumber in dhList:
                    dh2 = self.dhDict[folderNumber]
                    if dh2 != None:
                        if key in dh2.features.keys():
                            cmp = self.compareDhs(refDh, dh2, key)
                            axs[row, column].plot(cmp, label="Simu %05d" % folderNumber)

                axs[row, column].legend()
            except ValueError:
                continue
            count += 1
        plt.show()

    def showPhaseSpaceComparison(
        self,
        xParam,
        yParam,
        outFolder,
        referenceFolder=None,
        plotFeature=True,
        show=False,
        dump_matrices=False,
    ):
        self.loadPhaseSpace(xParam, yParam)

        matrixDict = {}
        xParamVals = np.unique(self.paramFile[:, xParam])
        yParamVals = np.unique(self.paramFile[:, yParam])
        plt.rcParams.update({"font.size": 20})
        count = 0
        keys = list(self.dhDict.keys())

        midXParam = xParamVals[int(0.5 * xParamVals.size)]
        midYParam = yParamVals[int(0.5 * yParamVals.size)]
        reducedParamVals = self.paramFile[keys, :]
        dh1KeyIndex = np.where(
            (reducedParamVals[:, xParam] == midXParam)
            & (reducedParamVals[:, yParam] == midYParam)
        )[0]
        dh1 = self.dhDict[keys[dh1KeyIndex[0]]]
        if referenceFolder != None:
            print("Using folder %05d as reference" % referenceFolder)
            self.addDH(referenceFolder, timestepRange=self.timestepRange)
            dh1 = self.dhDict[referenceFolder]
        metricMatrix = np.zeros((yParamVals.size, xParamVals.size), dtype=float)

        for key in dh1.features.keys():
            matrixDict[key] = {}
            print(key)
            metricMatrix[:, :] = 0.0
            for i, x in enumerate(xParamVals):
                print(i)
                for j, y in enumerate(yParamVals):
                    try:
                        dh2KeyIndex = np.where(
                            (reducedParamVals[:, xParam] == x)
                            & (reducedParamVals[:, yParam] == y)
                        )[0]
                        dh2 = self.dhDict[keys[dh2KeyIndex[0]]]

                        # metricMatrix = np.zeros((yParamVals.size,xParamVals.size),dtype=float)
                        if dh1 != None and dh2 != None:
                            plotFeature = True
                            metricVals = self.compareDhs(dh1, dh1, key)
                            metricMatrix[j, i] = np.mean(metricVals)
                            # metricMatrix[j,i] = np.mean(metricVals)
                    except KeyError:
                        continue
                    except ValueError:
                        continue
            if plotFeature == True:
                if dump_matrices == True:
                    matrixDict[key][(xParam, yParam)] = deepcopy(metricMatrix)
                # fig,axs = plt.subplots(1,1,figsize=(12,11))
                plt.figure(figsize=(12, 11))
                # fig.suptitle(('Comparing Param %d with param %d' % (xParam,yParam)))
                # row = count // 3
                # column = count% 3
                plt.title(("Comparing feature %s" % key))
                plt.imshow(
                    metricMatrix,
                    extent=[
                        xParamVals[0],
                        xParamVals[-1],
                        yParamVals[0],
                        yParamVals[-1],
                    ],
                    origin="lower",
                    aspect="auto",
                )
                plt.xlabel(self.paramNames[xParam])
                plt.ylabel(self.paramNames[yParam])
                if referenceFolder != None:
                    plt.colorbar(label="Distance from reference simu")
                else:
                    plt.colorbar(label="Distance from middle combination")
                # axs[row,column].imshow(metricMatrix,extent=[xParamVals[0],xParamVals[-1],yParamVals[0],yParamVals[-1]])
                # count+=1
                plt.savefig(
                    outFolder
                    + "/xParam_%d_yParam_%d_feature_%s.png" % (xParam, yParam, key)
                )
                if show == True:
                    plt.show()
                else:
                    plt.close()
        if dump_matrices == True:
            with open(
                outFolder + "/metric_matrices_xParam_%d_yParam_%d.p" % (xParam, yParam),
                "wb",
            ) as f:
                pickle.dump(matrixDict, f)
