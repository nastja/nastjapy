import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from matplotlib.colors import LogNorm

import ipyvolume as ipv
from ipywidgets import FloatSlider, jslink, VBox, interactive
import matplotlib.colors as mcolors

from .utils import dist

preset_colors = mcolors.CSS4_COLORS
color_names = list(preset_colors.keys())


def update_plot(xmin, ymin, zmin, xmax, ymax, zmax):
    ipv.xlim(xmin, xmax)
    ipv.ylim(ymin, ymax)
    ipv.zlim(zmin, zmax)
    ipv.set_box_aspect_data()


def show_pointclouds_3d(
    pointclouds_to_show:list[np.ndarray],
    labels:list[str],
    colors=["red", "blue"],
    ax=None,
    legend=True,
    xmin=-400,
    xmax=400,
    ymin=-400,
    ymax=400,
    zmin=-400,
    zmax=400,
    theta=20,
    phi=45,
    filename=None,
    fontsize=20,
    s=2,
    figsizeScale=1.0,
    axisScale=1.0,
    use_ipyvolume=True,
    auto_scale_axes=True,
):
    if auto_scale_axes:
        xmin = 0.0
        ymin = 0.0
        zmin = 0.0
        xmax = 0.0
        ymax = 0.0
        zmax = 0.0
        for pointcloud in pointclouds_to_show:
            if isinstance(pointcloud, np.ndarray) == False:
                continue
            x = pointcloud[:, 0]
            y = pointcloud[:, 1]
            z = pointcloud[:, 2]

            xmin = min(xmin, np.min(x.astype(float)))
            ymin = min(ymin, np.min(y.astype(float)))
            zmin = min(zmin, np.min(z.astype(float)))
            xmax = max(xmax, np.max(x.astype(float)))
            ymax = max(ymax, np.max(y.astype(float)))
            zmax = max(zmax, np.max(z.astype(float)))

    if use_ipyvolume:
        ipv_fig = ipv.figure()
        size = FloatSlider(min=0, max=s, step=s / 20)

        x_min_slider = FloatSlider(value=xmin, min=xmin, max=xmax, step=xmax / 100)
        y_min_slider = FloatSlider(value=ymin, min=ymin, max=ymax, step=ymax / 100)
        z_min_slider = FloatSlider(value=zmin, min=zmin, max=zmax, step=zmax / 100)
        x_max_slider = FloatSlider(value=xmax, min=xmin, max=xmax, step=xmax / 100)
        y_max_slider = FloatSlider(value=ymax, min=ymin, max=ymax, step=ymax / 100)
        z_max_slider = FloatSlider(value=zmax, min=zmin, max=zmax, step=zmax / 100)

        overall_lim_slider = interactive(
            update_plot,
            xmin=x_min_slider,
            ymin=y_min_slider,
            zmin=z_min_slider,
            xmax=x_max_slider,
            ymax=y_max_slider,
            zmax=z_max_slider,
        )
        for i, pointcloud in enumerate(pointclouds_to_show):
            if isinstance(pointcloud, np.ndarray) == False:
                continue
            x = pointcloud[:, 0]
            y = pointcloud[:, 1]
            z = pointcloud[:, 2]

            pointcloud_plot = ipv.scatter(
                x,
                y,
                z,
                marker="sphere",
                description=str(labels[i]) if i < len(labels) else f"Cloud {i}",
                color=colors[i]
                if i < len(colors)
                else color_names[i % len(color_names)],
                size=s,
            )

        from IPython.display import display

        display(VBox([ipv.gcc(), overall_lim_slider]))

    else:
        plt.rcParams.update({"font.size": fontsize})
        if ax == None:
            fig = plt.figure(figsize=(figsizeScale * 20, figsizeScale * 10))
            ax = fig.add_subplot(111, projection="3d")

        if auto_scale_axes == False:
            ax.set_xlim((axisScale * xmin, axisScale * xmax))
            ax.set_ylim((axisScale * ymin, axisScale * ymax))
            ax.set_zlim((axisScale * zmin, axisScale * zmax))

        for i, pointcloud in enumerate(pointclouds_to_show):
            x = pointcloud[:, 0]
            y = pointcloud[:, 1]
            z = pointcloud[:, 2]
            print("s = ", s)
            ax.scatter(
                x,
                y,
                z,
                s=s,
                c=colors[i] if i < len(colors) else color_names[i % len(color_names)],
                label=labels[i] if i < len(labels) else f"Cloud {i}",
            )

        ax.view_init(theta, phi)
        if legend == True:
            plt.legend()
        if filename == None:
            if ax == None:
                plt.show()
        else:
            plt.savefig(filename)
            plt.close()
    return None


def show_gas_and_non_gas_2d(
    gaslikes=None,
    non_gaslikes=None,
    ax=None,
    legend=True,
    highlight_gaslikes=True,
    xmin=-400,
    xmax=400,
    ymin=-400,
    ymax=400,
    proj_axis="z",
    filename=None,
    fontsize=20,
    gaslike_color="red",
    non_gaslike_color="blue",
    s=10,
    figsizeScale=1.0,
    axisScale=1.0,
    visibleAxes: bool = True,
):
    plt.rcParams.update({"font.size": fontsize})
    if ax == None:
        fig, ax = plt.subplots(figsize=(figsizeScale * 10, figsizeScale * 10))
    ax.set_xlim((axisScale * xmin, axisScale * xmax))
    ax.set_ylim((axisScale * ymin, axisScale * ymax))

    axes = {"x": (1, 2), "y": (0, 2), "z": (0, 1)}

    xAxis, yAxis = axes[proj_axis]

    if type(non_gaslikes) != type(None):
        x = non_gaslikes[:, xAxis]
        y = non_gaslikes[:, yAxis]
        ax.scatter(x, y, color=non_gaslike_color, label="non gaslikes", s=s)
    if type(gaslikes) != type(None):
        x = gaslikes[:, xAxis]
        y = gaslikes[:, yAxis]
        ax.scatter(x, y, color=gaslike_color, label="gaslikes", s=s)

    if visibleAxes == False:
        ax.axis("off")

    if legend == True:
        plt.legend()
    if filename == None:
        if ax == None:
            plt.show()
        else:
            return ax
    else:
        plt.savefig(filename)
        plt.close()
    return None


def show_all_aggregations(
    gaslikes=None,
    liquids=None,
    solids=None,
    legend=True,
    xmin=0,
    xmax=250,
    ymin=0,
    ymax=250,
    zmin=0,
    zmax=250,
    theta=20,
    phi=45,
    filename=None,
    fontsize=20,
    gaslike_color="orange",
    liquid_color="turquoise",
    solid_color="blue",
):
    plt.rcParams.update({"font.size": fontsize})
    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_subplot(111, projection="3d")
    ax.set_xlim((xmin, xmax))
    ax.set_ylim((ymin, ymax))
    ax.set_zlim((zmin, zmax))

    if type(solids) != type(None):
        ax.scatter(
            solids[:, 0],
            solids[:, 1],
            solids[:, 2],
            color=solid_color,
            label="Solid-likes",
        )
    if type(liquids) != type(None):
        ax.scatter(
            liquids[:, 0],
            liquids[:, 1],
            liquids[:, 2],
            color=liquid_color,
            label="Liquid-likes",
        )
    if type(gaslikes) != type(None):
        ax.scatter(
            gaslikes[:, 0],
            gaslikes[:, 1],
            gaslikes[:, 2],
            color=gaslike_color,
            label="Gas-likes",
        )

    ax.view_init(theta, phi)
    if legend == True:
        plt.legend()
    if filename == None:
        plt.show()
    else:
        plt.savefig(filename)
        plt.close()


def show_3d(
    x,
    y,
    z,
    ax=None,
    label="",
    legend=True,
    xmax=250,
    ymax=250,
    zmax=250,
    theta=20,
    phi=45,
    fontsize=20,
):
    plt.rcParams.update({"font.size": fontsize})
    if not ax:
        fig = plt.figure(figsize=(20, 10))
        ax = fig.add_subplot(111, projection="3d")
    ax.set_xlim((-xmax, xmax))
    ax.set_ylim((-ymax, ymax))
    ax.set_zlim((-zmax, zmax))
    ax.scatter(x, y, z, color="black", label=label)
    ax.view_init(theta, phi)
    if legend == True:
        plt.legend()
    plt.show()


def show_paths(
    gaslikes,
    non_gaslikes,
    path_dict,
    final_point=np.array([0, 0, 0]),
    theta=20,
    phi=20,
    filename=None,
    size=2,
    title="",
    loc=3,
    fontsize=20,
):
    gas_color = "black"
    non_gas_color = "gray"
    plt.rcParams.update({"font.size": fontsize})
    fig = plt.figure(figsize=(20, 10))
    ax = fig.add_subplot(111, projection="3d")
    ax.set_title(title)
    ax.set_xlabel(("\n x (µm)"))
    ax.set_ylabel(("\n y (µm)"))
    ax.set_zlabel(("\n z (µm)"))
    if type(non_gaslikes) != type(None):
        ax.scatter(
            non_gaslikes[:, 0],
            non_gaslikes[:, 1],
            non_gaslikes[:, 2],
            color=non_gas_color,
            s=size,
        )
    if type(gaslikes) != type(None):
        ax.scatter(
            gaslikes[:, 0], gaslikes[:, 1], gaslikes[:, 2], color=gas_color, s=size
        )
    # x = positions[:,0]
    # y = positions[:,1]
    # z = positions[:,2]
    # ax.scatter(x,y,z,color='gray',s=size)
    for value in path_dict.values():
        # final_point = np.array(value)[-1,:]
        x = np.array(value)[:, 0]
        y = np.array(value)[:, 1]
        z = np.array(value)[:, 2]
        ax.plot(x, y, z, linewidth=0.5 * size)
        ax.scatter(
            final_point[0],
            final_point[1],
            final_point[2],
            color="red",
            label="Path is composed of %d points" % x.size,
            s=size,
        )

    ax.view_init(theta, phi)
    #    ax.legend(loc=loc)
    if filename == None:
        pass
    else:
        plt.savefig(filename)

    plt.show()


def show_averaged_density_over_all_paths(
    positions,
    paths,
    densities,
    center=[0, 0, 0],
    xmax=300,
    ymax=300,
    title="",
    figx=16,
    figy=8,
    fontsize=20,
    log_exp_min=6,
    log_exp_max=4,
    save=False,
    interpolation=True,
    savefilesuffix="",
    reversed=False,
):
    all_dists = [dist(np.array(center), point) for point in positions]
    percentile = np.percentile(all_dists, 95)
    plt.rcParams.update({"font.size": fontsize})
    plt.figure(figsize=(figx, figy))
    plt.title((title))

    point_numbers = [len(densities[j].keys()) for j in range(len(densities.keys()))]
    N_bins_regular = 2 * np.max(point_numbers)
    regular_bins = np.linspace(0, xmax, N_bins_regular)
    N_y = 10
    for i in range(len(densities.keys())):
        try:
            N_y = densities[i][0][1].size
            break
        except KeyError:
            pass
    img_data = np.full((N_y, N_bins_regular), 0.0)
    path_dists = {}
    N_valid_paths = 0
    for i in range(len(densities.keys())):
        try:
            if reversed == True:
                path_dists[i] = [
                    dist(np.array(center), point - np.array(center))
                    for point in reversed(paths[i])
                ]
            else:
                path_dists[i] = [
                    dist(np.array(center), point - np.array(center))
                    for point in paths[i]
                ]
            if path_dists[i][-1] > percentile:
                N_valid_paths += 1
                for k in range(img_data[:, 0].size):
                    if reversed == True:
                        ybins = np.array(
                            [
                                densities[i][len(densities[i].keys()) - 1 - j][1][k]
                                for j in range(len(densities[i].keys()))
                            ]
                        )
                    else:
                        ybins = np.array(
                            [
                                densities[i][j][1][k]
                                for j in range(len(densities[i].keys()))
                            ]
                        )
                    img_data[k, :] += np.interp(regular_bins, path_dists[i], ybins)
        except KeyError:
            pass
        except TypeError:
            pass

    img_data[:, :] += 1e-9
    if N_valid_paths > 0:
        img_data[:, :] /= N_valid_paths
    plt.imshow(
        img_data,
        extent=[0, xmax, 0, ymax],
        aspect="auto",
        norm=LogNorm(vmin=10 ** (-log_exp_min), vmax=10 ** (-log_exp_max)),
    )
    #    plt.errorbar(densities[key][i][j][0],densities[key][i][j][1],densities[key][i][j][2])

    #    plt.xlim((0,xmax))
    #    plt.ylim((0,ymax))
    plt.colorbar(label="Density")
    if interpolation == True:
        plt.xlabel(("Distance from spheroid center (µm)"))
    else:
        plt.xlabel(("Point index"))
    plt.ylabel(("Distance from point center (µm)"))

    # plt.set_(('Density (µm⁻³)'))
    if save == True:
        plt.savefig("density_%s.png" % savefilesuffix)
        plt.close()
    else:
        plt.show()

    plt.figure(figsize=(figx, figy))
    plt.title((title))
    plt.plot(
        [0, np.max([len(path_dists[key]) - 1 for key in path_dists.keys()])],
        [percentile, percentile + 0.001],
        color="red",
        label="95th percentile",
    )
    for value in path_dists.values():
        plt.plot(range(len(value)), value)
        plt.scatter(range(len(value)), value)

    plt.ylim((0, 2 * ymax))
    plt.xlabel(("Point index"))
    plt.ylabel(("Distance of point from spheroid center (µm)"))
    plt.legend()
    if save == True:
        plt.savefig("path_distances_%s.png" % savefilesuffix)
        plt.close()
    else:
        plt.show()


def show_non_gas_fraction(
    feature_dict=None, title="", fontsize=20, label="", filename=None, show=True
):
    plt.rcParams.update({"font.size": fontsize})
    plt.figure(figsize=(20, 10))
    plt.title((title))
    plt.plot(list(feature_dict.keys()), list(feature_dict.values()), label=label)
    plt.xlabel(("Timestep"))
    plt.ylabel(("Non-gaslike fraction"))
    plt.legend()
    plt.ylim((0, 1.05))
    if filename != None:
        plt.savefig(filename)
    if show == True:
        plt.plot()
    else:
        plt.close()


def show_1d_feature(feature_name, feature, filename=None, show=True, ax=None):
    pass


def show_average_radial_displacement(
    feature,
    distance_threshold=300,
    title="",
    fontsize=20,
    label="",
    filename=None,
    show=True,
):
    plt.rcParams.update({"font.size": fontsize})

    averaged_displacement = np.full(250, 0.0)
    std_dev_displacement = np.full(250, 0.0)
    count_features
    for val in feature.values():
        averaged_displacement[:] += val[:]
    averaged_displacement[:] /= len(cell_keys)
    for val in feature.values():
        std_dev_displacement[:] += (val[:] - averaged_displacement[:]) ** 2
    std_dev_displacement[:] /= len(cell_keys)
    std_dev_displacement[:] = np.sqrt(std_dev_displacement[:])
    # TODO color as distance from center


def show_marching_cubes_surface(verts, faces, phi:int=45, theta:int=45, ax_lim: int = 100):
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection="3d")

    # Fancy indexing: `verts[faces]` to generate a collection of triangles
    mesh = Poly3DCollection(verts[faces])
    mesh.set_edgecolor("k")
    ax.add_collection3d(mesh)

    ax.set_xlim(0, ax_lim)
    ax.set_ylim(0, ax_lim)
    ax.set_zlim(0, ax_lim)
    ax.view_init(theta, phi)
    plt.tight_layout()
    plt.show()


def barplot(data_x, data_y, ax=None):
    if ax == None:
        fig, ax = plt.subplots()
    ax.bar(data_x, data_y)


def lineplot(data, ax=None):
    if ax == None:
        fig, ax = plt.subplots()
    ax.plot(data)


def show_deviation_score_comparison(overall_deviation_scores,phenotypes, N_replicates,fig = None, ax = None):
    plot : bool = False
    if ax == None:
            plot = True
            fig,ax = plt.subplots()
    grid = np.zeros((len(phenotypes)*N_replicates,len(phenotypes)*N_replicates),dtype=float)
    for key,deviation_score in overall_deviation_scores.items():
        grid[N_replicates*(key[0])+key[1],N_replicates*(key[2])+key[3]] = deviation_score[0]        
    im = ax.imshow(grid,origin="lower")
    cb = fig.colorbar(im,label = "Deviation score")

    if plot:
        plt.show()