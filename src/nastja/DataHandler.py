"""
DataHandler.py
====================================
Handle data loading and analysis
"""
from enum import Enum
from operator import pos
import os
from glob import glob
import pickle
import json
import scipy.io as sio
import pandas as pd
import numpy as np
import sys
from tables import NaturalNameWarning
import h5py
import warnings

import matplotlib.pyplot as plt
from .io import SimDir, LoadMode
from .utils import (
    extract_cuboid,
    get_args_dict,
    conditional_tqdm,
    dist,
    HDFGroup,
    HDFGroupWithSubgroups,
    HDFToDataframeHandler,
)

from .feature_extraction import (
    get_individual_local_density_function,
    get_central_local_density_function,
    get_individual_velocity_correlation_function,
    get_central_velocity_correlation_function,
    get_volumes,
    get_average_velocity_correlation_function,
    get_average_local_density_function,
    get_radial_local_density_function,
    get_radially_averaged_local_density_function,
    get_radially_averaged_velocity_correlation_function,
    get_path_density,
    extract_gaslikes,
    extract_non_gaslikes,
    assign_gaslikes,
    get_non_gaslike_fraction,
    get_radial_displacement,
    get_tumor_area,
    assign_gaslikes_via_fluidity,
    get_tumor_surface_cloud_MC,
    get_tumor_surface_cloud_orientation,
    get_gaslike_distance,
)
from .feature_visualization import (
    show_3d,
    show_pointclouds_3d,
    show_gas_and_non_gas_2d,
    show_paths,
    show_averaged_density_over_all_paths,
    show_non_gas_fraction,
    show_average_radial_displacement,
    show_marching_cubes_surface,
    barplot,
    lineplot,
)

import functools
import time
import typing
from typing import List, Dict, Tuple, Set, Optional
from mpl_toolkits.mplot3d import Axes3D
from scipy.stats import circmean
from copy import deepcopy


class ExtractionMode(Enum):
    all = [0, 1, 2]
    non_gaslikes = [1, 0]
    solidlikes = [0]
    fluidlikes = [1]
    gaslikes = [2]
    cell_type = [5]


class ShowMode(Enum):
    gas_and_non_gas = [1]
    non_gaslikes = [2]
    gaslikes = [3]
    by_type = [4]






class MatFormats(Enum):
    microspheroid = {
        "multifile": False,
        "mat_structure": {"all_frames": "rottimetracks"},
    }
    macrospheroid = {
        "multifile": True,
        "mat_structure": {"all_cells": "allnuclei", "single_cells": "singlecell"},
    }


def write_metadata(func):
    """Decorator for saving parameters of feature computing function

    Updates the value stored in the datahandler.feature_metadata[func_name] to contain the parameters used by
    by func to compute features

    Parameters:
        func: feature function to be decorated

    Returns:
        decorated Function
    """

    @functools.wraps(func)
    def metadata_wrapper(*args, **kwargs):
        assert (
            type(kwargs["self"]) is DataHandler
        ), "Metadata decorator can only be used on DataHandler Methods."
        feature = func(*args, **kwargs)
        args_dict = get_args_dict(func, args, kwargs)
        args_dict.pop("self", None)
        args_dict.pop("show_progress", None)
        kwargs["self"].feature_metadata[func.__name__] = args_dict
        return feature

    return metadata_wrapper


def write_to_feature_dict(func):
    """Decorator for saving results of feature computing function to datahandler.features

    Parameters:
        func: feature function to be decorated

    Returns:
        decorated Function
    """

    @functools.wraps(func)
    def feature_dict_wrapper(*args, **kwargs):
        feature = func(*args, **kwargs)
        kwargs["self"].features[func.__name__] = feature
        return feature

    return feature_dict_wrapper


class DataHandler:
    """
    A Class to access data from simulations as well as experiments

    Attributes
    ----------
    data_path : str
        filepath to data to be loaded
    data : dict
        stores positions and velocities in pd.DataFrames for each timestep
    features : dict
        Contains data and metadata of all computed features
    feature_metadata : dict
        specifies the parameters used to extract features. Falls back to default values if unspecified
    timestep_range : tuple
        allows to load the trajectory partially by giving start_time, end_time and stride. Defaults to entire trajectory
    keep_original_timestep_numbering : bool
        Only relevant when stride in timestep range not 1. Selects whether to reset timestep numbering.
    mat_format : str | dict
        Select formatting of .mat files. Currently available formats: 1) dict, 2) path to a .json, 3) the members of the MatFormats enum class.
        Available keys in mat_format: "multifile", "file_pattern", "mat_structure"
    shiftToOrigin : bool
        shift all positions to center the spheroid on the origin
    # TODO: shiftPeriodic deprecated?
    shiftPeriodic : bool
        ?
    assign_gaslikes: bool
        determine for each cell, whether it is gas-like or part of the spheroid bulk
    scale_factor: float
        Rescales positions by multiplying with scale_factor
    calc_velocities : bool
        If true, cell velocities will be calculated during data loading

    Methods
    -------
    reload_data
        reloads data from specified data_path
    get_positions_at_time
        Retrieves positional data for given timepoint if available
    get_velocities_at_time
        Retrieves velocity data for given timepoint if available
    get_volumes_at_time
        Retrieves/computes volume data for given timepoint if available
    get_average_local_density_at_time
        Retrieves/computes average local density data for given timepoint if available
     get_individual_local_density_at_time
        Retrieves/computes individual local density data for given timepoint if available
     get_central_local_density_at_time
        Retrieves/computes central local density data for given timepoint if available
    get_average_velocity_correlation_at_time
        Retrieves/computes average velocity correlation data for given timepoint if available
     get_individual_velocity_correlation_at_time
        Retrieves/computes individual velocity correlation data for given timepoint if available
     get_central_velocity_correlation_at_time
        Retrieves/computes central velocity correlation data for given timepoint if available
    """

    def __init__(
        self,
        data_path: str = None,
        data: dict = None,
        timestep_range: tuple = None,
        features: dict = None,
        feature_metadata: dict = None,
        keep_original_timestep_numbering: bool = True,
        mat_format: str | dict = "",
        shiftToOrigin: bool = False,
        shiftPeriodic: bool = False,
        # TODO: parameter name assign_gaslikes shadows imported function
        assign_gaslikes: bool = True,
        scale_factor: float = 1.0,
        calc_velocities: bool = False,
        load_mode: LoadMode = LoadMode.csv
    ):
        assert data_path is not None or data is not None, "No input data specified!"
        assert (
            timestep_range is None or len(timestep_range) == 3
        ), "Timesteps argument requires start time, end time and stride!"
        self.sim_dir = None
        self.data_path = data_path
        self.config = None
        self.timestep_range = timestep_range
        # TODO timestep renumbering currently not implemented
        self.keep_original_timestep_numbering = keep_original_timestep_numbering
        self.mat_format = mat_format
        self.scale_factor = scale_factor
        self.load_mode = load_mode
        if feature_metadata is None:
            self.feature_metadata = deepcopy(DEFAULT_FEATURE_METADATA)
        else:
            if isinstance(feature_metadata, str):
                with open(feature_metadata, "r") as f:
                    feature_metadata = json.load(f)
            self.feature_metadata = feature_metadata

        if features is None:
            features = {}
        self.features = features

        self.assign_gaslikes_bool = assign_gaslikes

        if data is None:
            self.data = self._load_data()
        else:
            self.data = data

        if calc_velocities:
            self.compute_velocities_from_positions()

        if self.timestep_range is None:
            self.timestep_range = (0, len(self.data), 1)

        if shiftToOrigin:
            self.shiftToOrigin(periodic=shiftPeriodic)

    def _load_data(self) -> Dict[int, pd.DataFrame]:
        if os.path.isdir(self.data_path) and self.mat_format == "":
            self.sim_dir = SimDir(self.data_path,self.load_mode)
            data = self.load_from_sim_dir()
        elif self.data_path.split(".")[-1] == "h5":
            data = self.load_from_hdf5(path=self.data_path)
        elif self.data_path.split(".")[-1] == "tracklets":
            data = self.load_data_from_tracklets()
        elif os.path.isfile(self.data_path) or len(glob(self.data_path)) > 0:
            data = self.load_from_mat()
        else:
            print(
                "Unknown format. Data loading failed! Please check your config and the datapath."
            )
            data = None
        return data

    def reload_data(self) -> Dict[int, pd.DataFrame]:
        """Reloads data found in self.data_path"""
        if self.data is not None:
            print("Warning: Overwriting previously loaded data!")
        if self.sim_dir is not None:
            data = self.load_from_sim_dir()
        elif os.path.isdir(self.data_path) and self.mat_format == "":
            self.sim_dir = SimDir(self.data_path, self.load_mode)
            data = self.load_from_sim_dir()
        elif self.data_path.split(".")[-1] == "h5":
            data = self.load_from_hdf5(path=self.data_path)
        elif self.data_path.split(".")[-1] == "tracklets":
            data = self.load_data_from_tracklets()
        elif os.path.isfile(self.data_path) or len(glob(self.data_path)) > 0:
            data = self.load_from_mat()
        else:
            print("Unable to load data. Check your config and the datapath")
            return None

        return data

    def load_from_sim_dir(self) -> Dict[int, pd.DataFrame]:
        """
        Loads data from Nastja Simulation Directory
        """
        with open(os.path.join(self.data_path, "config_save.json"), "r") as f:
            self.config = json.load(f)

        if self.timestep_range is None:
            self.timestep_range = (0, self.sim_dir.frames, 1)

        """positions = {}
        for idx, timestep in enumerate(range(*self.timestep_range)):
            pos = self.sim_dir.readCSV(timestep).set_index("CellID")
            positions[timestep] = pos[pos.index > self.config["CellsInSilico"]["liquid"]]

        data = {}
        for timestep, pos in positions.items():
            pos = pos.copy()
            if timestep-self.timestep_range[-1] in positions.keys():
                vel = (pos.subtract(positions[timestep-self.timestep_range[-1]], axis="index")).rename(columns={"CenterX": "VelX", "CenterY": "VelY", "CenterZ": "VelZ"})
                data[timestep] = pd.concat([pos, vel], axis=1)
            else:
                data[timestep] = pos"""

        data = {}
        positions = {}

        for tp in range(*self.timestep_range):
            if self.load_mode == LoadMode.sql and self.sim_dir.hasSQL:
                data_at_tp = self.sim_dir.readSQL(tp).set_index("CellID")
            elif self.load_mode == LoadMode.hdf5 and self.sim_dir.hasHDF5:
                data_at_tp = self.sim_dir.readHDF5(tp).set_index("CellID")
            else:
                data_at_tp = self.sim_dir.readCSV(tp).set_index("CellID")

            data_at_tp = data_at_tp[
                data_at_tp.index > self.config["CellsInSilico"]["liquid"]
            ]
            if not data_at_tp.index.is_unique:
                print("Warning! Duplicate CellIDs detected. Dropping duplicate IDs.")
                data_at_tp = data_at_tp[~data_at_tp.index.duplicated(keep="first")]

            data_at_tp["CenterX"] *= self.scale_factor
            data_at_tp["CenterY"] *= self.scale_factor
            data_at_tp["CenterZ"] *= self.scale_factor
            # = self.scale_factor
            pos = data_at_tp[["CenterX", "CenterY", "CenterZ"]].copy()
            # assert np.sum(np.isnan(pos['CenterX'])) < 1 and  np.sum(np.isnan(pos['CenterY'])) < 1 and np.sum(np.isnan(pos['CenterZ'])) < 1, 'NaNs detected in pos in load_from_sim_dir() for t = %d upon data loading. Stopping.' % tp
            positions[tp] = pos.copy()

            indices = np.where(np.isnan(data_at_tp["CenterX"]) == True)[0]
            if indices.size > 0:
                print("Warning: found NaNs. Removing...")
                data_at_tp = data_at_tp.drop(data_at_tp.index[indices], axis=0)

            if self.assign_gaslikes_bool == True:
                data_at_tp = assign_gaslikes(data_at_tp)

            data[tp] = data_at_tp
        return data

    def load_from_mat(self) -> Dict[int, pd.DataFrame]:
        """
        Loads data from .mat files
        """
        # TODO is there a better way to check whether self.mat_format is part of MatFormats?
        test_dict = self.mat_format.value if isinstance(self.mat_format, type(MatFormats.macrospheroid)) else self.mat_format
        if isinstance(test_dict, str):
            with open(test_dict, "r") as f:
                test_dict = json.load(f)

        data = {}
        if test_dict["multifile"] == True:
            filenames = []
            if "file_pattern" in test_dict.keys():
                filenames = glob(f"{self.data_path}/{test_dict['file_pattern']}")
            elif os.path.isfile(glob(self.data_path)[0]):
                filenames = glob(self.data_path)
            else:
                filenames = glob(f"{self.data_path}/*")
            filenames = [file for file in filenames if os.path.isfile(file)]
            for tp, file in enumerate(filenames):
                print(f"Loading {file}")
                mat = sio.loadmat(file)
                single_cells_mat = (
                    mat[test_dict["mat_structure"]["single_cells"]]
                    if "single_cells" in test_dict["mat_structure"]
                    else None
                )
                data[tp] = mat_to_dataframe(
                    mat[test_dict["mat_structure"]["all_cells"]],
                    single_cells_mat=single_cells_mat,
                )
        else:
            print(f"Loading {self.data_path}")
            mat = sio.loadmat(self.data_path)
            if "all_frames" in test_dict["mat_structure"]:
                for tp, data_at_tp in enumerate(
                    mat[test_dict["mat_structure"]["all_frames"]]
                ):
                    data_at_tp = mat_to_dataframe(data_at_tp[0])
                    data[tp] = data_at_tp
            else:
                single_cells_mat = (
                    mat[test_dict["mat_structure"]["single_cells"]]
                    if "single_cells" in test_dict["mat_structure"]
                    else None
                )
                data[0] = mat_to_dataframe(
                    mat[test_dict["mat_structure"]["all_cells"]],
                    single_cells_mat=single_cells_mat,
                )
        return data

    def load_data_from_csv(self) -> Dict[int, pd.DataFrame]:
        """
        Loads data from nastja csv files
        ***DEPRECATED***
        """

        csvs = glob(os.path.join(self.data_path, "*.csv"))
        csvs.sort(key=lambda csv_file: int(csv_file[:-4].split("-")[1]))
        if self.timestep_range is not None:
            start, end, stride = self.timestep_range
            csvs = csvs[start:end:stride]
        else:
            self.timestep_range = [0, len(csvs), 1]

        positions = {}

        for f in csvs:
            timestep = int(f[:-4].split("-")[1])
            with open(f, newline="") as csvfile:
                positions[timestep] = pd.read_csv(
                    csvfile,
                    sep=" ",
                    usecols=["#CellID", "CenterX", "CenterY", "CenterZ"],
                )
                positions[timestep] = positions[timestep].rename(
                    columns={"#CellID": "CellID"}
                )
                # positions[timestep].set_index("CellID")

        data = {}
        # FIXME: currently only saving frames for which velocity computable is (i.e. in general first frame gets lost)
        for timestep, pos in positions.items():
            pos = positions[timestep].copy().set_index("CellID")

            if timestep - self.timestep_range[-1] in positions.keys():
                vel = (data[timestep - 1].subtract(pos, axis="index")).rename(
                    columns={"CenterX": "VelX", "CenterY": "VelY", "CenterZ": "VelZ"}
                )

                data[timestep] = pd.concat([pos, vel], axis=1)
        return data

    def load_data_from_tracklets(self) -> Dict[int, pd.DataFrame]:
        """
        loads data from matlab files in tracklet format as used in the Kobitzki Zebrafish dataset
        """

        def todict(d):
            if isinstance(d, sio.matlab.mio5_params.mat_struct):
                return {name: todict(d.__dict__[name]) for name in d._fieldnames}
            elif isinstance(d, np.ndarray) and d.dtype == object:
                return np.vectorize(todict)(d)
            return d

        print(f"Loading {self.data_path}")
        data = sio.loadmat(
            self.data_path, struct_as_record=False, squeeze_me=True, mat_dtype=True
        )
        mat = {k: todict(v) for k, v in data.items()}
        tracklets = {
            "tracklet_positions": {
                tracklet["id"]: tracklet["pos"] for tracklet in mat["tracklets"]
            },
            "tracklet_startTimes": {
                tracklet["id"]: int(tracklet["startTime"]) - 1
                for tracklet in mat["tracklets"]
            },
            "tracklet_endTimes": {
                tracklet["id"]: int(tracklet["endTime"]) - 1
                for tracklet in mat["tracklets"]
            },
            "tracklets_per_timepoint": {
                tp: tp_data["tracklets"]
                for tp, tp_data in enumerate(mat["trackletsPerTimePoint"])
            },
        }
        if self.timestep_range is None:
            self.timestep_range = (0, len(data), 1)
        data = tracklets_to_dataframes(tracklets, timestep_range=self.timestep_range)

        return data

    def load_from_hdf5_v2(self, path: str) -> Dict[int, pd.DataFrame]:
        self.features = HDFGroupWithSubgroups(path, "features")
        self.feature_metadata = json.loads(self.features.attr["feature_metadata"])
        data = HDFGroup(filepath, "data")
        self.config = json.loads(data.attr["config"])
        return data

    def load_from_hdf5(self, path: str) -> Dict[int, pd.DataFrame]:
        with h5py.File(path) as f:
            self.feature_metadata = json.loads(f["features"].attrs["feature_metadata"])
            try:
                self.config = json.loads(f["data"].attrs["config"])
            except KeyError:
                self.config = None
        self.features = HDFGroupWithSubgroups(path, "features")
        # data = HDFGroup(path, "data")
        data = HDFToDataframeHandler(path, "data")
        return data

    def save_as_hdf5(self, filepath: str, show_progress: bool = False) -> None:
        """
        Saves the datahandler as hdf5 file.
        """
        if filepath.split(".")[-1] != "h5":
            filepath += ".h5"
        warnings.filterwarnings("ignore", category=NaturalNameWarning)
        with pd.HDFStore(filepath) as store:
            for timestep in conditional_tqdm(self.data.keys(), show_progress):
                store.append(f"data/{timestep}", self.data[timestep])
        with h5py.File(filepath, "a") as f:
            for feature_name, feature in self.features.items():
                # Currently only considers time step based features, might be needed to be changed later
                for timestep in conditional_tqdm(feature.keys(), show_progress):
                    try:
                        f.create_dataset(
                            f"features/{feature_name}/{timestep}",
                            data=feature[timestep],
                        )
                    except ValueError:
                        print(feature[timestep].shape)
                        f.create_dataset(
                            f"features/{feature_name}/{timestep}",
                            data=feature[timestep],
                            chunks=True,
                        )
            if self.feature_metadata is not None:
                # f.create_dataset("feature_metadata", data=json.dumps(self.feature_metadata))
                f["features"].attrs["feature_metadata"] = json.dumps(
                    self.feature_metadata
                )
            if self.config is not None:
                f.create_dataset("config", data=json.dumps(self.config))
                f["data"].attrs["config"] = json.dumps(self.config)

    def compute_velocities_from_positions(self):
        positions = {}
        for tp, data_at_tp in self.data.items():
            pos = data_at_tp[["CenterX", "CenterY", "CenterZ"]]
            positions[tp] = pos.copy()
            if tp - 1 in positions.keys():
                vel = (pos.subtract(positions[tp - 1], axis="index")).rename(
                    columns={"CenterX": "VelX", "CenterY": "VelY", "CenterZ": "VelZ"}
                )

                if self.sim_dir is not None:  # Check for periodic boundary jumps
                    system_size = (
                        self.scale_factor
                        * np.array(self.sim_dir.readConfig()["Geometry"]["blockcount"])
                        * np.array(self.sim_dir.readConfig()["Geometry"]["blocksize"])
                    )
                    x_jump_indices = np.array(
                        vel.index[
                            np.where(np.abs(vel["VelX"]) >= 0.5 * system_size[0])[0]
                        ]
                    )
                    y_jump_indices = np.array(
                        vel.index[
                            np.where(np.abs(vel["VelY"]) >= 0.5 * system_size[1])[0]
                        ]
                    )
                    z_jump_indices = np.array(
                        vel.index[
                            np.where(np.abs(vel["VelZ"]) >= 0.5 * system_size[2])[0]
                        ]
                    )
                    # Fix velocity values for boundary jumps (each axis needs to be treated separately)
                    for index in x_jump_indices:
                        if vel["VelX"][index] < 0:
                            vel["VelX"][index] += system_size[0]
                        else:
                            vel["VelX"][index] -= system_size[0]
                    for index in y_jump_indices:
                        if vel["VelY"][index] < 0:
                            vel["VelY"][index] += system_size[1]
                        else:
                            vel["VelY"][index] -= system_size[1]
                    for index in z_jump_indices:
                        if vel["VelZ"][index] < 0:
                            vel["VelZ"][index] += system_size[2]
                        else:
                            vel["VelZ"][index] -= system_size[2]

                data_at_tp = pd.concat([data_at_tp, vel], axis=1)
                # Temporary: investigate why NaN occurs
                data_at_tp = data_at_tp.dropna()
            self.data[tp] = data_at_tp

    def shiftToOrigin(self, periodic: bool = False) -> None:
        if periodic:
            # Use Circular mean
            if self.sim_dir is None:
                print(
                    "Periodic centering only works for simulations, exiting and leaving the data untouched.",
                    end="\r",
                )
                return None
            system_size = (
                self.scale_factor
                * np.array(self.sim_dir.readConfig()["Geometry"]["blockcount"])
                * np.array(self.sim_dir.readConfig()["Geometry"]["blocksize"])
            )
            for t in self.data.keys():
                df = self.data[t]
                if df.isnull().values.any():
                    print("WARNING: Data at time %d contains NAN values!" % t)

                # Calculate Periodic center of mass
                com = [
                    circmean(
                        df["CenterX"], high=system_size[0], low=0, nan_policy="omit"
                    ),
                    circmean(
                        df["CenterY"], high=system_size[1], low=0, nan_policy="omit"
                    ),
                    circmean(
                        df["CenterZ"], high=system_size[2], low=0, nan_policy="omit"
                    ),
                ]

                tmpData = (
                    self.data[t][["CenterX", "CenterY", "CenterZ"]] - com
                )  # Center around com

                # Apply periodic boundary conditions
                tmpData += system_size / 2
                tmpData = tmpData % system_size
                tmpData -= system_size / 2
                self.data[t][
                    ["CenterX", "CenterY", "CenterZ"]
                ] = tmpData  # Write back into Dataframe

        else:
            # Use Standard Numpy mean
            for t in self.data.keys():
                try:
                    df = self.data[t]
                    com = [
                        np.mean(df["CenterX"]),
                        np.mean(df["CenterY"]),
                        np.mean(df["CenterZ"]),
                    ]
                    self.data[t][["CenterX", "CenterY", "CenterZ"]] -= com
                except KeyError:
                    pass
        return None

    def get_ids_at_time(self, tp: int) -> np.array:
        """
        Returns CellIDs for all cells at given timepoint in same order
        as given by get_positions or get_velocities
        """
        dataframe = self.data[tp]
        return np.array(dataframe.index, dtype=int)

    def update_positions_at_time(self, tp: int, new_positions: np.array) -> None:
        self.data[tp][["CenterX", "CenterY", "CenterZ"]] = new_positions

    def update_velocities_at_time(self, tp: int, new_velocities: np.array) -> None:
        self.data[tp][["VelX", "VelY", "VelZ"]] = new_velocities

    def get_positions_at_time(
        self,
        tp: int,
        keep_ids: bool = False,
        extraction_mode: ExtractionMode | str = ExtractionMode.all,
        cell_type=1,
        zmin=None,
        zmax=None,
    ) -> np.array:
        """
        Returns positions at given time point

        extraction_mode determines, whether all, only non-gaslikes(liquid+solid), only gaslikes, only solids or only liquids are returned

        """
        assert (
            "aggregation_state" in self.data[tp].keys()
            or extraction_mode is ExtractionMode.all
        ), "Please assign gaslikes before you call this function"
        if isinstance(extraction_mode, str):
            extraction_mode = ExtractionMode[extraction_mode]
        if extraction_mode == ExtractionMode.all or extraction_mode is None:
            return np.array(self.data[tp][["CenterX", "CenterY", "CenterZ"]])
        if extraction_mode == ExtractionMode.cell_type:
            return np.array(
                self.data[tp].loc[self.data[tp]["Type"] == cell_type][
                    ["CenterX", "CenterY", "CenterZ"]
                ]
            )
        positions = self.data[tp][
            ["CenterX", "CenterY", "CenterZ", "aggregation_state"]
        ]
        if positions["aggregation_state"].isin(extraction_mode.value).any():
            return np.array(
                positions[positions["aggregation_state"].isin(extraction_mode.value)][
                    ["CenterX", "CenterY", "CenterZ"]
                ]
            )
        return None

    def get_positions_at_time_old(
        self,
        tp: int,
        keep_ids: bool = False,
        extraction_mode="all",
        cell_type=1,
        zmin=None,
        zmax=None,
    ) -> np.array:
        """
        Returns positions at given time point

        extraction_mode determines, whether all, only non-gaslikes(liquid+solid), only gaslikes, only solids or only liquids are returned

        """

        extraction_modes = {
            "all": 1,
            "non_gaslikes": 2,
            "gaslikes": 3,
            "solidlikes": 4,
            "fluidlikes": 5,
            "cell_type": 6,
        }

        if type(extraction_mode) == str:
            assert (
                extraction_mode in extraction_modes.keys()
            ), "Invalid extraction_mode."

            extraction_mode = extraction_modes[extraction_mode]

        else:
            assert extraction_mode > 0 and extraction_mode <= len(
                extraction_modes
            ), "Invalid extraction_mode"

        assert (
            "aggregation_state" in self.data[tp].keys()
        ), "Please assign gaslikes before you call this function"  # This happens when assign_gaslikes_via_fluidity is not applicable

        positions = None

        dataframe = self.data[tp]
        if all(key in dataframe.keys() for key in ["CenterX", "CenterY", "CenterZ"]):
            if extraction_mode == 1:
                positions = np.array(dataframe[["CenterX", "CenterY", "CenterZ"]])
            elif extraction_mode == 2:
                try:
                    positions = np.array(
                        self.data[tp].loc[self.data[tp]["aggregation_state"] < 2][
                            ["CenterX", "CenterY", "CenterZ"]
                        ]
                    )
                except KeyError:
                    print("No non_gaslikes at t = %d. Using all cells." % tp)
                    positions = np.array(dataframe[["CenterX", "CenterY", "CenterZ"]])
            elif extraction_mode == 3:
                try:
                    positions = np.array(
                        self.data[tp].loc[self.data[tp]["aggregation_state"] == 2][
                            ["CenterX", "CenterY", "CenterZ"]
                        ]
                    )
                except KeyError:
                    print("No gaslikes at t = %d. Using all cells." % tp)
                    positions = np.array(dataframe[["CenterX", "CenterY", "CenterZ"]])
            elif extraction_mode == 4:
                try:
                    positions = np.array(
                        self.data[tp].loc[self.data[tp]["aggregation_state"] == 0][
                            ["CenterX", "CenterY", "CenterZ"]
                        ]
                    )
                except KeyError:
                    print("No solid-likes at t = %d. Using all cells." % tp)
                    positions = np.array(dataframe[["CenterX", "CenterY", "CenterZ"]])
            elif extraction_mode == 5:
                try:
                    positions = np.array(
                        self.data[tp].loc[self.data[tp]["aggregation_state"] == 1][
                            ["CenterX", "CenterY", "CenterZ"]
                        ]
                    )
                except KeyError:
                    print("No liquid-likes at t = %d. Using all cells." % tp)
                    positions = np.array(dataframe[["CenterX", "CenterY", "CenterZ"]])
            elif extraction_mode == 6:
                try:
                    positions = np.array(
                        self.data[tp].loc[self.data[tp]["Type"] == cell_type][
                            ["CenterX", "CenterY", "CenterZ"]
                        ]
                    )
                except KeyError:
                    try:
                        positions = np.array(
                            self.data[tp].loc[self.data[tp]["Type"] == cell_type][
                                ["CenterX", "CenterY", "CenterZ"]
                            ]
                        )
                    except KeyError:
                        print(
                            "No cells of type %d at t = %d. Returning None."
                            % (cell_type, tp)
                        )
                        positions = None

            if keep_ids:
                if extraction_mode == 1:  # Return all
                    ids = np.array(dataframe.index)
                elif extraction_mode == 2:  # Return only non-gaslikes
                    ids = np.array(
                        self.data[tp].loc[self.data[tp]["aggregation_state"] < 2].index
                    )
                elif extraction_mode == 3:  # Return only gas-likes
                    ids = np.array(
                        self.data[tp].loc[self.data[tp]["aggregation_state"] == 2].index
                    )
                elif extraction_mode == 4:  # Return only solid-likes
                    ids = np.array(
                        self.data[tp].loc[self.data[tp]["aggregation_state"] == 0].index
                    )
                elif extraction_mode == 5:  # Return only liquid-likes
                    ids = np.array(
                        self.data[tp].loc[self.data[tp]["aggregation_state"] == 1].index
                    )
                elif extraction_mode == 6:  # Return only liquid-likes
                    try:
                        ids = np.array(
                            self.data[tp].loc[self.data[tp]["Type"] == cell_type].index
                        )
                    except KeyError:
                        ids = np.array(
                            self.data[tp].loc[self.data[tp]["Type"] == cell_type].index
                        )
                positions = np.concatenate((ids[:, None], positions), axis=1)

            if (
                isinstance(positions, np.ndarray)
                and isinstance(zmin, int)
                and isinstance(zmax, int)
            ):
                indices = np.where(
                    (positions[:, 2] >= zmin) & (positions[:, 2] < zmax)
                )[0]
                positions = positions[indices, :]

        else:
            print("Requested timepoint does not contain positional data")
            positions = None

        return positions

    # def get_boundary_jump_counts(self):
    #     try:
    #         feature_name = 'boundary_jump_counts'
    #         N = len(list(self.features[feature_name].keys()))
    #         jump_counts = np.full((N,3),0)
    #         for i in range(3):
    #             jump_counts[:,i] = np.array([val[i] for val in self.features[feature_name].values()])
    #         return (np.array(list(self.features[feature_name].keys())),jump_counts) #Return timepoints and velocity jump counts
    #     except KeyError:
    #         print('Boundary jump feature not found. It is only creted for simulation data.')

    def get_centroid_trajectory(self):
        feature_name = "centroid_position"
        self.features[feature_name] = {}
        for tp in self.data.keys():
            pos = self.data[tp]
            self.features[feature_name][tp] = np.array(
                [
                    np.mean(pos["CenterX"]),
                    np.mean(pos["CenterY"]),
                    np.mean(pos["CenterZ"]),
                ]
            )

        N = len(list(self.features[feature_name].keys()))
        centroid_positions = np.full((N, 3), 0.0)
        for i in range(3):
            centroid_positions[:, i] = np.array(
                [self.features[feature_name][tp][i] for tp in self.data.keys()]
            )
        return (
            np.array(list(self.features[feature_name].keys())),
            centroid_positions,
        )  # Return timepoints and centroid positions

    def assign_gaslikes_via_fluidity(self, **kwargs):
        self.data = assign_gaslikes_via_fluidity(self.data, self.scale_factor, **kwargs)

    def assign_gaslikes_at_time(self, tp: int, **kwargs) -> None:
        """
        For re-assigning gaslikes, e.g. with different threshold distance
        """
        if self.assign_gaslikes_bool == True:
            try:
                df = self.data[tp]
                if "aggregation_state" in df:
                    del df["aggregation_state"]
                if "single_cell" in df:
                    del df["single_cell"]

                self.data[tp] = assign_gaslikes(df, **kwargs)
            except KeyError:
                print("Timestep not loaded")
        else:
            print(
                "assign_gaslikes_bool is set to false. Set to true if you want to use this function"
            )
            return

    def get_velocities_at_time(self, tp: int) -> np.array:
        """
        Returns velocities at given time point
        """
        dataframe = self.data[tp]

        if all(key in dataframe.keys() for key in ["VelX", "VelY", "VelZ"]):
            velocities = np.array(dataframe[["VelX", "VelY", "VelZ"]])
            # velocities = dataframe[["VelX", "VelY", "VelZ"]]
        else:
            print(f"Timepoint {tp} does not contain velocity data")
            velocities = None
        return velocities

    def get_volumes_at_time(
        self, tp: int, max_volume: float = 40000, store: bool = True
    ) -> np.array:
        """
        Retrieves volumes at given time point if available, computes it otherwise
        """
        try:
            volumes = self.features["volumes"][tp]
        except KeyError:
            volumes = get_volumes(self.get_positions_at_time(tp), max_volume=max_volume)
            if volumes.size > 0:
                if volumes[0] == -1:
                    print("Possible NaNs at t = %d." % tp)
                if store:
                    self.store_feature("volumes", volumes, tp)
            else:
                print("Volumes.size = 0")
        return volumes

    def store_feature(self, feature_name: str, feature, tp: int) -> None:
        try:
            self.features[feature_name][tp] = feature
        except KeyError:
            self.features[feature_name] = {}
            self.features[feature_name][tp] = feature

    def get_average_local_density_at_time(
        self, tp: int, store: bool = True, **kwargs
    ) -> np.array:
        try:
            local_density = self.features["average_local_density"][tp]
        except KeyError:
            positions = self.get_positions_at_time(tp)
            local_density = get_average_local_density_function(positions, **kwargs)
            if store:
                self.store_feature("average_local_density", local_density, tp)
        return local_density

    def get_individual_local_density_at_time(
        self, tp: int, store: bool = True, **kwargs
    ) -> np.array:
        try:
            local_density = self.features["individual_local_density"][tp]
        except KeyError:
            positions = self.get_positions_at_time(tp)
            local_density = get_individual_local_density_function(positions, **kwargs)
            if store:
                self.store_feature("individual_local_density", local_density, tp)
        return local_density

    def get_radially_averaged_local_density_at_time(
        self, tp: int, store: bool = True, **kwargs
    ) -> np.array:
        try:
            local_density = self.features["radially_averaged_local_density"][tp]
        except KeyError:
            positions = self.get_positions_at_time(tp)
            local_density = get_radially_averaged_local_density_function(
                positions, **kwargs
            )
            if store:
                self.store_feature("radially_averaged_local_density", local_density, tp)
        return local_density

    def get_radial_local_density_function_at_time(
        self, tp: int, store: bool = True, **kwargs
    ) -> Tuple[np.array, np.array, np.array]:
        bins = np.full(1, -1)
        local_density = np.full(1, -1)
        local_density_std_dev = np.full(1, -1)
        try:
            local_density = self.features["radial_local_density"][tp]
        except KeyError:
            positions = self.get_positions_at_time(tp)
            (
                bins,
                local_density,
                local_density_std_dev,
            ) = get_radial_local_density_function(positions, **kwargs)
            if store:
                self.store_feature("radial_local_density", local_density, tp)
        return bins, local_density, local_density_std_dev

    def get_path_density_at_time(
        self, tp: int, store: bool = True, overwrite: bool = False, **kwargs
    ) -> Tuple[dict, dict]:
        feature = {}
        feature["paths"] = {}
        feature["densities"] = {}
        calc_feature = False
        try:
            feature = self.features["path_density"][tp]
            if overwrite == True:
                calc_feature = True
        except KeyError:
            calc_feature = True

        if calc_feature == True:
            positions = np.array([0, 0, 0])
            try:
                positions = extract_non_gaslikes(self.data[tp])
            except KeyError:
                print("Timestep not loaded.")
                return feature["paths"], feature["densities"]

            feature["paths"], feature["densities"] = get_path_density(
                positions, **kwargs
            )
            if store:
                self.store_feature("path_density", feature, tp)

        return feature["paths"], feature["densities"]

    def get_average_velocity_correlation_at_time(
        self, tp: int, store: bool = True, **kwargs
    ) -> np.array:
        try:
            velocity_correlation = self.features["average_velocity_correlation"][tp]
        except KeyError:
            positions = self.get_positions_at_time(tp)
            velocities = self.get_velocities_at_time(tp)
            if velocities is not None:
                velocity_correlation = get_average_velocity_correlation_function(
                    positions, velocities, **kwargs
                )
                if store:
                    self.store_feature(
                        "average_velocity_correlation", velocity_correlation, tp
                    )
            else:
                velocity_correlation = None
        return velocity_correlation

    def get_radially_averaged_velocity_correlation_at_time(
        self, tp: int, store: bool = True, **kwargs
    ) -> np.array:
        try:
            velocity_correlation = self.features[
                "radially_averaged_velocity_correlation"
            ][tp]
        except KeyError:
            positions = self.get_positions_at_time(tp)
            velocities = self.get_velocities_at_time(tp)
            if velocities is not None:
                velocity_correlation = (
                    get_radially_averaged_velocity_correlation_function(
                        positions, velocities, **kwargs
                    )
                )
                if store:
                    self.store_feature(
                        "radially_averaged_velocity_correlation",
                        velocity_correlation,
                        tp,
                    )
            else:
                velocity_correlation = None
        return velocity_correlation

    def get_individual_velocity_correlation_at_time(
        self, tp: int, store: bool = True, **kwargs
    ) -> np.array:
        try:
            velocity_correlation = self.features["individual_velocity_correlation"][tp]
        except KeyError:
            positions = self.get_positions_at_time(tp)
            velocities = self.get_velocities_at_time(tp)
            if velocities is not None:
                velocity_correlation = get_individual_velocity_correlation_function(
                    positions, velocities, **kwargs
                )
                if store:
                    self.store_feature(
                        "individual_velocity_correlation", velocity_correlation, tp
                    )
            else:
                velocity_correlation = None
        return velocity_correlation

    def get_central_local_density_at_time(
        self,
        tp: int,
        store: bool = True,
        extraction_mode: ExtractionMode = ExtractionMode.all,
        **kwargs,
    ) -> np.array:
        feature_name = "central_local_density"
        if extraction_mode == ExtractionMode.non_gaslikes:
            feature_name += "_non_gaslikes"
        elif extraction_mode == ExtractionMode.gaslikes:
            feature_name += "_gaslikes"
        try:
            central_local_density = self.features[feature_name][tp]
        except KeyError:
            positions = self.get_positions_at_time(tp, extraction_mode=extraction_mode)
            central_local_density = get_central_local_density_function(
                positions, **kwargs
            )
            if store:
                self.store_feature(feature_name, central_local_density, tp)
        return central_local_density

    def get_central_velocity_correlation_at_time(
        self, tp: int, store: bool = True, **kwargs
    ) -> np.array:
        try:
            velocity_correlation = self.features["central_velocity_correlation"][tp]
        except KeyError:
            positions = self.get_positions_at_time(tp)
            velocities = self.get_velocities_at_time(tp)
            if velocities is not None:
                velocity_correlation = get_central_velocity_correlation_function(
                    positions, velocities, **kwargs
                )
                if store:
                    self.store_feature(
                        "central_velocity_correlation", velocity_correlation, tp
                    )
            else:
                velocity_correlation = None
        return velocity_correlation

    def get_local_density_and_velocity_correlation_at_time(
        self, tp: int, store: bool = True, **kwargs
    ):
        # TODO
        positions = self.get_positions_at_time(tp)
        velocities = self.get_velocities_at_time(tp)

    def get_non_gaslike_fraction_at_time(
        self,
        tp: int,
        store: int = True,
        overwrite: int = False,
        threshold_dist=16,
        **kwargs,
    ):
        if overwrite == True:
            print("Overwriting previous value if present")
        non_gaslike_fraction = None
        recalc = False
        try:
            non_gaslike_fraction = self.features["non_gaslike_fraction"][tp]
        except KeyError:
            recalc = True
        if recalc == True or overwrite == True:
            # self.assign_gaslikes_at_time(tp,threshold_dist=threshold_dist)
            if "aggregation_state" in self.data[tp]:
                gaslike_column = np.array(self.data[tp]["aggregation_state"])
                non_gaslike_fraction = get_non_gaslike_fraction(
                    gaslike_column=gaslike_column
                )
                if store:
                    self.store_feature("non_gaslike_fraction", non_gaslike_fraction, tp)
            else:
                print("No 'single_cells' column for t = %d" % tp)

        return non_gaslike_fraction

    def get_tumor_area_at_time(
        self, tp, store=True, overwrite=False, reassign=False, **kwargs
    ):
        area = None
        if tp not in list(self.data.keys()):
            print("Timepoint not in data. Returning None")
            return None
        recalc = False
        try:
            area = self.features["tumor_area"][tp]
        except KeyError:
            recalc = True
        if recalc == True or overwrite == True:
            if "aggregation_state" not in self.data[tp].keys() or reassign == True:
                self.assign_gaslikes_at_time(tp)
            positions = self.get_positions_at_time(
                tp, extraction_mode=ExtractionMode.non_gaslikes
            )  # We only want the non-gaslikes for this

            if positions is None:
                return None
            # NOTE: this returns a tuple if return_area_only is set to false

            verts, faces = get_tumor_surface_cloud_MC(
                positions, retTypes=["verts", "faces"], **kwargs
            )
            area = get_tumor_area(positions, verts, faces)

            #            area = get_axis_averaged_tumor_area(positions, **kwargs)
            if store:
                self.store_feature("tumor_area", area, tp)

        return area

    def get_tumor_surface_orientation_at_time(
        self, tp, store=True, overwrite=False, reassign=False, Nbins=100, **kwargs
    ):
        orientations = None
        vals = None
        if tp not in list(self.data.keys()):
            print("Timepoint not in data. Returning None")
            return None
        recalc = False
        try:
            vals = self.features["tumor_surface_orientation"][tp]
        except KeyError:
            recalc = True
        if overwrite == True or recalc == True:
            positions = self.get_positions_at_time(
                tp, extraction_mode=ExtractionMode.non_gaslikes
            )  # We only want the non-gaslikes for this
            if positions is None:
                return None
            surface_cloud, normals = get_tumor_surface_cloud_MC(
                positions, retTypes=["points", "normals"], **kwargs
            )
            orientations = get_tumor_surface_cloud_orientation(surface_cloud, normals)
            vals, bins = np.histogram(orientations, bins=np.linspace(-1, 1, Nbins))
            vals = vals.astype(float)[:] / orientations.shape[0]
            if store:
                self.store_feature("tumor_surface_orientation", vals, tp)
        return vals

    def get_gaslike_distance_at_time(
        self, tp, store=True, overwrite=False, reassign=False, **kwargs
    ):
        dists = None
        if tp not in list(self.data.keys()):
            print("Timepoint not in data. Returning None")
            return None
        recalc = False
        try:
            dists = self.features["gaslike_dists"][tp]
        except KeyError:
            recalc = True
        if overwrite == True or recalc == True:
            gaslike_positions = self.get_positions_at_time(
                tp, extraction_mode=ExtractionMode.gaslikes
            )
            non_gaslike_positions = self.get_positions_at_time(
                tp, extraction_mode=ExtractionMode.non_gaslikes
            )
            if gaslike_positions is None or non_gaslike_positions is None:
                return None
            dists = get_gaslike_distance(
                gl=gaslike_positions, ngl=non_gaslike_positions
            )
            if store:
                self.store_feature("gaslike_dists", dists, tp)
        return dists

    def get_radial_displacement(self, store=True, **kwargs):
        radial_displacement = None
        try:
            radial_displacement = self.features["radial_displacement"][0]
        except KeyError:
            radial_displacement = get_radial_displacement(self.data)
            if store:
                self.store_feature("radial_displacement", radial_displacement, 0)
        return radial_displacement

    def show_feature_at_time(self, feature_name, tp, ax=None, **kwargs):
        if feature_name == "non_gaslike_fraction" or feature_name == "tumor_area":
            barplot(feature_name, self.features[feature_name][tp], ax=ax, **kwargs)
        else:
            lineplot(self.features[feature_name][tp], ax=ax, **kwargs)

    def show_feature(self, feature_name="", ax=None, **kwargs):
        # TODO This function is currently only implemented for the 'non_gaslike_fraction' feature. The rest needs to be included"
        if feature_name == "non_gaslike_fraction":
            show_non_gas_fraction(
                self.features["non_gaslike_fraction"], ax=ax, **kwargs
            )
        elif feature_name == "radial_displacement":
            show_average_radial_displacement(
                self.features["radial_displacement"], ax=ax, **kwargs
            )
        else:
            print(f"Function not implemented for feature {feature_name}")

    @write_to_feature_dict
    @write_metadata
    def velocities(self, show_progress=False):
        velocities = {}
        for timestep in conditional_tqdm(self.data.keys(), show_progress):
            velocities[timestep] = self.get_velocities_at_time(timestep)
        return velocities

    @write_metadata
    def volumes(self, *args, show_progress=False, **kwargs):
        volumes = {}
        for timestep in conditional_tqdm(self.data.keys(), show_progress):
            volumes[timestep] = self.get_volumes_at_time(timestep, *args, **kwargs)
        return volumes

    @write_metadata
    def average_local_density(self, *args, show_progress=False, **kwargs):
        local_densities = {}
        for timestep in conditional_tqdm(self.data.keys(), show_progress):
            local_densities[timestep] = self.get_average_local_density_at_time(
                timestep, *args, **kwargs
            )
        return local_densities

    @write_metadata
    def radially_averaged_local_density(self, *args, show_progress=False, **kwargs):
        local_densities = {}
        for timestep in conditional_tqdm(self.data.keys(), show_progress):
            local_densities[
                timestep
            ] = self.get_radially_averaged_local_density_at_time(
                timestep, *args, **kwargs
            )
        return local_densities

    @write_metadata
    def individual_local_density(self, *args, show_progress=False, **kwargs):
        local_densities = {}
        for timestep in conditional_tqdm(self.data.keys(), show_progress):
            local_densities[timestep] = self.get_individual_local_density_at_time(
                timestep, *args, **kwargs
            )
        return local_densities

    @write_metadata
    def central_local_density(self, *args, show_progress=False, **kwargs):
        central_local_densities = {}
        for timestep in conditional_tqdm(self.data.keys(), show_progress):
            central_local_densities[timestep] = self.get_central_local_density_at_time(
                timestep, *args, **kwargs
            )
        return central_local_densities

    @write_metadata
    def average_velocity_correlation(self, *args, show_progress=False, **kwargs):
        velocity_correlations = {}
        for timestep in conditional_tqdm(self.data.keys(), show_progress):
            velocity_correlations[
                timestep
            ] = self.get_average_velocity_correlation_at_time(timestep, *args, **kwargs)
        return velocity_correlations

    @write_metadata
    def radially_averaged_velocity_correlation(
        self, *args, show_progress=False, **kwargs
    ):
        velocity_correlations = {}
        for timestep in conditional_tqdm(self.data.keys(), show_progress):
            velocity_correlations[
                timestep
            ] = self.get_radially_averaged_velocity_correlation_at_time(
                timestep, *args, **kwargs
            )
        return velocity_correlations

    @write_metadata
    def individual_velocity_correlation(self, *args, show_progress=False, **kwargs):
        velocity_correlations = {}
        for timestep in conditional_tqdm(self.data.keys(), show_progress):
            velocity_correlations[
                timestep
            ] = self.get_individual_velocity_correlation_at_time(
                timestep, *args, **kwargs
            )
        return velocity_correlations

    @write_metadata
    def central_velocity_correlation(self, *args, show_progress=False, **kwargs):
        central_velocity_correlations = {}
        for timestep in conditional_tqdm(self.data.keys(), show_progress):
            central_velocity_correlations[
                timestep
            ] = self.get_central_velocity_correlation_at_time(timestep, *args, **kwargs)

        return central_velocity_correlations

    @write_metadata
    def non_gaslike_fraction(self, *args, show_progress=False, **kwargs):
        non_gaslike_fractions = {}
        for timestep in conditional_tqdm(self.data.keys(), show_progress):
            non_gaslike_fractions[timestep] = self.get_non_gaslike_fraction_at_time(
                timestep, *args, **kwargs
            )
        return non_gaslike_fractions

    @write_metadata
    def radial_displacement(self, *args, show_progress=False, **kwargs):
        radial_displacement = {}
        radial_displacement[0] = self.get_radial_displacement(*args, **kwargs)
        return radial_displacement

    @write_metadata
    def tumor_area(self, *args, show_progress=False, **kwargs):
        tumor_area = {}
        for timestep in conditional_tqdm(self.data.keys(), show_progress):
            tumor_area[timestep] = self.get_tumor_area_at_time(
                timestep, *args, **kwargs
            )
        return tumor_area

    @write_metadata
    def tumor_surface_orientation(self, *args, show_progress=False, **kwargs):
        orientations = {}
        for timestep in conditional_tqdm(self.data.keys(), show_progress):
            orientations[timestep] = self.get_tumor_surface_orientation_at_time(
                timestep, *args, **kwargs
            )
        return orientations

    @write_metadata
    def gaslike_dists(self, *args, show_progress=False, **kwargs):
        dists = {}
        for timestep in conditional_tqdm(self.data.keys(), show_progress):
            dists[timestep] = self.get_gaslike_distance_at_time(
                timestep, *args, **kwargs
            )
        return dists

    def extract_all_features(self, show_progress=False):
        for feature_name, feature_parameters in self.feature_metadata.items():
            print(
                f"{time.strftime('%H:%M:%S')} | Computing {feature_name}, with {feature_parameters} ..."
            )
            FEATURE_FUNCTIONS[feature_name](
                self=self, show_progress=show_progress, **feature_parameters
            )

    def show_all_features_at_time(self, tp: int, size_per_fig=5, **kwargs):
        N_features = (
            len(self.features.keys()) + 1
        )  # We add one, because we want a show_3d in first place
        N_cols = (
            N_features if N_features <= 3 else int(np.floor(np.sqrt(N_features)) + 1)
        )
        N_rows = 1 if N_features <= 3 else int(np.floor(np.sqrt(N_features)) + 1)

        fig = plt.figure(figsize=(size_per_fig * N_cols, size_per_fig * N_rows))

        count = 1  # Apparently, matplotlib starts indexing at 1...
        ax = fig.add_subplot(N_rows, N_cols, count, projection="3d")
        self.show_3d_at_time(tp, ax=ax, use_ipyvolume=False, s=50)
        ax.title.set_text("3D view")
        count += 1
        for feature_name in self.features.keys():
            print(f"Plotting feature {feature_name} at tp {tp}...")
            ax = fig.add_subplot(N_rows, N_cols, count)
            self.show_feature_at_time(feature_name, tp, ax=ax, **kwargs)
            ax.title.set_text(feature_name)
            count += 1
        plt.show()

    def get_bounding_box_at_time(self, tp: int) -> np.array:
        positions = self.get_positions_at_time(tp)
        lowest_x = np.min(positions[:, 0])
        lowest_y = np.min(positions[:, 1])
        lowest_z = np.min(positions[:, 2])
        highest_x = np.max(positions[:, 0])
        highest_y = np.max(positions[:, 1])
        highest_z = np.max(positions[:, 2])
        return np.array((lowest_x, lowest_y, lowest_z)), np.array(
            (highest_x, highest_y, highest_z)
        )

    def get_yolk_axes(
        self, tp, cylinder_radius="adaptive", start_radius=10, cell_size=15
    ):
        positions = self.get_positions_at_time(tp)

        if cylinder_radius != "adaptive":
            x_cylinder = np.array(
                list(
                    filter(lambda x: x[1] ** 2 + x[2] ** 2 < cylinder_radius, positions)
                )
            )
            y_cylinder = np.array(
                list(
                    filter(lambda x: x[0] ** 2 + x[2] ** 2 < cylinder_radius, positions)
                )
            )
            z_cylinder = np.array(
                list(
                    filter(lambda x: x[0] ** 2 + x[1] ** 2 < cylinder_radius, positions)
                )
            )
        else:
            cylinder_radius = start_radius
            x_cylinder = np.array(
                list(
                    filter(lambda x: x[1] ** 2 + x[2] ** 2 < cylinder_radius, positions)
                )
            )
            while len(x_cylinder) <= 0:
                cylinder_radius *= 1.5
                x_cylinder = np.array(
                    list(
                        filter(
                            lambda x: x[1] ** 2 + x[2] ** 2 < cylinder_radius, positions
                        )
                    )
                )

            cylinder_radius = start_radius
            y_cylinder = np.array(
                list(
                    filter(lambda x: x[0] ** 2 + x[2] ** 2 < cylinder_radius, positions)
                )
            )
            while len(y_cylinder) <= 0:
                cylinder_radius *= 1.5
                y_cylinder = np.array(
                    list(
                        filter(
                            lambda x: x[0] ** 2 + x[2] ** 2 < cylinder_radius, positions
                        )
                    )
                )

            cylinder_radius = start_radius
            z_cylinder = np.array(
                list(
                    filter(lambda x: x[0] ** 2 + x[1] ** 2 < cylinder_radius, positions)
                )
            )
            while len(z_cylinder) <= 0:
                cylinder_radius *= 1.5
                z_cylinder = np.array(
                    list(
                        filter(
                            lambda x: x[0] ** 2 + x[1] ** 2 < cylinder_radius, positions
                        )
                    )
                )

        x_semiaxis = min(map(lambda x: abs(x), x_cylinder[:, 0])) - cell_size
        y_semiaxis = min(map(lambda x: abs(x), y_cylinder[:, 1])) - cell_size
        z_semiaxis = min(map(lambda x: abs(x), z_cylinder[:, 2])) - cell_size

        return x_semiaxis, y_semiaxis, z_semiaxis

    def extract_starting_positions(
        self,
        tp,
        positions=None,
        size=100.0,
        size_vec=None,
        min_num_points=20,
        translate_to_origin=False,
        entire_bounding_box=False,
    ):
        # TODO method needs complete overhaul (risk of infinite loop, hard coded values, unclear naming...)
        if size_vec is None:
            size_vec = np.array((size, size, size))

        if positions is None:
            positions = self.get_positions_at_time(tp)

        bounding_box = self.bounding_box_at_time[tp]
        if entire_bounding_box:
            corner = bounding_box[0]
            size_vec = bounding_box[1] - bounding_box[0]
            box = extract_cuboid(
                positions, corner, size_vec[0], size_vec[1], size_vec[2]
            )
        else:
            num_points_in_box = 0
            while num_points_in_box < min_num_points:
                corner = (
                    np.random.rand(3) * (bounding_box[1] - size_vec - bounding_box[0])
                    + bounding_box[0]
                )
                while not (200 < np.linalg.norm(corner) < 600):
                    corner = (
                        np.random.rand(3)
                        * (bounding_box[1] - size_vec - bounding_box[0])
                        + bounding_box[0]
                    )
                box = extract_cuboid(
                    positions, corner, size_vec[0], size_vec[1], size_vec[2]
                )
                num_points_in_box = len(box)
        if translate_to_origin:
            box -= corner
        return box, [corner, size_vec]

    def extract_box_at_time(self, tp, corners):
        pass

    def save_feature_dict(self, file_path, include_metadata=False):
        with open(file_path, "wb") as f:
            features = self.features
            if include_metadata:
                self.features["metadata"] = self.feature_metadata
            pickle.dump(features, f)

    def save_datahandler(self, filepath, overwrite=False):
        if os.path.isfile(filepath):
            if overwrite:
                print("File exists, overwriting ...")
            else:
                print("File exists, specify overwrite=True to overwrite. Aborting.")
                return

        self.sim_dir = None
        with open(filepath, "wb") as f:
            pickle.dump(self, f)

    def save_feature_metadata(self, filepath):
        with open(filepath, "w") as f:
            json.dump(self.feature_metadata, f)

    def extract_datahandler_by_range(self, timestep_range):
        assert timestep_range[0] >= 0 and timestep_range[1] <= max(
            self.data.keys()
        ), "Timestep range out of bound!"
        data = {tp: self.data[tp] for tp in range(*timestep_range)}
        features = {
            feature_name: {tp: feature[tp] for tp in range(*timestep_range)}
            for feature_name, feature in self.features.items()
        }

        feauture_metadata = self.feature_metadata
        return DataHandler(
            data=data,
            features=features,
            feature_metadata=feauture_metadata,
            timestep_range=timestep_range,
            scale_factor=self.scale_factor,
        )

    def extract_datahandler_by_timesteps(self, list_of_timesteps):
        if list_of_timesteps is None:
            return None
        if len(list_of_timesteps) == 0:
            return None
        assert max(list_of_timesteps) <= max(
            self.data.keys()
        ), "Timestep range out of bound!"
        data = {tp: self.data[tp] for tp in list_of_timesteps}
        try:
            features = {
                feature_name: {tp: feature[tp] for tp in list_of_timesteps}
                for feature_name, feature in self.features.items()
            }
        except KeyError:
            print(
                "Not all timesteps present for all features. Splitting only those for which they exist"
            )
            features = {}

            for feature_name, feature in self.features.items():
                features[feature_name] = {}
                for tp in list_of_timesteps:
                    try:
                        features[feature_name][tp] = feature[tp]
                    except KeyError:
                        pass
        feauture_metadata = self.feature_metadata
        return DataHandler(
            data=data,
            features=features,
            feature_metadata=feauture_metadata,
            scale_factor=self.scale_factor,
        )

    def split_datahandler(self, number_of_chunks):
        # timestep_ranges = [(chunk[0], chunk[-1], self.timestep_range[-1]) for chunk in np.array_split(list(self.data.keys()), number_of_chunks)]
        # print(timestep_ranges)
        lists_of_timesteps = np.array_split(list(self.data.keys()), number_of_chunks)
        new_datahandlers = [
            self.extract_datahandler_by_timesteps(list_of_timesteps)
            for list_of_timesteps in lists_of_timesteps
        ]
        return new_datahandlers

    def visualize_timepoint(
        self,
        tp,
        ax=None,
        positions=None,
        angle=None,
        file=None,
        limits=None,
        color=None,
        size=0.5,
        yolk=False,
    ):
        if file is None:
            pass  # %matplotlib notebook
        if positions is None:
            positions = self.get_positions_at_time(tp)
        if ax is None:
            fig = plt.figure()
            ax = Axes3D(fig)

        if color == "dist":
            positions = np.array(
                list(filter(lambda pos: np.linalg.norm(pos) < 550, positions))
            )

        sequence_containing_x_vals = positions[:, 0]
        sequence_containing_y_vals = positions[:, 1]
        sequence_containing_z_vals = positions[:, 2]

        ax.view_init(azim=angle)

        if color is not None:
            if isinstance(color, np.ndarray):
                if len(color) != len(positions):
                    print("Specified color dimensions do not match number of cells!")
                    color = None
            elif color == "dist":
                color = [dist(pos, np.array([0, 0, 0])) for pos in positions]

                color = [np.log(c) for c in color]
            elif color == "vel":
                color = [np.linalg.norm(vel) for vel in self.get_velocities_at_time(tp)]
                # high,low = max(color), min(color)
                # color = [(c -low)/(high-low) for c in color]
                # print(color)
            else:
                print(
                    "Warning: color argument does not match options. Ignoring argument."
                )

        ax.scatter(
            sequence_containing_x_vals,
            sequence_containing_y_vals,
            sequence_containing_z_vals,
            s=size,
            c=color,
            cmap="plasma",
        )
        if limits is not None:
            ax.set_xlim(limits[0])
            ax.set_ylim(limits[1])
            ax.set_zlim(limits[2])

        if yolk:
            axes = self.get_yolk_axes(tp, ax=ax)
            # print(axes)

        if file is None:
            plt.show()
        else:
            plt.savefig(file, dpi=300)

        return fig

    def visualize_timepoint_basic(self, tp):
        positions = self.get_positions_at_time(tp)

        fig = plt.figure()
        ax = fig.add_subplot(111, projection="3d")

        # Extract X, Y, and Z coordinates from positions
        x = positions[:, 0]
        y = positions[:, 1]
        z = positions[:, 2]

        # Create a 3D scatter plot
        ax.scatter(x, y, z)

        # Set labels for each axis
        ax.set_xlabel("X")
        ax.set_ylabel("Y")
        ax.set_zlabel("Z")

        # Optionally, set limits for each axis if needed
        # ax.set_xlim([xmin, xmax])
        # ax.set_ylim([ymin, ymax])
        # ax.set_zlim([zmin, zmax])

        plt.show()

    def show_2d_at_time(self, tp, show_mode=1, ax=None, **kwargs):
        """
        shows a 2d plot of the point cloud

        show_mode:  1 --> show gaslikes and non-gaslikes
                    2 --> show non-gaslikes only
                    3 --> show gaslikes only
        """
        assert (
            show_mode >= 1 and show_mode <= 3
        ), "show_mode only accepts values between 1 and 3"
        try:
            gaslikes = self.get_positions_at_time(
                tp, extraction_mode=ExtractionMode.gaslikes
            )  # extract_gaslikes(self.data[tp])
            non_gaslikes = self.get_positions_at_time(
                tp, extraction_mode=ExtractionMode.non_gaslikes
            )  # extract_non_gaslikes(self.data[tp])

            if show_mode == 2:
                gaslikes = None
            if show_mode == 3:
                non_gaslikes = None
            ax = show_gas_and_non_gas_2d(
                gaslikes=gaslikes, non_gaslikes=non_gaslikes, ax=ax, **kwargs
            )
        except KeyError:
            print("Time point not loaded")
        return ax

    def show_3d_at_time(self, tp, show_mode:ShowMode = ShowMode.gas_and_non_gas, ax=None, **kwargs):
        """
        shows a 3d plot of the point cloud


        """
        assert (
            show_mode in ShowMode
        ), f"show_mode only accepts the following values: {[e.name for e in ShowMode]} "
        labels = []
        try:
            if show_mode == ShowMode.by_type:
                types = np.unique(self.data[tp]["Type"])
                labels = [typenbr for typenbr in types]
                pointclouds_to_show = [
                    self.get_positions_at_time(
                        tp, extraction_mode=ExtractionMode.cell_type, cell_type=type
                    )
                    for type in types
                ]
            else:
                gaslikes = self.get_positions_at_time(
                    tp, extraction_mode=ExtractionMode.gaslikes
                )  # extract_gaslikes(self.data[tp])
                non_gaslikes = self.get_positions_at_time(
                    tp, extraction_mode=ExtractionMode.non_gaslikes
                )  # extract_non_gaslikes(self.data[tp])
                pointclouds_to_show = []
                if show_mode == ShowMode.gas_and_non_gas :
                    pointclouds_to_show = [gaslikes, non_gaslikes]
                    labels = ["gaslikes","non-gaslikes"]
                elif show_mode == ShowMode.non_gaslikes:
                    pointclouds_to_show = [non_gaslikes]
                    labels = ["non-gaslikes"]
                elif show_mode == ShowMode.gaslikes:
                    pointclouds_to_show = [gaslikes]
                    labels = ["gaslikes"]

            show_pointclouds_3d(
                pointclouds_to_show, labels, ax=ax, **kwargs
            )
        except KeyError:
            print("Time point not loaded")
        return ax

    def show_all_3d(self, tp, ax=None, file=None, **kwargs):
        """
        Displays all cells in 3d at a given timepoint
        """
        try:
            positions = self.get_positions_at_time(tp)
            ax = show_3d(
                positions[:, 0], positions[:, 1], positions[:, 2], ax=ax, **kwargs
            )
            if file:
                plt.savefig(file, dpi=300)
        except KeyError:
            print("Time point not loaded")
        return ax

    def show_centroid_over_time(
        self, ret=False, **kwargs
    ):  # Extracts centroid coordinates from all timeframes and plots them
        t, pos = self.get_centroid_trajectory()
        x, y, z = (pos[:, 0], pos[:, 1], pos[:, 2])
        show_3d(x, y, z, label="Centroid trajectory", **kwargs)
        if ret == True:
            return (t, pos)  # Return centroid positions

    def show_boundary_jumps_over_time(
        self, show_separate=False, legend=True, figsize=(20, 10), ret=False
    ):  # Extracts boundary jump counts from all timeframes and plots them (currently only used for simulation data)
        try:
            t, counts = self.get_boundary_jump_counts()
            x, y, z = (counts[:, 0], counts[:, 1], counts[:, 2])
            if show_separate == True:
                plt.figure(figsize=figsize)
                plt.plot(list(t), x, label="Jumps along x-axis", c="red")
                if legend == True:
                    plt.legend()
                plt.show()
                plt.figure(figsize=figsize)
                plt.plot(list(t), y, label="Jumps along y-axis", c="green")
                if legend == True:
                    plt.legend()
                plt.show()
                plt.figure(figsize=figsize)
                plt.plot(list(t), z, label="Jumps along z-axis", c="blue")
                if legend == True:
                    plt.legend()
                plt.show()
            else:
                plt.figure(figsize=figsize)
                plt.plot(list(t), x, label="Jumps along x-axis", c="red")
                plt.plot(list(t), y, label="Jumps along y-axis", c="green")
                plt.plot(list(t), z, label="Jumps along z-axis", c="blue")
                if legend == True:
                    plt.legend()
                plt.show()
            if ret == True:
                return (t, pos)  # Return velocity jump counts

        except KeyError:
            print(
                "Boundary jump feature not found. It is only created for simulation data."
            )

    def show_tumor_surface_at_time(self, tp, phi:int=45, theta:int=45, **kwargs):
        positions = self.get_positions_at_time(
            tp, extraction_mode=ExtractionMode.non_gaslikes
        )
        verts, faces = get_tumor_surface_cloud_MC(
            positions, retTypes=["verts", "faces"], **kwargs
        )
        show_marching_cubes_surface(verts, faces,phi=phi,theta=theta)

    def vis3DInteractive(self):
        """
        Interactive 3D visualization of the loaded timepoints. Only works in Jupyter notebooks and requires ipyvolume
        """
        import ipyvolume as ipv  # Not as global dependency since it makes problems installing it sometimes
        from ipywidgets import interact, fixed

        def vIhelper(posFunc, time):
            positions = np.array(posFunc(time))
            ipv.clear()
            ipv.scatter(
                positions[:, 0], positions[:, 1], positions[:, 2], marker="sphere"
            )
            ipv.squarelim()
            ipv.show()

        interact(
            vIhelper,
            posFunc=fixed(self.get_positions_at_time),
            time=self.timestep_range,
        )

    def show_paths_at_time(self, tp, **kwargs):
        try:
            gaslikes = extract_gaslikes(self.data[tp])
            non_gaslikes = extract_non_gaslikes(self.data[tp])
            #            positions = self.get_positions_at_time(tp)
            paths = self.features["path_density"][tp]["paths"]
            show_paths(gaslikes, non_gaslikes, paths, **kwargs)
        except KeyError:
            print("Paths not loaded")

    def show_average_path_densities_at_time(self, tp, **kwargs):
        try:
            positions = self.get_positions_at_time(tp)
            feature = self.features["path_density"][tp]
            paths = deepcopy(feature["paths"])
            densities = deepcopy(feature["densities"])
            show_averaged_density_over_all_paths(positions, paths, densities, **kwargs)
        except KeyError:
            print("Paths and densities not loaded")


from operator import itemgetter
from itertools import groupby


def compress_list(l):
    l = sorted(l)
    ranges = []
    for k, g in groupby(enumerate(l), lambda x: x[0] - x[1]):
        group = map(itemgetter(1), g)
        group = list(map(int, group))
        ranges.append((group[0], group[-1]))
    return ranges


def combine_datahandlers(datahandlers: list, verbose: bool = False) -> DataHandler:
    feature_metadata = datahandlers[0].feature_metadata
    data = datahandlers[0].data
    features = datahandlers[0].features
    scale_factor = datahandlers[0].scale_factor
    for dh in datahandlers[1:]:
        if feature_metadata != dh.feature_metadata:
            print("Warning! Combining datahandlers with different feature metadata!")
            print("--------")
            print(feature_metadata)
            print("........")
            print(dh.feature_metadata)
            print("--------")
        # possible in python 3.9: data |= dh.data
        # possible in python 3.9: features |= dh.features
        data.update(dh.data)
        for feature_name, feature in features.items():
            if verbose:
                print(feature_name)
            try:
                feature.update(dh.features[feature_name])
            except KeyError:
                continue

        data = {key: val for key, val in sorted(data.items(), key=lambda ele: ele[0])}
    combined_datahandler = DataHandler(
        data=data,
        features=features,
        feature_metadata=feature_metadata,
        scale_factor=scale_factor,
    )
    return combined_datahandler


def copy_datahandler(datahandler):
    """Returns a deepcopy of a datahandler object"""
    new_dh = DataHandler(
        data=deepcopy(datahandler.data),
        features=deepcopy(datahandler.features),
        feature_metadata=deepcopy(datahandler.feature_metadata),
        scale_factor=datahandler.scale_factor,
    )
    return new_dh


FEATURE_FUNCTIONS = {
    "velocities": DataHandler.velocities,
    "volumes": DataHandler.volumes,
    "average_local_density": DataHandler.average_local_density,
    "individual_local_density": DataHandler.individual_local_density,
    "individual_velocity_correlation": DataHandler.individual_velocity_correlation,
    "average_velocity_correlation": DataHandler.average_velocity_correlation,
    "central_local_density": DataHandler.central_local_density,
    "central_velocity_correlation": DataHandler.central_velocity_correlation,
    "radially_averaged_local_density": DataHandler.radially_averaged_local_density,
    "radially_averaged_velocity_correlation": DataHandler.radially_averaged_velocity_correlation,
    "non_gaslike_fraction": DataHandler.non_gaslike_fraction,
    "radial_displacement": DataHandler.radial_displacement,
    "tumor_area": DataHandler.tumor_area,
    "tumor_surface_orientation": DataHandler.tumor_surface_orientation,
    "gaslike_dists": DataHandler.gaslike_dists,
}


DEFAULT_FEATURE_METADATA = {
    "volumes": {},
    "central_local_density": {"min_r": 0, "max_r": 800, "dr": 2},
    "average_local_density": {"min_r": 0, "max_r": 800, "dr": 2},
    "tumor_area": {"overwrite": True},
    "tumor_surface_orientation": {"reassign": True, "overwrite": True},
    "gaslike_dists": {},
}

DEFAULT_FEATURE_METADATA2 = {
    "velocities": {},
    # "volumes": {"max_volume": 40000},
    "central_local_density": {"min_r": 0, "max_r": 600, "dr": 10},
    "central_velocity_correlation": {"min_r": 0, "max_r": 600, "dr": 10},
}

PARALLELIZABLE_FEATURE_FUNCTIONS = {
    "volumes": DataHandler.volumes,
    "average_local_density": DataHandler.average_local_density,
    "individual_local_density": DataHandler.individual_local_density,
    "individual_velocity_correlation": DataHandler.individual_velocity_correlation,
    "average_velocity_correlation": DataHandler.average_velocity_correlation,
    "central_local_density": DataHandler.central_local_density,
    "central_velocity_correlation": DataHandler.central_velocity_correlation,
    "radially_averaged_local_density": DataHandler.radially_averaged_local_density,
    "radially_averaged_velocity_correlation": DataHandler.radially_averaged_velocity_correlation,
    "non_gaslike_fraction": DataHandler.non_gaslike_fraction,
    "tumor_area": DataHandler.tumor_area,
    "tumor_surface_orientation": DataHandler.tumor_surface_orientation,
    "gaslike_dists": DataHandler.gaslike_dists,
}


def tracklets_to_dataframes(tracklets: dict, timestep_range: tuple = None):
    if timestep_range is None:
        timestep_range = (0, len(tracklets["tracklets_per_timepoint"]), 1)
    data = {}
    pos = tracklets["tracklet_positions"]
    start = tracklets["tracklet_startTimes"]
    for tp in range(*timestep_range):
        tracklets_at_tp = tracklets["tracklets_per_timepoint"][tp]
        positions_at_timepoint = {
            cellID: pos[cellID][tp - start[cellID]] for cellID in tracklets_at_tp
        }
        positions_at_timepoint = [
            {"CellID": cellID, "CenterX": v[0], "CenterY": v[1], "CenterZ": v[2]}
            for cellID, v in positions_at_timepoint.items()
        ]
        df = pd.DataFrame(data=positions_at_timepoint)
        # df["pos"] = list(positions_at_timepoint.values()).set_index("CellID")
        data[tp] = df.set_index("CellID")
    return data


def mat_to_dataframe(all_nuclei_mat, single_cells_mat: dict = None):
    all_nuclei_numpy = np.array(all_nuclei_mat, dtype=np.float64)
    columns = (
        {0: "CenterX", 1: "CenterY", 2: "CenterZ"}
        if all_nuclei_numpy.shape[1] == 3
        else {0: "CellID", 1: "CenterX", 2: "CenterY", 3: "CenterZ"}
    )
    df = pd.DataFrame(all_nuclei_numpy).rename(columns=columns)
    single_cells = (
        pd.DataFrame(np.array(single_cells_mat, dtype=np.float64)).rename(
            columns=columns
        )
        if isinstance(single_cells_mat, np.ndarray)
        else None
    )
    if isinstance(single_cells, pd.DataFrame):
        df = df.merge(single_cells, how="left", indicator="single_cell")
        df["single_cell"] = df["single_cell"] == "both"
        df = df.rename(columns={"single_cell": "aggregation_state"})
        indices = np.where(df["aggregation_state"] == True)[0]
        df["aggregation_state"] = np.zeros(df["CenterX"].size, dtype=int)
        if indices.size > 0:
            df.loc[indices, "aggregation_state"] = 2
    else:
        df["aggregation_state"] = np.zeros(df["CenterX"].size, dtype=int)
    df["CellID"] = df.index
    df = df.set_index("CellID")
    if not df.index.is_unique:
        print("Warning! Duplicate CellIDs detected. Dropping duplicate IDs.")
        df = df[~df.index.duplicated(keep="first")]
    return df
