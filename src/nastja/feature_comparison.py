from .DataHandler import DataHandler
from .metrics import (
    sliced_wasserstein_distance_over_time,
    mse,
    ordered_wasserstein_distance,
    unordered_wasserstein_distance,
    euclidean_distance,
)
from .utils import (
    standardize_data,
    fit_lambdas
)
import pandas as pd
import numpy as np

METRICS_FOR_FEATURES_OVER_TIME = {
    "volumes": sliced_wasserstein_distance_over_time,
    "velocities": sliced_wasserstein_distance_over_time,
    "velocity_correlation": sliced_wasserstein_distance_over_time,
}

METRICS_FOR_FEATURES = {
    "velocities": unordered_wasserstein_distance,
    "volumes": unordered_wasserstein_distance,
    "average_local_density": ordered_wasserstein_distance,
    "individual_local_density": ordered_wasserstein_distance,
    "individual_velocity_correlation": ordered_wasserstein_distance,
    "average_velocity_correlation": ordered_wasserstein_distance,
    "central_local_density": ordered_wasserstein_distance,
    "central_velocity_correlation": ordered_wasserstein_distance,
    "radially_averaged_local_density": ordered_wasserstein_distance,
    "radially_averaged_velocity_correlation": ordered_wasserstein_distance,
    "non_gaslike_fraction": mse,
    "tumor_area": mse,
    "tumor_surface_orientation": ordered_wasserstein_distance,
    "gaslike_dists": euclidean_distance,
    # TODO: which metric for radial displacement?
    "radial_displacement": None,
}


IS_FEATURE_ORDERED = {
    "velocities": False,
    "volumes": False,
    "average_local_density": True,
    "individual_local_density": True,
    "individual_velocity_correlation": True,
    "average_velocity_correlation": True,
    "central_local_density": True,
    "central_velocity_correlation": True,
    "radially_averaged_local_density": True,
    "radially_averaged_velocity_correlation": True,
    "non_gaslike_fraction": False,
    "tumor_surface_orientation": True,
    "gaslike_dists": True,
    "radial_displacement": True,
}


def filter_nan_from_gaslike_dists(feature_dict):
    for timestep, dists in feature_dict["gaslike_dists"].items():
        if any(np.isnan(dists)):
            feature_dict["gaslike_dists"][timestep] = [0.0, 0.0]
    return feature_dict


def compare_datahandlers(
    dh1: DataHandler,
    dh2: DataHandler,
    feature_names = None,
    timestep_mapping: list = [(1, 1), (249, 249)],
):
    dh1.extract_all_features()
    dh2.extract_all_features()
    return compare_feature_dicts(dh1.features,dh2.features,feature_name=feature_names,timestep_mapping = timestep_mapping)
    # if len(feature_names) == 0:
    #     feature_names = set(dh1.features.keys()).union(set(dh2.features.keys()))

    # N_timesteps = len(timestep_mapping)
    # results = pd.DataFrame()
    # results["tp1"] = [tp[0] for tp in timestep_mapping]
    # results["tp2"] = [tp[1] for tp in timestep_mapping]
    # for feature in feature_names:
    #     results[feature] = np.zeros(N_timesteps)

    # results.index = [tp[0] for tp in timestep_mapping]
    # # TODO More than one tp1 will lead to problems
    # # TODO Call compare_feature_dicts here
    # # results.index = range(*timestep_mapping)
    # for tp1, tp2 in timestep_mapping:
    #     for feature_name in feature_names:
    #         results.at[tp1, feature_name] = METRICS_FOR_FEATURES[feature_name](
    #             dh1.features[feature_name][tp1], dh2.features[feature_name][tp2]
    #         )
    # return results


def compare_feature_dicts(
    feat_dict1: dict, feat_dict2: dict, feature_name=None, timestep_mapping: list = None
):
    if feature_name is None:
        feature_names = set(feat_dict1.keys()).intersection(set(feat_dict2.keys()))
        # feature_names = set(feat_dict1.keys()).union(set(feat_dict2.keys()))
    else:
        if isinstance(feature_name, list):
            feature_names = feature_name
        else:
            feature_names = [feature_name]

    if "gaslike_dists" in feature_names:
        feat_dict1 = filter_nan_from_gaslike_dists(feat_dict1)
        feat_dict2 = filter_nan_from_gaslike_dists(feat_dict2)

    # print('Comparing the following features:')
    # print(feature_names)
    results = []
    for feature_name in feature_names:
        if timestep_mapping is None:
            assert len(feat_dict1[feature_name]) == len(
                feat_dict2[feature_name]
            ), "Timestep mapping is required when number of timesteps differ."
            # timestep_mapping = zip(range(len(feat_dict1[feature_name])), range(len(feat_dict1[feature_name])))
            timestep_mapping_for_this_feature = zip(
                feat_dict1[feature_name], feat_dict2[feature_name]
            )
        else:
            timestep_mapping_for_this_feature = timestep_mapping
        # print(f"Comparing {feature_name}")
        results.extend(
            [
                {
                    "feature": feature_name,
                    "timestep1": tp1,
                    "timestep": tp2,
                    "distance": METRICS_FOR_FEATURES[feature_name](
                        feat_dict1[feature_name][tp1], feat_dict2[feature_name][tp2]
                    ),
                }
                for tp1, tp2 in timestep_mapping_for_this_feature
            ]
        )
    return results


def compare_feature_dict_to_macrospheroids(
    feature_dict: dict,
    macrospheroids_feature_dicts: list,
    timestep_mapping: list = None,
    timestep_margin: int = 5,
):
    if timestep_mapping is None:
        timestep_mapping = list(zip([10, 30, 110, 190], range(4)))
    timestep_mapping = [
        (tp1 + i, tp2)
        for tp1, tp2 in timestep_mapping
        for i in range(-timestep_margin, timestep_margin)
    ]
    results = []
    for it, macrospheroid in enumerate(macrospheroids_feature_dicts):
        it_results = compare_feature_dicts(
            feature_dict, macrospheroid, timestep_mapping=timestep_mapping
        )
        for row in it_results:
            row.update({"it": it})
        results.extend(it_results)
    return pd.DataFrame(results)


def get_overall_deviation_score(
    dh1: DataHandler,
    dh2: DataHandler,
    lambda_dict: dict,
    standardization_factors: dict,
    timestep_mapping,
    feature_metadata: dict = None,
) -> float:
    if feature_metadata is None:
        feature_metadata = {}
    dh1.feature_metadata.update(feature_metadata)
    dh2.feature_metadata.update(feature_metadata)
    dh1.extract_all_features()
    dh2.extract_all_features()

    feature_cmp = compare_datahandlers(
        dh1,
        dh2,
        timestep_mapping=timestep_mapping,
        feature_names=[feature for feature in lambda_dict.keys()],
    )

def calculate_overall_deviation_score(
    cmp_dict : dict,
    lambda_dict: dict,
    standardization_factors: dict,
    timestep_mapping):

    deviation_score = {tp: 0.0 for tp in range(*timestep_mapping)}

    for i,feature in enumerate(lambda_dict.keys()):
        mean_dist, std_dist, dist_shift = standardization_factors[feature]
        try:
            for tp in range(*timestep_mapping):
                deviation_score[tp] += lambda_dict[feature] * (
                    (cmp_dict[i]["distance"] - mean_dist) / std_dist + dist_shift
                )
        except KeyError:
            print("Not all features accounted for! Check your metadata")
            return -1

    return deviation_score


def compare_dataset(datahandler_dict,timestep, features_to_compare: list = None, standardization_factors:dict = None, lambdas: dict = None):
    assert len(datahandler_dict) > 1, "This function needs at least two datahandlers!"
    if features_to_compare == None:
        features_to_compare = list(datahandler_dict[list(datahandler_dict.keys())[0]].feature_metadata.keys())

    print("Extracting features...")
    for key in datahandler_dict.keys():
        datahandler_dict[key].extract_all_features()
    cmp_dict = {}
    print("Comparing datahandlers...")
    for key_i,dh_i in datahandler_dict.items():
        for key_j, dh_j in datahandler_dict.items():
            cmp_dict[(key_i[0],key_i[1],key_j[0],key_j[1])] = compare_feature_dicts(dh_i.features,dh_j.features,feature_name=features_to_compare,timestep_mapping=[(timestep,timestep)])

    if standardization_factors == None:
        print("Calculating standardization factors...")
        standardization_factors = standardize_data(cmp_dict,featureList=features_to_compare,timestep=timestep)
    if lambdas == None:
        print("Fitting lambdas")
        lambdas = fit_lambdas(cmp_dict,features_to_compare,standardization_factors=standardization_factors,timestep=timestep)
    print("Calculating overall deviation scores...")
    overall_deviation_scores = {key : calculate_overall_deviation_score(cmp_dict[key], lambdas, standardization_factors, timestep_mapping=(timestep,timestep+1,1)) for key in cmp_dict.keys()}
    print("Done!")
    return cmp_dict, standardization_factors, lambdas, overall_deviation_scores