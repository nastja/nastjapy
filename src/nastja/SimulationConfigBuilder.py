from copy import copy, deepcopy
import json
import jsbeautifier
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import os
import pandas as pd
import pickle
import scipy.io as sio
from scipy.spatial import Voronoi, voronoi_plot_2d
from scipy.spatial import ConvexHull
from scipy.spatial import cKDTree
from scipy.stats import wasserstein_distance
from time import time

import h5py

from nastja.DataHandler import DataHandler
from nastja.templates import NASTJA_TEMPLATE_1
from nastja.utils import find_highest_density

# TODO Integrate SimulationGenerator into NastjaPy framework

DEFAULT_SETTINGS = {"box_size": 200.,
                                "box_padding": 50,
                                "block_count": [2, 2, 2],
                                "space_scaling_factor": 1.0,
                                "time_scaling_factor": 1,
                                # chosen to allow for max speed as seen in data; 1 timestep in data = 5 timesteps in sim
                                "initial_cell_size": 5,
                                "min_num_points": 50,
                                "simulation_length": 250000,
                                "sim_box_border_size": 0,
                                "particle_id": 0,
                                "outdir": "test",
                                "liquid": 1,
                                "celltypes": [1]}


def load_template(template):
    if template is None:
        return
    if isinstance(template, dict):
        return deepcopy(template)
    if isinstance(template, str):
        with open(template, 'r') as f:
            return json.load(f)
    print("No template specified. Using default template.")


def cell_entry_from_position(position, radius, celltype):
    return {"shape": "sphere",
            "center": position.astype(int).tolist(),
            "radius": radius,
            "celltype": celltype}


def deep_update(d, u):
    """
    Updates d with u recursively.
    """
    for key, value in u.items():
        if isinstance(value, dict):
            # If value is a dictionary, recursively call update_json
            d[key] = update_json(d.get(key, {}), value)
        elif isinstance(value, list):
            # If value is a list, extend d's list with u's list
            d[key] = d.get(key, []) + value
        else:
            # Otherwise, update d's value with u's value
            d[key] = value
    return d


def beautify_config(config, indent=2):
    options = jsbeautifier.default_options()
    options.indent_size = 2
    return jsbeautifier.beautify(json.dumps(config), options)


def update_parameter_in_config(config, param_name: str, param_value):
    match param_name:
        case "temperature":
            config["CellsInSilico"]["temperature"] = param_value
        case "volume" | "surface":
            config["CellsInSilico"][param_name]["default"]["value"] = param_value
        case "volume_lambda" | "surface_lambda":
            config["CellsInSilico"][param_name[:-7]]["lambda"][-1] = param_value
        case "adhesion":
            #if type(param_value) is not list:
            #    param_value = [param_value]
            #adhesion_matrix = np.zeros((len(param_value) + 2, len(param_value) + 2))
            adhesion_matrix = np.zeros((10, 10))
            adhesion_matrix[-1, -1] = param_value
            adhesion_matrix = adhesion_matrix.tolist()
            config["CellsInSilico"]["adhesion"]["matrix"] = adhesion_matrix
        case 'timesteps' | "randomseed":
            config['Settings'][param_name] = param_value
        case "liquid":
            config["CellsInSilico"]["liquid"] = param_value
        case "blockcount" | "blocksize":
            config["Geometry"][param_name] = param_value
        case "motility":
            config["CellsInSilico"]["orientation"]["motilityamount"][-1] = param_value
        case "persistence":
            config["CellsInSilico"]["orientation"]["persistenceMagnitude"] = param_value
        case "cell_division_rate":
            config["CellsInSilico"]["division"]["condition"][-1] = f"( volume >= 0.9 * volume0 )  & ( rnd() <= {param_value} )"
        case _:
            raise ValueError(f"Unknown parameter name {param_name}.")
    return config


def determine_volume(datahandler: DataHandler, tp: int = 0, use_entire_trajectory: bool = False):
    if not use_entire_trajectory:
        volumes = datahandler.get_volumes_at_time(tp)
    else:
        volumes = np.concatenate([datahandler.get_volumes_at_time(tp) for tp in datahandler.data.keys()])
    most_probable_volume = int(find_highest_density(volumes) * .1)
    return most_probable_volume


def translate_pointcloud_to_center_of_box(pointcloud, block_count, block_size):
    """
    Translates a pointcloud to the center of the box.

    pointcloud: np.array of shape (n, 3)
    block_count: list of length 3
    block_size: list of length 3
    """
    center_of_pointcloud = np.mean(pointcloud, axis=0)
    center_of_box = np.array(block_count) * np.array(block_size) / 2
    translation_vector = center_of_box - center_of_pointcloud
    return pointcloud + translation_vector


class SimulationConfigBuilder:

    def __init__(self,
                 config_template=NASTJA_TEMPLATE_1,
                 filling_template=None,
                 space_scaling_factor: float = 1.,
                 time_scaling_factor: float = 1.,
                 seperate_filling_config: bool = False,
                 settings=None):

        self.config_template = load_template(config_template)
        self.filling_template = load_template(filling_template)
        self.config = deepcopy(self.config_template)
        self.filling = deepcopy(self.filling_template)
        self.seperate_filling_config = seperate_filling_config
        if settings is None:
            self.settings = {"celltype": 9}
        else:
            self.settings = settings

    def generate_filling(self, positions, initial_cell_volume=None):
        # positions = (self.settings["space_scaling_factor"] * self.dh.get_positions_at_time(tp)).astype(int)
        positions = translate_pointcloud_to_center_of_box(positions, self.config["Geometry"]["blockcount"], self.config["Geometry"]["blocksize"])
        if (positions < 0).any():
            raise ValueError("Positions are not within the box.")
        if initial_cell_volume is None:
            initial_cell_volume = self.config["CellsInSilico"]["volume"]["default"]["value"]
        radius = int(np.floor(((3 * initial_cell_volume) / (4 * np.pi)) ** (1/3)))
        self.filling = {'cells': [cell_entry_from_position(pos, radius, celltype=self.settings["celltype"]) for pos in positions]}
        return self.filling
    
    def generate_random_spheroid_filling(self, num_cells, initial_cell_volume=None):
        if initial_cell_volume is None:
            initial_cell_volume = self.config["CellsInSilico"]["volume"]["default"]["value"]
        # generate random positions uniformly distributed in a sphere
        radius = int(np.floor(((3 * initial_cell_volume) / (4 * np.pi)) ** (1/3)))

        positions = np.random.normal(scale= ,size=(num_cells, 3))
        # round positions to closest integer and convert to integer
        positions = np.round(positions).astype(int)
        
        positions = translate_pointcloud_to_center_of_box(positions, self.config["Geometry"]["blockcount"], self.config["Geometry"]["blocksize"])


    def update_parameters(self, parameters: dict, is_in_config_syntax: bool = False):
        """
        Updates the config with the specified parameters.
        If parameter name is not in the look-up function (update_parameter_in_config), the full path to the parameter
        can be specified by setting is_in_config_syntax to True.
        is_in_config_syntax can also be used when using an existing config file as update.
        """
        if not is_in_config_syntax:
            for param_name, param_val in parameters.items():
                self.config = update_parameter_in_config(self.config, param_name, param_val)
        else:
            self.config = deep_update(self.config, parameters)
        return self.config

    def update_cell_division_rate(self, initial_cell_count, final_cell_count, num_timesteps=None):
        """
        Updates the config with a cell division rate that results in the specified final cell count after the specified
        number of timesteps.
        """
        if num_timesteps is None:
            num_timesteps = self.config["Settings"]["timesteps"]
        cell_division_rate = np.log(final_cell_count / initial_cell_count) / num_timesteps
        self.config = update_parameter_in_config(self.config, "cell_division_rate", cell_division_rate)
        return self.config

    def update_config_template(self, parameters: dict, is_in_config_syntax: bool = False):
        """
        Updates the config with the specified parameters.
        If parameter name is not in the look-up function (update_parameter_in_config), the full path to the parameter
        can be specified by setting is_in_config_syntax to True.
        is_in_config_syntax can also be used when using an existing config file as update.
        """
        if not is_in_config_syntax:
            for param_name, param_val in parameters.items():
                self.config_template = update_parameter_in_config(self.config_template, param_name, param_val)
        else:
            self.config_template = deep_update(self.config_template, parameters)
        return self.config

    def generate_simulation_config(self, positions=None, parameters_to_update: dict = {}, ):
        self.config = deepcopy(self.config_template)
        self.update_parameters(parameters=parameters_to_update)
        if positions is not None:
            self.filling = self.generate_filling(positions)

        if self.filling is not None:
            if self.seperate_filling_config:
                self.config['Include'] = f"filling.json"
            else:
                self.config['Filling'] = self.filling

    def dump_config(self, config_dir, config_name=None, indent=2):
        if config_name is not None:
            config_name = f"{config_name}_conf.json"
            filling_name = f"{config_name}_filling.json"
        else:
            config_name = "conf.json"
            filling_name = "filling.json"
        config_filepath = os.path.join(config_dir, config_name)
        with open(config_filepath, "w+") as f:
            f.write(beautify_config(self.config, indent=indent))
        if self.seperate_filling_config:
            self.config["Include"] = f"{filling_name}"
            filling_filepath = os.path.join(config_dir, filling_name)
            with open(filling_filepath, "w+") as f:
                f.write(beautify_config(self.filling_config, indent=indent))

        return config_filepath
