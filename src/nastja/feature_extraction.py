import numpy as np
from pandas.core.frame import DataFrame
from scipy.spatial import Voronoi
from scipy.spatial import ConvexHull
from itertools import combinations
from sklearn.neighbors import KDTree

# import pyscal.core as pc
from .utils import dist, create_distance_cache, pointwise_norm, create_dot_cache
from .utils import compute_neighbours
from .utils import (
    get_circumference,
    extract_wall_points,
    rot,
    get_marching_cubes_tumor_surface,
)


def get_radial_distance_histogram(positions, min_r, max_r, dr):
    counts = np.zeros((int(max_r / dr) - min_r))
    centroid = positions.mean(axis=0)
    radial_distances = np.sum(np.abs(positions - centroid) ** 2, axis=-1) ** (1.0 / 2)
    for distance in radial_distances:
        if distance < max_r:
            counts[int(distance / dr) - min_r] += 1
    return counts / len(positions)


def get_absolute_velocity_histogram(velocities, dv=10, max_v=600, min_v=0):
    counts = np.zeros((int(max_v / dv) - min_v))
    absolute_velocities = np.sum(np.abs(velocities) ** 2, axis=-1) ** (1.0 / 2)

    for vel in absolute_velocities:
        if vel < max_v:
            counts[int(vel / dv) - min_v] += 1
    result = counts / len(velocities)
    return result


def get_radial_distance_and_absolute_velocity_histogram(
    positions, velocities, min_r, max_r, dr, min_v, max_v, dv
):
    assert len(positions) == len(
        velocities
    ), "Positions array and velocities array need to be of the same length!"
    counts = np.zeros((int(max_r / dr) - min_r), (int(max_v / dv) - min_v))
    centroid = positions.mean(axis=0)
    radial_distances = np.sum(np.abs(positions - centroid) ** 2, axis=-1) ** (1.0 / 2)
    absolute_velocities = np.sum(np.abs(velocities) ** 2, axis=-1) ** (1.0 / 2)
    for distance, vel in zip(radial_distances, absolute_velocities):
        counts[int(distance / dr) - min_r, int(vel / dv) - min_v] = +1
    result = counts / len(velocities)
    return result


def get_volumes(positions, max_volume=40000):
    try:
        vor = Voronoi(positions)
        regions = [
            np.array([vor.vertices[vertex] for vertex in region])
            for region in vor.regions
            if len(region) > 3
        ]
        volumes = []
        for region in regions:
            try:
                volumes.append(ConvexHull(region).volume)
            except:
                pass
        # volumes = [ConvexHull(region, qhull_options= "QbB").volume for region in regions]
        volumes = np.array(list(filter(lambda x: 0 < x < max_volume, volumes)))
    except ValueError:
        volumes = np.array([-1])

    return volumes


def get_individual_local_density_function(positions, min_r=0, max_r=600, dr=10):
    counts = np.zeros((len(positions), int(max_r / dr) - min_r))
    try:
        pairwise_distances = create_distance_cache(positions)
        for pair_num, (i, j) in enumerate(combinations(range(len(positions)), 2)):
            distance = pairwise_distances[pair_num]
            if distance < max_r:
                counts[i, int(distance / dr) - min_r] += 1
                counts[j, int(distance / dr) - min_r] += 1
    except MemoryError:
        print(
            "Warning: Point cloud too big for fast computation, falling back on old version."
        )
        for i, j in combinations(range(len(positions)), 2):
            distance = dist(positions[i], positions[j])
            if distance < max_r:
                counts[i, int(distance / dr) - min_r] += 1
                counts[j, int(distance / dr) - min_r] += 1
    return counts / len(positions)


def get_radially_averaged_local_density_function(positions, min_r=0, max_r=600, dr=10):
    centroid = positions.mean(axis=0)
    try:
        distances_to_centroid = pointwise_norm(positions - centroid)
        pairwise_distances = create_distance_cache(positions)
        counts = np.zeros(((int(max_r / dr) - min_r), int(max_r / dr) - min_r))
        for num_pair, (i, j) in enumerate(combinations(range(len(positions)), 2)):
            # There might be a faster way for this part...
            distance = pairwise_distances[num_pair]
            r_i = distances_to_centroid[i]
            r_j = distances_to_centroid[j]
            if distance < max_r and r_i < max_r and r_j < max_r:
                counts[int(r_i / dr) - min_r, int(distance / dr) - min_r] += 1
                counts[int(r_j / dr) - min_r, int(distance / dr) - min_r] += 1
    except MemoryError:
        print(
            "Warning: Point cloud too big for fast computation, falling back on old version."
        )
        counts = np.zeros(((int(max_r / dr) - min_r), int(max_r / dr) - min_r))
        for i, j in combinations(range(len(positions)), 2):
            distance = dist(positions[i], positions[j])
            r_i = dist(positions[i], centroid)
            r_j = dist(positions[j], centroid)
            if distance < max_r and r_i < max_r and r_j < max_r:
                counts[int(r_i / dr) - min_r, int(distance / dr) - min_r] += 1
                counts[int(r_j / dr) - min_r, int(distance / dr) - min_r] += 1
    return counts / len(positions)


def get_average_local_density_function(positions, min_r=0, max_r=600, dr=10):
    try:
        distance_cache = create_distance_cache(positions)
        counts = np.histogram(
            distance_cache, range=(min_r, max_r), bins=int((max_r - min_r) / dr)
        )[0]
    except MemoryError:
        print(
            "Warning: Point cloud too big for fast computation, falling back on old version."
        )
        counts = np.zeros(int((max_r - min_r) / dr))
        for i, j in combinations(range(len(positions)), 2):
            distance = dist(positions[i], positions[j])
            if distance < max_r:
                counts[int(distance / dr) - min_r] += 1
    return counts / len(positions)


def get_central_local_density_function(positions, min_r=0, max_r=600, dr=10):
    centroid = positions.mean(axis=0)
    try:
        distance_cache = pointwise_norm(positions - centroid)
        counts = np.histogram(
            distance_cache, range=(min_r, max_r), bins=int((max_r - min_r) / dr)
        )[0]
    except MemoryError:
        print(
            "Warning: Point cloud too big for fast computation, falling back on old version."
        )
        counts = np.zeros(int((max_r - min_r) / dr))
        for i in range(len(positions)):
            distance = dist(positions[i], centroid)
            if distance < max_r:
                counts[int(distance / dr)] += 1
    return counts / len(positions)


def get_radial_local_density_function(
    positions,
    center=(0, 0, 0),
    min_r=0,
    max_r=400,
    dr=10,
    check_dist=40,
    N_phis=10,
    N_thetas=10,
    variable_check_dist=False,
    mean_scale_factor=4,
):
    """
    Calculates the radial local density function by sampling points within layers of increasing distance to the center and averaging over these

    returns:

    tuple containing the bins(upper boundaries), density values and density standard deviations

    args:

    positions: x,y,z coordinates of all cell centers. Needs to be a numpy array with shape (N_cells,3)

    kwargs:

    min_r (= 0): starting distance of density calculation. Can usually be left at 0
    max_r (= 400): End distance of density calculation. Choose depending on your system
    dr (= 10): Thickness of sphere layers within which points are sampled
    check_dist (= 40): Radius of the spherical volume checked around each sample point
    N_phis (= 10): Number of sample points within each z-layer
    N_thetas (= 10): Number of sample z-layers
    variable_check_dist( = False): If set to False, the variable check_dist is used as is. Otherwise it is calculated from the average closest distance between particles in positions
    mean_scale_factor( = 6): Scale factor for check_dist in case cariable_check_dist = True



    """

    bins = np.linspace(min_r, max_r, int(max_r / dr) + 1)[1:]
    #    bin_counts = np.full(bins.shape,0.)
    #    bin_thickness = bins[1]-bins[0]
    N_positions = positions[:, 0].size
    vals = np.full(bins.shape, 0.0)
    val_std_dev = np.full(bins.shape, 0.0)
    if variable_check_dist == True:
        tree = KDTree(positions, 2)
        nearest_dist, nearest_ind = tree.query(positions, k=2)
        average_dist = np.sum(nearest_dist[:, 1]) / N_positions
        check_dist = mean_scale_factor * average_dist

    val_arr = np.full(
        N_phis * N_thetas, 0.0
    )  # Stores all local density values for a single layer (for averaging and std_deviation extraction)

    # center = (np.sum(positions[:,0])/N_positions, np.sum(positions[:,1])/N_positions, np.sum(positions[:,0])/N_positions)#Center of positions

    for i, r in enumerate(bins):  # iterate through layers
        count = 0
        for theta in np.linspace(
            0, np.pi, N_thetas
        ):  # iterate through z_coordinates within layer
            for phi in np.linspace(
                0, 2 * np.pi, N_phis
            ):  # iterate through xy_coordinates within layer
                vec = np.array(
                    [
                        center[0] + r * np.sin(theta) * np.cos(phi),
                        center[1] + r * np.sin(theta) * np.cos(phi),
                        center[2] + r * np.cos(theta),
                    ]
                )
                N_cells_inside = np.sum(
                    np.sqrt(
                        (vec[0] - positions[:, 0]) ** 2
                        + (vec[1] - positions[:, 1]) ** 2
                        + (vec[2] - positions[:, 2]) ** 2
                    )
                    < check_dist
                )
                val_arr[count] = N_cells_inside
                count += 1
        vals[i] = np.mean(val_arr)
        val_std_dev[i] = np.std(val_arr)

    test_vol = (
        4.0 * np.pi * check_dist**3 / 3.0
    )  # Volume which is investigated around each sample point
    vals[:] /= test_vol
    val_std_dev[:] /= test_vol

    return bins, vals, val_std_dev


def get_individual_velocity_correlation_function(
    positions, velocities, dr=10, max_r=600, min_r=0
):
    correlations = np.zeros(
        (len(positions), int(max_r / dr) - min_r)
    )  # [[[] for _ in range(int(max_r/dr)-min_r)] for _ in range(len(positions))]
    counts = np.zeros((len(positions), int(max_r / dr) - min_r))

    # for i,j in tqdm(combinations(range(len(positions)), 2)):
    with np.errstate(invalid="ignore"):
        for i, j in combinations(range(len(positions)), 2):
            vel_i, vel_j = velocities[i], velocities[j]
            if isinstance(vel_i, np.ndarray) and isinstance(vel_j, np.ndarray):
                distance = dist(positions[i], positions[j])
                if distance < max_r:
                    vel_i_abs, vel_j_abs = np.linalg.norm(vel_i), np.linalg.norm(vel_j)
                    cor = np.dot(vel_i, vel_j) / (vel_j_abs * vel_i_abs)
                    correlations[i][int(distance / dr) - min_r] += cor
                    correlations[j][int(distance / dr) - min_r] += cor
                    counts[i][int(distance / dr) - min_r] += 1
                    counts[j][int(distance / dr) - min_r] += 1
        result = correlations / counts
    return result


def get_radially_averaged_velocity_correlation_function(
    positions, velocities, dr=10, max_r=600, min_r=0
):
    centroid = positions.mean(axis=0)
    correlations = np.zeros((int(max_r / dr) - min_r, int(max_r / dr) - min_r))
    counts = np.zeros((int(max_r / dr) - min_r, int(max_r / dr) - min_r))
    try:
        distances = create_distance_cache(positions)
        distances_to_centroid = pointwise_norm(positions - centroid)
        absolute_velocities = pointwise_norm(velocities)
        dots = create_dot_cache(velocities)
        for num_pair, (i, j) in enumerate(combinations(range(len(positions)), 2)):
            distance = distances[num_pair]
            if distance < max_r:
                r_i = distances_to_centroid[i]
                r_j = distances_to_centroid[j]
                if r_i < max_r and r_j < max_r:
                    cor = dots[num_pair] / (
                        absolute_velocities[i] * absolute_velocities[j]
                    )
                    correlations[int(r_i / dr) - min_r][
                        int(distance / dr) - min_r
                    ] += cor
                    correlations[int(r_j / dr) - min_r][
                        int(distance / dr) - min_r
                    ] += cor
                    counts[int(r_i / dr) - min_r][int(distance / dr) - min_r] += 1
                    counts[int(r_j / dr) - min_r][int(distance / dr) - min_r] += 1
        result = correlations
    except MemoryError:
        with np.errstate(invalid="ignore"):
            for i, j in combinations(range(len(positions)), 2):
                vel_i, vel_j = velocities[i], velocities[j]
                if isinstance(vel_i, np.ndarray) and isinstance(vel_j, np.ndarray):
                    distance = dist(positions[i], positions[j])
                    if distance < max_r:
                        r_i = dist(positions[i], centroid)
                        r_j = dist(positions[j], centroid)
                        if r_i < max_r and r_j < max_r:
                            vel_i_abs, vel_j_abs = np.linalg.norm(
                                vel_i
                            ), np.linalg.norm(vel_j)
                            cor = np.dot(vel_i, vel_j) / (vel_j_abs * vel_i_abs)
                            correlations[int(r_i / dr) - min_r][
                                int(distance / dr) - min_r
                            ] += cor
                            correlations[int(r_j / dr) - min_r][
                                int(distance / dr) - min_r
                            ] += cor
                            counts[int(r_i / dr) - min_r][
                                int(distance / dr) - min_r
                            ] += 1
                            counts[int(r_j / dr) - min_r][
                                int(distance / dr) - min_r
                            ] += 1
        result = correlations
    return result


def get_average_velocity_correlation_function(
    positions, velocities, dr=10, max_r=600, min_r=0
):
    correlations = np.zeros(
        int(max_r / dr) - min_r
    )  # [[[] for _ in range(int(max_r/dr)-min_r)] for _ in range(len(positions))]
    counts = np.zeros(int(max_r / dr) - min_r)

    try:
        distances = create_distance_cache(positions)
        absolute_velocities = pointwise_norm(velocities)
        dots = create_dot_cache(velocities)
        with np.errstate(invalid="ignore"):
            for num_pair, (i, j) in enumerate(combinations(range(len(positions)), 2)):
                distance = distances[num_pair]
                if distance < max_r:
                    cor = dots[num_pair] / (
                        absolute_velocities[i] * absolute_velocities[j]
                    )
                    correlations[int(distance / dr) - min_r] += cor
                    correlations[int(distance / dr) - min_r] += cor
                    counts[int(distance / dr) - min_r] += 1
                    counts[int(distance / dr) - min_r] += 1
    except MemoryError:
        with np.errstate(invalid="ignore"):
            for i, j in combinations(range(len(positions)), 2):
                vel_i, vel_j = velocities[i], velocities[j]
                if isinstance(vel_i, np.ndarray) and isinstance(vel_j, np.ndarray):
                    distance = dist(positions[i], positions[j])
                    if distance < max_r:
                        vel_i_abs, vel_j_abs = np.linalg.norm(vel_i), np.linalg.norm(
                            vel_j
                        )
                        cor = np.dot(vel_i, vel_j) / (vel_j_abs * vel_i_abs)
                        correlations[int(distance / dr) - min_r] += cor
                        correlations[int(distance / dr) - min_r] += cor
                        counts[int(distance / dr) - min_r] += 1
                        counts[int(distance / dr) - min_r] += 1
    result = correlations
    return result


def get_central_velocity_correlation_function(
    positions, velocities, dr=1, max_r=600, min_r=0
):
    centroid_vel = np.array([0, 0, 1])
    centroid = positions.mean(axis=0)
    try:
        absolute_velocities = pointwise_norm(velocities)
        distances_to_centroid = pointwise_norm(positions - centroid)
        cors = (
            np.sum(np.repeat([[0, 0, 1]], len(velocities), axis=0) * velocities, axis=1)
            / absolute_velocities
        )
        counts = np.histogram(
            distances_to_centroid, range=(min_r, max_r), bins=int((max_r - min_r) / dr)
        )[0]
        correlations = np.zeros(int(max_r / dr) - min_r)
        for i in range(len(positions)):
            distance = distances_to_centroid[i]
            correlations[int(distance / dr) - min_r] += cors[i]
    except MemoryError:
        print(
            "Warning: Point cloud too big for fast computation, falling back on old version."
        )
        correlations = np.zeros(int(max_r / dr) - min_r)
        counts = np.zeros(int(max_r / dr) - min_r)
        with np.errstate(invalid="ignore"):
            for i in range(len(positions)):
                vel_i = velocities[i]
                if isinstance(vel_i, np.ndarray):
                    distance = dist(positions[i], centroid)
                    if distance < max_r:
                        vel_i_abs = np.linalg.norm(vel_i)
                        cor = np.dot(vel_i, centroid_vel) / vel_i_abs
                        correlations[int(distance / dr) - min_r] += cor
                        counts[int(distance / dr) - min_r] += 1
    result = correlations / counts
    return result


"""
def construct_pyscal_system(positions: np.array, ids) -> pc.System:
    
    box = [[np.min(positions[i]), np.max(positions[i])] for i in range(1, 3)]
    atoms = [pc.Atom(pos=list(pos), id=int(id)) for id,pos in zip(ids,positions)]
    system = pc.System()
    system.atoms = atoms
    #system.box = box
    return system


def get_steinhardt_parameters(positions: np.array, ids: np.array, q_indexes: list = [4, 6], averaged: bool = True):
    print("Construct System")
    system = construct_pyscal_system(positions, ids)

    print("Find neighbors")
    #system.find_neighbors(method="voronoi")
    system.find_neighbors(method="cutoff", cutoff=5.)
    #print(system.)
    print("get q_vals")
    q = system.get_qvals(q_indexes, averaged=averaged)
    return q"""


def feature_dict_to_sample_lists(feature_dict):
    sample_lists = {}
    sample_lists["positions"] = [
        (tp, *position)
        for tp, positions in enumerate(feature_dict["positions"])
        for position in positions
    ]
    sample_lists["volumes"] = [
        (tp, volume)
        for tp, volumes in enumerate(feature_dict["volumes"])
        for volume in volumes
    ]
    sample_lists["velocities"] = [
        (tp, *velocity)
        for tp, velocities in enumerate(feature_dict["velocities"])
        for velocity in velocities
    ]
    sample_lists["average_local_density"] = [
        (tp, distance_bin, count)
        for tp, local_density in enumerate(feature_dict["local_density"])
        for distance_bin, count in enumerate(local_density)
    ]
    sample_lists["average_velocity_correlation"] = [
        (tp, distance_bin, angle)
        for tp, velocity_correlation in enumerate(feature_dict["velocity_correlation"])
        for distance_bin, angle in enumerate(velocity_correlation)
    ]
    sample_lists["central_velocity_correlation"] = [
        (tp, distance_bin, angle)
        for tp, velocity_correlation in enumerate(
            feature_dict["central_velocity_correlation"]
        )
        for distance_bin, angle in enumerate(velocity_correlation)
    ]
    sample_lists["central_local_density"] = [
        (tp, distance_bin, count)
        for tp, local_density in enumerate(feature_dict["central_local_density"])
        for distance_bin, count in enumerate(local_density)
    ]
    # sample_lists["t_pos_vel"] = []
    return sample_lists


def combine_feature_dicts(list_of_feature_dicts):
    pass


def get_metadata_keys(feature_dict):
    return list(filter(lambda key: key[-5:] == "_conf", feature_dict.keys()))


def extract_hull(positions, N_bins=50):
    N_cells = positions[:, 0].size

    spherical_positions = np.full(
        positions.shape, 0.0
    )  # This stores rho, theta and phi for each of the cartesian coordinates in positions

    spherical_positions[:, 0] = np.sqrt(
        positions[:, 0] ** 2 + positions[:, 1] ** 2 + positions[:, 2] ** 2
    )
    spherical_positions[:, 1] = np.arccos(positions[:, 2] / spherical_positions[:, 0])
    for i in range(N_cells):
        if positions[i, 1] < 0:
            spherical_positions[i, 2] = (
                np.arccos(
                    positions[i, 0]
                    / np.sqrt(positions[i, 0] ** 2 + positions[i, 1] ** 2)
                )
                + np.pi
            )
        else:
            spherical_positions[i, 2] = np.arccos(
                positions[i, 0] / np.sqrt(positions[i, 0] ** 2 + positions[i, 1] ** 2)
            )

    dphi = 2 * np.pi / N_bins
    dtheta = np.pi / N_bins
    theta_bins = [i * dtheta for i in range(int(N_bins))]
    phi_bins = [i * dphi for i in range(N_bins)]

    hull_positions = []
    for i, theta in enumerate(theta_bins):
        if i == 0:
            pass
        for j, phi in enumerate(phi_bins):
            if j == 0:
                pass
            bin_members = np.where(
                (spherical_positions[:, 1] < theta)
                & (spherical_positions[:, 1] >= theta_bins[i - 1])
                & (spherical_positions[:, 2] < phi)
                & (spherical_positions[:, 2] >= phi_bins[j - 1])
            )
            if bin_members[0].size > 0:
                member_spherical_coords = spherical_positions[bin_members[0]]
                member_cartesian_coords = positions[bin_members[0]]
                outermost = np.max(member_spherical_coords[:, 0])
                outermost_index = np.where(member_spherical_coords[:, 0] == outermost)[
                    0
                ][0]
                hull_positions.append(member_cartesian_coords[outermost_index, :])
    return (positions, np.array(hull_positions))


def extract_non_gaslikes(df):
    positions = df.loc[df["aggregation_state"] < 2]
    if np.array(positions["CenterX"]).size > 0:
        return np.stack(
            (
                np.array(positions["CenterX"]),
                np.array(positions["CenterY"]),
                np.array(positions["CenterZ"]),
            ),
            axis=-1,
        )
    else:
        print("No points")


def extract_gaslikes(df):
    positions = df.loc[df["aggregation_state"] == 2]
    if np.array(positions["CenterX"]).size > 0:
        return np.stack(
            (
                np.array(positions["CenterX"]),
                np.array(positions["CenterY"]),
                np.array(positions["CenterZ"]),
            ),
            axis=-1,
        )
    else:
        print("No points")


def get_path_density(
    positions,
    N_hull_points=10,
    N_hull_bins=10,
    timelimit_per_path=10,
    max_r=300,
    dr=10,
    check_dist=40,
    percentile_ratio_threshold=1.1,
    verbose=False,
    start_at_center=True,
):
    centered_positions, current_hull = extract_hull(positions, N_bins=N_hull_bins)
    center = (0, 0, 0)  # Coordinates are always centered within this function
    dists = [
        dist(centered_positions[i, :], center)
        for i in range(centered_positions[:, 0].size)
    ]
    percentile = np.percentile(dists, 95)
    # center = (np.sum(positions[:,0])/N_positions, np.sum(positions[:,1])/N_positions, np.sum(positions[:,0])/N_positions)#Center of positions

    N_points = current_hull[:, 0].size
    current_hull = np.append(current_hull, np.array(center)).reshape((N_points + 1, 3))
    tree = KDTree(current_hull, current_hull[:, 0].size)
    nearest_dist, nearest_ind = tree.query(current_hull, k=current_hull[:, 0].size)

    putative_arms = current_hull[
        nearest_ind[
            nearest_ind[:, 0].size - 1,
            max(0, nearest_ind[0, :].size - 1 - N_hull_points) :,
        ]
    ]  # extract the N=N_hull_points number of points that are furthest from the center
    point_dists = [
        dist(putative_arms[i, :], center) for i in range(putative_arms[:, 0].size)
    ]  # Distances of putative arm points from center
    arm_indices = np.where(point_dists[:] / percentile >= percentile_ratio_threshold)[
        0
    ]  # Extract those points which are far enough away from the center

    if verbose == True:
        print("Ratios between path points and 95th percentile:")
        print(np.array(point_dists)[arm_indices] / percentile)
    if arm_indices.size > 0:
        hull_to_investigate = putative_arms[arm_indices]
        if verbose == True:
            print("Number of points: %d" % hull_to_investigate[:, 0].size)
    else:
        print(
            "No points found at a distance larger than %1.4f times the 95th percentile"
            % percentile_ratio_threshold
        )
        return {}, {}

    max_nearest_neighbor_dist = np.max(nearest_dist[:, 1])

    paths = {}
    densities = {}
    for i in range(hull_to_investigate[:, 0].size):
        if i % 5 == 0 and verbose == True:
            print("%d out of %d" % (i, hull_to_investigate[:, 0].size - 1))

        centered_positions, neighbours = compute_neighbours(
            centered_positions, center, max_nearest_neighbor_dist
        )
        start = hull_to_investigate[i, :]  # not bound to be within neighbourhood radius
        # start_index = int(np.where((centered_positions[:,0] == start[0]) & (centered_positions[:,1] == start[1]) & (centered_positions[:,2] == start[2]))[0])
        N_points_on_path = 10
        dx = (start[0] - center[0]) / (N_points_on_path - 1)
        dy = (start[1] - center[1]) / (N_points_on_path - 1)
        dz = (start[2] - center[2]) / (N_points_on_path - 1)
        path = [
            [
                center[0] + point_nr * dx,
                center[1] + point_nr * dy,
                center[2] + point_nr * dz,
            ]
            for point_nr in range(N_points_on_path + 1)
        ]
        # path = astar(centered_positions, neighbours,start_index,0,verbose = verbose,timelimit=timelimit_per_path)
        if path != None:
            if len(path) > 0:
                paths[i] = path
                densities[i] = {}
                try:
                    for j, point in enumerate(paths[i]):
                        densities[i][j] = get_radial_local_density_function(
                            centered_positions,
                            center=(point[0], point[1], point[2]),
                            max_r=max_r,
                            dr=dr,
                            check_dist=check_dist,
                        )
                except TypeError:
                    pass
                except IndexError:
                    pass

    return paths, densities


def assign_gaslikes_via_fluidity(data, scale_factor, NTimesteps=5, minNNeighbors=3):
    # if 'radial_displacement' not in self.features.keys():
    #    self.get_radial_displacement(store=True)

    for t in list(data.keys()):
        #            if 'volumes' not in self.features.keys():
        #                self.get_volumes_at_time(t)

        oldTimepoint = max(list(data.keys())[0], t - NTimesteps)
        #            oldPos = self.get_positions_at_time(oldTimepoint)
        #            currentPos = self.get_positions_at_time(t)

        dists = np.array(
            [
                dist(
                    np.array(
                        [
                            data[t]["CenterX"][i],
                            data[t]["CenterY"][i],
                            data[t]["CenterZ"][i],
                        ]
                    ),
                    np.array(
                        [
                            data[oldTimepoint]["CenterX"][i],
                            data[oldTimepoint]["CenterY"][i],
                            data[oldTimepoint]["CenterZ"][i],
                        ]
                    ),
                )
                for i in data[oldTimepoint].index
            ]
        )

        volumes = np.array([data[t]["Volume"][i] for i in data[oldTimepoint].index])

        ids = np.array(data[oldTimepoint].index)

        radii = scale_factor * (volumes[:] * 3.0 / (4.0 * np.pi)) ** (1 / 3)

        threshold_dist = 2 * np.mean(radii)

        cmpArr = dists > radii

        indices = np.where(cmpArr == True)[0]
        data[t]["aggregation_state"] = np.full(
            data[t].index.size, 0
        )  # 0 --> solid, 1 --> liquid, 2 --> gas
        if indices.size > 0:
            data[t].loc[ids[indices], "aggregation_state"] = 2

        # data[t] = assign_gaslikes(data[t],mode=1)

        # ids = np.array(data[t].index)
        # singleCellState = np.array(data[t]['aggregation_state'])
        # indices = np.where(singleCellState == 2)[0]
        # if indices.size > 0:
        #     data[t].loc[ids[indices],'aggregation_state'] -= 1
        # indices = np.where(data[t]['aggregation_state'] < 0)[0]
        # if indices.size > 0:
        #     data[t].loc[ids[indices],'aggregation_state'] = 0

    return data


# def assign_gaslikes(df,neighbor_threshold:int = 5,threshold_percentile: int = 95) -> DataFrame:
def assign_gaslikes(
    df,
    init_p95=100,
    init_neighbordist=7.26,
    threshold_dist: float = 16,
    mode: int = 2,
    min_n_neighbors: int = 3,
) -> DataFrame:
    """
    This function assigns gaslike or non-gaslike status to datasets which have no prior classifiction, e.g. simulation data

    args:
    df: Single timepoint Dataframe, which is adjusted and then returnedN
    neighbor_threshold: number of neighbors of each cell which is included in the calculation of the threshold distance:
                        any cell, that contains less than N=neighbor_threshold neighbors within a distance of twice the threshold distance
                        is classified as gaslike
    threshold_percentile: Determines cut-off distance for classification as gaslikes
    """

    positions = None
    if all(key in df.keys() for key in ["CenterX", "CenterY", "CenterZ"]):
        positions = np.array(df[["CenterX", "CenterY", "CenterZ"]])
        NaNIndices = np.where(np.isnan(positions[:, 0]) == True)[0]
        if NaNIndices.size > 0:
            positions[NaNIndices, 0] = 0
            positions[NaNIndices, 1] = 0
            positions[NaNIndices, 2] = 0
        ids = np.array(df["CenterX"].keys())
    else:
        print("Not all coordinates present. Stopping")
        return df

    #    try:

    if mode == 0:  # EXTRACT ACCORDING TO DISTANCE TO NEAREST NEIGHBOR
        tree = KDTree(positions, 2)
        nearest_dist, nearest_ind = tree.query(positions, k=2)
        indices = np.where(nearest_dist[:, 1] < threshold_dist)[0]
    #        for index in indices:
    #            df.loc[ids[index],'aggregation_state'] = 2
    elif mode == 1:
        tree = KDTree(positions, 20)
        neighbor_counts = tree.query_radius(positions, threshold_dist, count_only=True)
        indices = np.where(neighbor_counts >= min_n_neighbors)[0]
    else:
        center = np.array([np.mean(positions[:, i]) for i in range(3)])
        dists = np.array(
            [dist(positions[i, :], center) for i in range(positions[:, 0].size)]
        )  # Distances from centroid
        p95 = np.percentile(dists, 95)
        #        if p95 >= 1.25*init_p95:
        #            indices = np.array([])
        #        else:
        tree = KDTree(positions, 2)
        nearest_dist, nearest_ind = tree.query(positions, k=2)
        indices = np.where(
            (nearest_dist[:, 1] < threshold_dist) | (dists < 1.25 * init_p95)
        )[0]

    df["aggregation_state"] = np.full(positions[:, 0].size, 2)
    if indices.size > 0:
        df.loc[ids[indices], "aggregation_state"] = 0
    #    except ValueError:
    #        print("Positions may contain NaNs. Check your data! Returning unchanged df")

    return df


def get_non_gaslike_fraction(gaslike_column=None):
    return len(np.where(gaslike_column[:] < 2)[0]) / gaslike_column.size


def getBinnedGrid(positions, binSize=20):
    """
    Returns a 3D grid with the number of cells in the respective bins.
    Bin size is the relative Size of the bins to the positions.
    """
    sysSize = int(np.max(positions) / binSize) + 1
    print(sysSize)
    grid = np.zeros((sysSize, sysSize, sysSize))
    for c in positions:
        grid[int(c[0] / binSize), int(c[1] / binSize), int(c[2] / binSize)] += 1
    return grid


def getRadialFFT(positions):
    """
    Gets the radially averaged FFT of the binned positions.
    Returns:

    distances, ffts: all Points of the FFT for a scatter plot

    fftFreq, fftVal, std: Binned values of the FFT to have a 1D radial distribution

    For Plotting see commented out Section at end of function
    """
    grid = getBinnedGrid(positions)

    fftgrid = np.fft.fftn(grid)
    shiftFFT = np.abs(np.fft.fftshift(fftgrid))

    # Radially average the fourier transformed data
    distances = []
    ffts = []
    for x in range(grid.shape[0]):
        for y in range(grid.shape[1]):
            for z in range(grid.shape[2]):
                dist = np.sqrt(
                    (x - grid.shape[0] / 2) ** 2
                    + (y - grid.shape[1] / 2) ** 2
                    + (z - grid.shape[2] / 2) ** 2
                )
                distances.append(dist)
                ffts.append(np.log(shiftFFT[x, y, z]))
    # Extract 1d distribution copy and paste https://stackoverflow.com/questions/15556930/turn-scatter-data-into-binned-data-with-errors-bars-equal-to-standard-deviation
    nbins = 70
    ffts = np.array(ffts)
    n, _ = np.histogram(distances, bins=nbins)
    sy, _ = np.histogram(distances, bins=nbins, weights=ffts)
    sy2, _ = np.histogram(distances, bins=nbins, weights=ffts * ffts)
    mean = sy / n
    std = np.sqrt(sy2 / n - mean * mean)
    fftFreq = (_[1:] + _[:-1]) / 2
    fftVal = mean

    # plt.scatter(distances, ffts, s=0.1)
    # plt.errorbar(fftFreq,fftVal,std, fmt='r-')
    # plt.xlabel('Radially Averaged frequency')
    # plt.ylabel('log FFT value')
    # plt.xlim(0,20)
    # plt.ylim(0,12)
    # plt.show()

    return fftFreq, fftVal, std, distances, ffts


def getNeighborRetentionAtTime(DH, startTime, searchRadius=40, timeBack=15):
    coords = dhs.get_positions_at_time(startTime)
    tree = KDTree(coords)
    start_nn_indices, start_nn_distances = tree.query_radius(
        coords, return_distance=True, r=searchRadius
    )
    startCellIds = dhs.data[
        startTime
    ].index.to_numpy()  # To map the positions to cellIDs
    allRetention = []
    for time in range(startTime - 1, startTime - timeBack, -1):
        print("Calculating time %d" % time)
        coordsCurr = dhs.get_positions_at_time(time)
        treeCur = KDTree(coordsCurr)
        curr_nn_indices, curr_nn_distances = tree.query_radius(
            coordsCurr, return_distance=True, r=searchRadius
        )
        currCellIds = dhs.data[time].index.to_numpy()  # To map the positions to cellIDs

        radialRetention = np.zeros(searchRadius + 1)

        for startIndex, cellID in enumerate(startCellIds):
            try:
                currIndex = int(
                    np.where(currCellIds == cellID)[0][0]
                )  # Check if cell already exists
            except:
                continue

            newStartID = []
            newStartDist = []
            for i in range(len(start_nn_indices[startIndex])):
                startNeiCellID = startCellIds[start_nn_indices[startIndex][i]]
                startNeiDist = start_nn_distances[startIndex][i]

                try:
                    curNeiIndex = int(
                        np.where(currCellIds == startNeiCellID)[0][0]
                    )  # Check if cell already exists
                except:
                    continue
                # Check if is in neighbors
                if not (curNeiIndex in curr_nn_indices[currIndex]):
                    continue

                curNeiDist = curr_nn_distances[currIndex][
                    curr_nn_indices[currIndex] == curNeiIndex
                ]
                # chekc if distance is still lower than x
                if np.ceil(curNeiDist) <= np.ceil(startNeiDist):
                    radialRetention[int(np.ceil(startNeiDist))] += 1

                    newStartID.append(
                        start_nn_indices[startIndex][i]
                    )  # Only keep them if they stay within their sphere
                    newStartDist.append(startNeiDist)

                curNeiDist = curr_nn_distances
                curr_nn_indices[currIndex]
            start_nn_indices[startIndex] = newStartID
            start_nn_distances[startIndex] = newStartDist
        allRetention.append(radialRetention)
    return allRetention
    #####PLOT LIKE THIS:
    # i=1
    # for radialRetention in allRetention:
    #    plt.plot(radialRetention,label=i)
    #    i+=1
    # plt.legend()
    # plt.title('Number = time steps back')
    # plt.xlabel('Distance from cell')
    # plt.ylabel('Cells that remained within their distance')
    # plt.show()

    # plt.imshow(allRetention)
    # plt.ylabel('Time steps back')
    # plt.xlabel('Distance from cell')
    # plt.show()


def get_radial_displacement(
    df, system_size=(400, 400, 402)
):  # Returns a dictionary containing the radial displacement over time for each cell ID
    start_time = list(df.keys())[
        0
    ]  # Define start time. This should usually be set to 0  TODO: what happens if the first frame is discarded?
    # end_time = list(df.keys()[-1])

    tracked_IDs = list(
        df[start_time].index
    )  # This list will update as we move forward in time, if there are new cells added (e.g. by division)

    cell_start_times = {
        id: start_time for id in tracked_IDs
    }  # Contains the starting times for all cells, which needs to be tracked in case of cell division
    displacements = {
        id: np.full(len(list(df.keys())), -1.0) for id in tracked_IDs
    }  # The actual feature: a dictionary with cell IDs as keys and the displacement trajectories as values, in the form of a numpy array

    initial_positions = {
        id: (
            df[start_time]["CenterX"][id],
            df[start_time]["CenterY"][id],
            df[start_time]["CenterZ"][id],
        )
        for id in tracked_IDs
    }  # Starting positions of each cell.

    for t in list(df.keys()):
        current_IDs = list(df[t].index)

        ################## Check, if new cells have been generated ############
        if current_IDs != tracked_IDs:
            for id in current_IDs:
                ############ Add new IDs to tracked_IDs and displacement dict ############
                if id in tracked_IDs == False:
                    cell_start_times[id] = t
                    tracked_IDs.append(id)
                    displacements[id] = np.full(len(list(df.keys())), -1.0)
                    initial_positions[id] = (
                        df[t]["CenterX"][id],
                        df[t]["CenterY"][id],
                        df[t]["CenterZ"][id],
                    )

        ################## Update displacements for this timeframe ############
        for id in displacements.keys():
            try:
                current_pos = (
                    df[t]["CenterX"][id],
                    df[t]["CenterY"][id],
                    df[t]["CenterZ"][id],
                )

                if t > 0:  # Check for boundary jumps
                    try:
                        old_pos = (
                            df[t - 1]["CenterX"][id],
                            df[t - 1]["CenterY"][id],
                            df[t - 1]["CenterZ"][id],
                        )
                        intermediate_pos = np.array(current_pos)
                        for i in range(3):
                            if current_pos[i] - old_pos[i] > 0.5 * system_size[i]:
                                intermediate_pos[i] -= system_size[i]
                            elif current_pos[i] - old_pos[i] < -0.5 * system_size[i]:
                                intermediate_pos[i] += system_size[i]

                        current_pos = tuple(intermediate_pos)

                    except (
                        KeyError
                    ):  # Cell did not exist before (was newly generated at t)
                        pass

                displacements[id][t] = np.sqrt(
                    (current_pos[0] - initial_positions[id][0]) ** 2
                    + (current_pos[1] - initial_positions[id][1]) ** 2
                    + (current_pos[2] - initial_positions[id][2]) ** 2
                )
            except KeyError:
                pass
    return {
        "initial_positions": initial_positions,
        "start_times": cell_start_times,
        "displacements": displacements,
    }


def get_densities_along_axes(
    positions: np.array, cylinder_radius: float = 25.0, max_distance: float = 300
) -> list:
    x_cylinder = np.array(
        list(
            filter(
                lambda x: x[1] ** 2 + x[2] ** 2 < cylinder_radius**2
                and x[0] < max_distance
                and x[0] > -max_distance,
                positions,
            )
        )
    )
    y_cylinder = np.array(
        list(
            filter(
                lambda x: x[0] ** 2 + x[2] ** 2 < cylinder_radius**2
                and x[1] < max_distance
                and x[1] > -max_distance,
                positions,
            )
        )
    )
    z_cylinder = np.array(
        list(
            filter(
                lambda x: x[0] ** 2 + x[1] ** 2 < cylinder_radius**2
                and x[2] < max_distance
                and x[2] > -max_distance,
                positions,
            )
        )
    )
    cells_in_cylinders = [len(cyl) for cyl in [x_cylinder, y_cylinder, z_cylinder]]
    cylinder_volume = 1  # 2*max_distance*np.pi*cylinder_radius**2
    densities = [cell_count / cylinder_volume for cell_count in cells_in_cylinders]
    return densities


# def get_projection_circumference(positions,
#                      verbose:bool=False,
#                      brute_force:bool=False,
#                      query_radius=5,
#                      **kwargs
#                      ):


#     img,area = get_projection_edge(positions,**kwargs)
#     x,y = extract_wall_points(img)
#     if brute_force == True:
#         sorted_positions = order_points_brute_force(x,y,verbose=verbose,query_radius=query_radius)
#         return get_circumference(sorted_positions)
#     else:
#         return len(x)#This may be less accurate than the "brute force" method


def get_tumor_area(points, verts, faces):
    points = points.astype(float)
    verts = verts.astype(float)
    for i in range(3):
        verts[:, i] -= np.mean(verts[:, i])
        points[:, i] -= np.mean(points[:, i])

    dists_verts = np.linalg.norm(verts[:], axis=1)
    p95_verts = np.percentile(dists_verts, 95)
    dists_points = np.linalg.norm(points[:], axis=1)
    p95_points = np.percentile(dists_points, 95)

    scale_factor = p95_points / p95_verts
    verts *= scale_factor

    area = 0.0
    vecs1 = verts[faces[:, 1], :] - verts[faces[:, 0], :]
    vecs2 = verts[faces[:, 2], :] - verts[faces[:, 0], :]
    crosses = np.cross(vecs1[:], vecs2[:])
    area = 0.5 * np.sum(np.linalg.norm(crosses[:], axis=1))

    return area


def get_tumor_surface_cloud_MC(pos, retTypes=["points"], **kwargs):
    points, MCInfo = get_marching_cubes_tumor_surface(pos, returnMCInfo=True, **kwargs)
    verts, faces, normals, values = MCInfo
    retList = []
    if "points" in retTypes:
        retList.append(points)
    if "verts" in retTypes:
        retList.append(verts)
    if "faces" in retTypes:
        retList.append(faces)
    if "normals" in retTypes:
        retList.append(normals)
    if "values" in retTypes:
        retList.append(values)
    return retList


def get_tumor_surface_cloud_orientation(points, normals):
    assert normals.shape == points.shape, "normals must have same shape as points"
    scalarProducts = np.zeros(points.shape[0], dtype=float)
    for i in range(points.shape[0]):
        points[i, :] /= np.linalg.norm(points[i, :])
        scalarProducts[i] = np.dot(points[i, :], normals[i, :])
    return scalarProducts


def get_gaslike_distance(gl=None, ngl=None, glFrac=True):
    if type(gl) == type(None):
        gl = np.array([])
    NPoints = gl.shape[0] + ngl.shape[0]
    center = [(np.sum(ngl[:, i]) + np.sum(gl[:, i])) / NPoints for i in range(3)]
    ngl_dists = [dist(ngl[i, :], center) for i in range(ngl.shape[0])]
    gl_dists = [dist(gl[i, :], center) for i in range(gl.shape[0])]
    p95_ngl = np.percentile(ngl_dists, 95)
    r_gyr = gl_dists / p95_ngl
    glNbr = gl.shape[0]
    if glNbr == 0:
        r_gyr = [1.0]  # The default value if we do not have any gaslikes
    if glFrac == True:
        glNbr /= gl.shape[0] + ngl.shape[0]
    return [glNbr, np.mean(r_gyr)]
