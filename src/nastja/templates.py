NASTJA_TEMPLATE_1 = {"Comments": [
                        "Celltype         Usage",
                        "0                Not Used",
                        "1-5             Solids for blood vessels / ECM",
                        "6               Liquid  ",
                        "7               Apoptotic cell Type ",
                        "8               Basic Non Dividing Cell type (surrounding)",
                        "9               Cancer"
                    ],
                    "Application": "Cells",
                    "CellsInSilico": {
                        "ecmdegradation": {
                            "enabled": "true",
                            "steps": 5000,
                            "stochastic": "true",
                            "probability": 0.5
                        },
                        "energyfunctions": [
                            "Volume00",
                            "Surface01",
                            "Motility00",
                            "Adhesion01"
                        ],
                        "liquid": 6,
                        "volume": {
                            "default": {
                                "storage": "const",
                                "value": 500
                            },
                            "lambda": [
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                7.5,
                                7.5,
                                7.5
                            ],
                            "sizechange": [
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                -0.05,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0
                            ]
                        },
                        "surface": {
                            "default": {
                                "storage": "const",
                                "value": 400
                            },
                            "lambda": [
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                5.625,
                                5.625,
                                1
                            ],
                            "sizechange": [
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                -0.05,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0
                            ]
                        },
                        "adhesion": {
                            "matrix": [
                                [
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0
                                ],
                                [
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    450
                                ],
                                [
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0
                                ],
                                [
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0
                                ],
                                [
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0
                                ],
                                [
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0
                                ],
                                [
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0
                                ],
                                [
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0
                                ],
                                [
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0
                                ],
                                [
                                    0,
                                    450,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    0,
                                    50
                                ]
                            ]
                        },
                        "temperature": 50,
                        "division": {
                            "enabled": "true",
                            "condition": [
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "( volume >= 0.9 * volume0 )  & ( rnd() <= 0.00001 ) & generation < 1"
                            ]
                        },
                        "centerofmass": {
                            "steps": 1
                        },
                        "signaling": {
                            "enabled": "false"
                        },
                        "orientation": {
                            "enabled": "true",
                            "motility": "persistentRandomWalk",
                            "persistenceMagnitude": 0.0,
                            "recalculationtime": 200,
                            "motilityamount": [
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                75
                            ]
                        },
                        "visitor": {
                            "stepwidth": 10,
                            "checkerboard": "01"
                        }
                    },
                    "Geometry": {
                        "blockcount": [
                            4,
                            4,
                            3
                        ],
                        "blocksize": [
                            100,
                            100,
                            134
                        ]
                    },
                    "Settings": {
                        "randomseed": 0,
                        "statusoutput": 500,
                        "timesteps": 250000
                    },
                    "WriteActions": [
                        "ParallelVTK_Cells",
                        "CellInfo"
                    ],
                    "Writers": {
                        "CellInfo": {
                            "field": "",
                            "groupsize": 0,
                            "steps": 1000,
                            "writer": "CellInfo"
                        },
                        "ParallelVTK_Cells": {
                            "field": "cells",
                            "outputtype": "UInt32",
                            "printhints": "false",
                            "steps": 10000,
                            "writer": "ParallelVtkImage"
                        }
                    },
                }
