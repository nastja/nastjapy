#!/bin/bash

if [ "$#" -ne 2 ]; then
  echo "Usage $0 database folder_with_csv"
  exit
fi

database="$1"

read -r -d '' table << EOM
CREATE TABLE cells(
  "FRAME" UNSIGNED SMALLINT,
  "CellID" UNSIGNED INT,
  "CenterX" UNSIGNED SMALLINT,
  "CenterY" UNSIGNED SMALLINT,
  "CenterZ" UNSIGNED SMALLINT,
  "Volume" FLOAT,
  "Surface" FLOAT,
  "Typ" UNSIGNED SMALLINT,
  "Signal0" FLOAT,
  "Signal1" FLOAT,
  "Signal2" FLOAT,
  "Age" UNSIGNED INT,
  "PolarityX" SMALLINT,
  "PolarityY" SMALLINT,
  "PolarityZ" SMALLINT,
  "MotilityDirX" FLOAT,
  "MotilityDirY" FLOAT,
  "MotilityDirZ" FLOAT
);
CREATE INDEX frame on cells(frame, cellid);
EOM
echo "$table" | sqlite3 "$database"

pat='.*([0-9]{5})\.csv'

for f in "$2"/output_cells*.csv; do
  [[ "$f" =~ $pat ]]
  frame=${BASH_REMATCH[1]}
  echo -ne "\rImport frame $frame..."
read -r -d '' commands << EOM
.mode csv
.separator " "
.import --skip 1 "|awk -v Frame=$frame \'{if (\$2 != -1) {print Frame, \$0}}\' \'$f\'" cells
EOM
  echo "$commands" | sqlite3 "$database"
done
echo