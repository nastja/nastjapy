import numpy as np

# from nastja.utils import collect_all_features_for_total_time_parallel, feature_dict_to_sample_lists
# import torch
from scipy.stats import wasserstein_distance as wsd
from sklearn.metrics import mean_squared_error

# import ddks

# device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def feature_to_sample_list(feature, feature_name):
    if feature_name in ["velocities", "volumes"]:
        samples = np.array(
            [
                [timepoint, feat]
                for timepoint, feature_at_timepoint in feature.items()
                for feat in feature_at_timepoint
            ]
        )
    elif feature_name in [
        "average_local_density",
        "central_local_density",
        "average_velocity_correlation",
        "central_velocity_correlation",
    ]:
        samples = np.array(
            [
                [timepoint, distance, f]
                for timepoint, feature_at_timepoint in feature.items()
                for distance, f in enumerate(feature_at_timepoint)
            ]
        )
    elif feature_name in [
        "individual_local_density",
        "individual_velocity_correlation",
        "radially_averaged_local_density",
        "radially_averaged_velocity_correlation",
    ]:
        samples = [
            [timepoint, distance, f]
            for timepoint, feature_at_timepoint in feature.items()
            for individual in feature_at_timepoint
            for distance, f in enumerate(individual)
        ]
    else:
        print("Unknown feature. Returning None")
        samples = None
    return samples


def vectorize_feature(feature, feature_name):
    if feature_name in [
        "average_local_density",
        "central_local_density",
        "radially_averaged_local_density",
        "average_velocity_correlation",
        "central_velocity_correlation",
        "radially_averaged_velocity_correlation",
    ]:
        tensor = np.array(
            [
                f
                for feature_at_timepoint in feature.values()
                for f in feature_at_timepoint
            ]
        )
    elif feature_name in [
        "individual_local_density",
        "individual_velocity_correlation",
    ]:
        bins = list(feature.items())[0].shape[1]
        shape = (len(feature.items()), bins, bins)
        tensor = [
            [timepoint, distance, f]
            for timepoint, feature_at_timepoint in feature.items()
            for individual in feature_at_timepoint
            for distance, f in enumerate(individual)
        ]
    else:
        print("Unknown feature. Returning None")
        tensor = None
    return tensor


# def ddKS_test(dist1,dist2):
#     dist1 = torch.from_numpy(dist1)
#     dist2 = torch.from_numpy(dist2)
#     calculation = ddks.methods.ddKS()
#     distance = calculation(dist1, dist2)
#     return distance


# def vdKS_test(dist1,dist2):
#     dist1 = torch.from_numpy(dist1)
#     dist2 = torch.from_numpy(dist2)
#     calculation = ddks.methods.vdKS()
#     distance = calculation(dist1, dist2)
#     return distance

# def rdKS_test(dist1,dist2):
#     dist1 = torch.from_numpy(dist1)
#     dist2 = torch.from_numpy(dist2)
#     calculation = ddks.methods.rdKS()
#     distance = calculation(dist1, dist2)
#     return distance

# def MMD(x, y, kernel="rbf"):
#     """Emprical maximum mean discrepancy. The lower the result
#        the more evidence that distributions are the same.
#
#     Args:
#         x: first sample, distribution P
#         y: second sample, distribution Q
#         kernel: kernel type such as "multiscale" or "rbf"
#     """
#     if type(x) is not torch.tensor:
#         x = torch.tensor(x)
#     if type(y) is not torch.tensor:
#         x = torch.tensor(y)
#     xx, yy, zz = torch.mm(x, x.t()), torch.mm(y, y.t()), torch.mm(x, y.t())
#     rx = (xx.diag().unsqueeze(0).expand_as(xx))
#     ry = (yy.diag().unsqueeze(0).expand_as(yy))
#
#     dxx = rx.t() + rx - 2. * xx  # Used for A in (1)
#     dyy = ry.t() + ry - 2. * yy  # Used for B in (1)
#     dxy = rx.t() + ry - 2. * zz  # Used for C in (1)
#
#     XX, YY, XY = (torch.zeros(xx.shape).to(device),
#                   torch.zeros(xx.shape).to(device),
#                   torch.zeros(xx.shape).to(device))
#
#     if kernel == "multiscale":
#
#         bandwidth_range = [0.2, 0.5, 0.9, 1.3]
#         for a in bandwidth_range:
#             XX += a ** 2 * (a ** 2 + dxx) ** -1
#             YY += a ** 2 * (a ** 2 + dyy) ** -1
#             XY += a ** 2 * (a ** 2 + dxy) ** -1
#
#     if kernel == "rbf":
#
#         bandwidth_range = [10, 15, 20, 50]
#         for a in bandwidth_range:
#             XX += torch.exp(-0.5 * dxx / a)
#             YY += torch.exp(-0.5 * dyy / a)
#             XY += torch.exp(-0.5 * dxy / a)
#
#     return torch.mean(XX + YY - 2. * XY)


def sliced_wasserstein_distance_d1_over_time(
    feature1, feature2, respect_ordering=False
):
    print("1D")
    assert len(feature1.values()) == len(
        feature2.values()
    ), "Features need to contain same number of timesteps!"
    assert all(
        [len(value.shape) == 1 for value in feature1.values()]
    ), "Feature1 needs to be a dictionary of 1D np.arrays."
    assert all(
        [len(value.shape) == 1 for value in feature2.values()]
    ), "Feature2 needs to be a dictionary of 1D np.arrays."
    distance = np.sum(
        [
            wasserstein_distance(
                np.array(feat1), np.array(feat2), respect_ordering=respect_ordering
            )
            for feat1, feat2 in zip(feature1.values(), feature2.values())
        ]
    )
    return distance


def sliced_wasserstein_distance_d2_over_time(
    feature1, feature2, respect_ordering=False, include_transposed=False
):
    print("2D")
    assert len(feature1.values()) == len(
        feature2.values()
    ), "Features need to contain same number of timesteps!"
    assert all(
        [len(value.shape) == 2 for value in feature1.values()]
    ), "Feature1 needs to be a dictionary of 1D np.arrays."
    assert all(
        [len(value.shape) == 2 for value in feature2.values()]
    ), "Feature2 needs to be a dictionary of 1D np.arrays."
    print("2D")
    distance = np.sum(
        [
            wasserstein_distance(row1, row2, respect_ordering=respect_ordering)
            for feat1, feat2 in zip(feature1.values(), feature2.values())
            for row1, row2 in zip(feat1, feat2)
        ]
    )
    if include_transposed:
        distance += np.sum(
            [
                wasserstein_distance(row1, row2, respect_ordering=respect_ordering)
                for feat1, feat2 in zip(feature1.values(), feature2.values())
                for row1, row2 in zip(feat1.transpose(), feat2.transpose())
            ]
        )
        distance /= 2
    return distance


def sliced_wasserstein_distance_over_time(
    feature1, feature2, respect_ordering=False, truncate=False
):
    assert (
        len(feature1.values()) == len(feature2.values()) or truncate
    ), "Features need to contain same number of timesteps!"
    if truncate:
        if len(feature1) < len(feature2):
            (
                feature1,
                feature2,
            ) = (
                feature2,
                feature1,
            )
        tps = [tp for tp in feature1.keys() if tp in feature2.keys()]
        feature1 = {tp: feature1[tp] for tp in tps}
    if all([len(value.shape) == 1 for value in feature1.values()]) and all(
        [len(value.shape) == 1 for value in feature2.values()]
    ):
        return sliced_wasserstein_distance_d1_over_time(
            feature1, feature2, respect_ordering=respect_ordering
        )
    elif all([len(value.shape) == 2 for value in feature1.values()]) and all(
        [len(value.shape) == 2 for value in feature2.values()]
    ):
        return sliced_wasserstein_distance_d2_over_time(
            feature1, feature2, respect_ordering=respect_ordering
        )
    else:
        print("Features not in suitable shape. Returning None")
        return None


def wasserstein_distance(dist1, dist2, respect_ordering=False):
    if type(dist1) == type(None) or type(dist2) == type(None):
        print("Neither dist1 nor dist2 may be of type None. Returning None")
        return None
    # assert (type(dist1) != type(None) and type(dist2) != type(None)), "Neither dist1 nor dist2 may be of type None"
    assert (
        len(dist1) == len(dist2) or not respect_ordering
    ), "To take ordering into consideration distribution lengths need to be identical!"
    if respect_ordering:
        dist = list(range(len(dist1)))
        if min(dist1) < 0 or min(dist2) < 0:
            low = np.min((np.min(dist1), np.min(dist2)))
            dist1 += low
            dist2 += low
        return wsd(dist, dist, dist1, dist2)
    else:
        return wsd(dist1, dist2)


def ordered_wasserstein_distance(dist1, dist2):
    return wasserstein_distance(dist1, dist2, respect_ordering=True)


def unordered_wasserstein_distance(dist1, dist2):
    return wasserstein_distance(dist1, dist2, respect_ordering=False)


def mse(feature1, feature2):
    try:
        return mean_squared_error(feature1, feature2)
    except TypeError:
        return mean_squared_error([feature1], [feature2])


def mse_over_time(feature1, feature2):
    assert len(feature1) == len(
        feature2
    ), "Features need to contain same number of timesteps!"
    return np.sum([mse(feat1, feat2) for feat1, feat2 in zip(feature1, feature2)])


def wasserstein_distance_2d(
    feature1, feature2, include_transposed=True, respect_ordering=True
):
    print(np.min(feature1), np.min(feature2), np.max(feature1), np.max(feature2))
    if np.min(feature1) < 0 or np.min(feature2) < 0:
        low = np.min((np.min(feature1), np.min(feature2)))
        feature1 += low
        feature2 += low
    feature1 += 0.0000001
    feature2 += 0.0000001
    distance = np.sum(
        [
            wasserstein_distance(row1, row2, respect_ordering=respect_ordering)
            for row1, row2 in zip(feature1, feature2)
        ]
    )
    if include_transposed:
        distance += np.sum(
            [
                wasserstein_distance(row1, row2, respect_ordering=respect_ordering)
                for row1, row2 in zip(feature1.transpose(), feature2.transpose())
            ]
        )
        distance /= 2
    return distance


def euclidean_distance(feature1, feature2):
    if type(feature1) == list:
        feature1 = np.array(feature1)
    if type(feature2) == list:
        feature2 = np.array(feature2)
    return np.sqrt(np.sum((feature1 - feature2) ** 2))
