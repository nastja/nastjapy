import mpi4py

mpi4py.rc.initialize = False
from mpi4py import MPI

MPI.Init()
import argparse
import os
from nastja.DataHandler import DataHandler, combine_datahandlers
import json
import pickle


def extract_parallel(
    datahandler=None,
    data_path=None,
    comm=None,
    out_dir=None,
    timestep_range=None,
    mat_format=None,
    load_existing=False,
    metadata=None,
    path_to_json="",
    scale_factor=1.0,
    delete_velocities=False,
    save_pickle=True,
    save_hdf5=False,
):
    assert (
        datahandler is not None or data_path is not None
    ), "Datahandler or data_path needs to be specified!"

    if timestep_range == [-1, -1, -1]:
        timestep_range = None

    if out_dir is None:
        if os.path.isdir(data_path):
            out_dir = data_path
        else:
            out_dir, _ = os.path.split(data_path)

    if comm is None:
        comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    print(f"{rank}/{size} active...")

    # chunks = [(chunk[0], chunk[-1]+1) for chunk in np.array_split(np.arange(args.sim_length), size)]
    # print(f"Start extraction on rank {rank}. First Timestep: {chunks[rank][0]}")
    if rank == 0:
        print("Load Data...")

        if path_to_json:
            with open(path_to_json) as json_file:
                additional_metadata = json.load(json_file)
        else:
            additional_metadata = {}

        if datahandler:
            dh = datahandler
        else:
            if load_existing:
                with open(data_path + "/datahandler.p", "rb") as input_file:
                    dh = pickle.load(input_file)
            else:
                print("Arg scale factor = %1.2f" % scale_factor)
                dh = DataHandler(
                    data_path=data_path,
                    timestep_range=timestep_range,
                    mat_format=mat_format,
                    shiftToOrigin=True,
                    scale_factor=scale_factor,
                    feature_metadata=metadata,
                )
                print("dh scale factor = %1.2f" % dh.scale_factor)
            print("Update metadata")
            dh.feature_metadata.update(additional_metadata)
        if delete_velocities:
            keys_to_delete = []
            for key in dh.feature_metadata.keys():
                if "velocit" in key:
                    keys_to_delete.append(key)
            for key in keys_to_delete:
                del dh.feature_metadata[key]

        print("Split Data...")

        datahandler_chunks = dh.split_datahandler(size)
    else:
        datahandler_chunks = None

    print("Distribute Data...")
    dh = comm.scatter(datahandler_chunks, root=0)
    print("Data distributed!")
    print(f"Start feature extraction on rank {rank}.")
    if dh:
        dh.extract_all_features()
    print("Gather data")
    all_datahandlers = comm.gather(dh, root=0)

    if rank == 0:
        all_datahandlers = list(filter(lambda x: x, all_datahandlers))
        combined_datahandler = combine_datahandlers(all_datahandlers)
        # print(len(combined_datahandler.data))
        # print(combined_datahandler.features)
        combined_datahandler.save_feature_dict(os.path.join(out_dir, "feature_dict.p"))
        if save_pickle:
            combined_datahandler.save_datahandler(
                os.path.join(out_dir, "datahandler.p"), overwrite=True
            )
        if save_hdf5:
            combined_datahandler.save_as_hdf5(os.path.join(out_dir, "datahandler.h5"))
    else:
        combined_datahandler = None

    combined_datahandler = comm.bcast(combined_datahandler, root=0)
    if rank == 0:
        print("Feature extraction completed!")
    return combined_datahandler


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("data_path")  # Path to the data file
    parser.add_argument("-o", "--out_dir")  # Output location
    parser.add_argument(
        "-t", "--timestep_range", nargs=3, type=int, default=[-1, -1, -1]
    )  # Timesteps to be analyzed
    parser.add_argument(
        "-m", "--mat_format"
    )  # Data format. Specify this if you use .mat files
    parser.add_argument(
        "-lp", "--load_existing", default=False, action="store_true"
    )  # Load pre-existing datahandlers
    parser.add_argument("-md", "--metadata")
    parser.add_argument(
        "-json", "--path_to_json"
    )  # Specify additional feature_metadata
    parser.add_argument(
        "-sf", "--scale_factor", default=1.0, type=float
    )  # Scale the system
    parser.add_argument(
        "-dv", "--delete_velocities", default=False, type=bool
    )  # Remove velocity-related features from metadata
    parser.add_argument(
        "-h5", "--save_hdf5", default=False, action="store_true"
    )  # Save as HDF5
    # parser.add_argument('-p', '--save_pickle', default=True, type=bool)  # Save as pickle
    # parser.add_argument('sim_length', type=int)

    args = parser.parse_args()
    extract_parallel(**vars(args))
    MPI.Finalize()
